# Kohlenstoffinventur 2027

Auf der Bund-Länderkonferenz vom 08. - 09.11.2022 in Bonn (siehe [Protokoll in der Faircloud](https://faircloud.eu/nextcloud/index.php/s/HBxgtepSjg3jYGw) oder [Protokoll im Repo](/CI27/ci27_doku/Prot_B-L-2022_11_mA1.pdf))
stellte das TI das Konzept (siehe: [Konzept in der Faircloud](https://faircloud.eu/nextcloud/index.php/s/r65ZDFHXkB7Fxbw), [Konzept im Repo](/CI27/ci27_doku/20221019_TI_Konzept_CI2027.pdf)) zur Kohlenstoffinventur 2027 vor und bat die Länder das Projektmanagement vorzunehmen und zu verdichten.

## Eckenzustand

### Waldstatus

Ein überprüfung des Waldstatus würde vollständig zu Lasten der Länder gehen und erscheint in Anbetracht der Feststellung zur BWI2022 nicht zielführend, weshalb Brandenburg auf eine Überprüfung verzichtet. Siehe hierzu auch [Top 5 im Protokoll](/CI27/ci27_doku/Prot_B-L-2024_06_mAnl.pdf)([Protokoll in der Faircloud](https://faircloud.eu/nextcloud/index.php/s/9dxtH6CQRrgrQzQ)) der Bund-Länder-Konferenz am 05.-07.06.2024 in Marienheide.

### Begehbahrkeit

Da die zur BWI22 als nicht begehbar geschlüsselten Waldecken mittlerweile zugänglich sein können (Wasser, Baustelle, Munitionsbelastung, Kalamitätsfläche, usw.) sollten auch diese mit ausgeschrieben, der aktuelle Status geprüft und ggf. aufgenommen werden.

## Eckenanzahl

Für die Planung und den Beschaffungsantrag ist die Anzahl der zu bearbeitenden Ecken zu ermitteln. Gemäß der der betrieblichen Absprache erfolgt

- eine Verdichtung im Landeswald auf das 2 km x 2 km Netz;
- Berlin wünschte ebenfalls eine 2 km x 2 km Aufnahme;
- Die Landesinventurleitung empfahl für den **restlichen Wald Brandenburgs** die Verdichtung auf 4 km x 4 km. Da aber weder von Seiten der Hoheit noch von den mit Klimaschutz befassten Abteilungen des Ministeriums ein Interesse an den Daten und einer Kostenübernahme geäußert wurde, wird die restliche Fläche im **8 km x 8 km** Netz aufgenommen.

Anhand der bei der BWI2022 und über den Kompletexport als csv von der Aufnahmedatenbank exportierte und in die Waldinv-DB importierten erfassten Ecken ergibt sich folgende Anzahl an Ecken:

| Eigentumsgruppe  | DB-Eckenzahl | TI-Meldung | Beschaffungsantrag |
| :--------------- | -----------: | ---------: | -----------------: |
| Landeswald BB    |         2857 |       3130 |                    |
| Berlin           |          180 |        176 |                    |
| anderes Eigentum |          558 |        712 |                    |
| Grenztrakte BB-B |              |         10 |                    |
| **Gesamt**       |         3595 |       4028 |                    |
| 5 % Kontrollen   |          180 |        202 |                    |

TODO:

- [ ] Differenzen erklären
- [ ] gültige Zahlen finden
- [ ] Beschaffungsantrag ergänzen

### Prüfung ob mit zusätzlichen Ecken weitere Ziele erreicht werden können

