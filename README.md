# BWI 2022

## Ziel und Zweck

Ziel ist die Dokumentation der Durchführung, nebst Durchführungskritik als auch die Erstellung und Dokumentation der Auswertung.

## git

### Neuer Branch

Wenn Änderungen vorgenommen wurden, die absehbar zu einem Konflikt mit anderen Mitarbeiterbranches führen könnten oder wenn die Änderungen noch nicht fertig sind, sollte ein neuer Branch erstellt werden. Dieser Branch wird dann mit dem Masterbranch zusammengeführt, wenn die Änderungen fertig sind.

#### Branch erstellen

```bash
git checkout -b neuer-branch
```

#### Branch hochladen

```bash
git push -u origin neuer-branch
```

### Head losgelöst von "Branchname"

Die Meldung "HEAD losgelöst von branchname" bedeutet, dass du dich in einem "detached HEAD" Zustand befindest. Das passiert, wenn du nicht auf einem Branch, sondern direkt auf einem bestimmten Commit oder einem Remote-Branch (z. B. origin/tw) arbeitest. In diesem Zustand sind deine Änderungen nicht mit einem Branch verknüpft, und es kann zu Problemen kommen, wenn du deine Arbeit speichern oder hochladen möchtest.
