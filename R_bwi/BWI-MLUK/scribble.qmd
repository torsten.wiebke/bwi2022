---
title: "BWI-MLUK VK"
#author: "Torsten Wiebke"
subtitle: "Diskussionsgrundlage zu den Daten der BWI 2022"
institution: "Landeskompetenzzentrum Forst Eberswalde"
author: "Sebastian Schmidt und Torsten Wiebke"
date: last-modified
date-format: "[Stand] M. D. YYYY"
bibliography: ../../LaTeX/bib/bibl.bib
csl: ../../LaTeX/bib/iso690-author-date-de.csl
lang: de
format:
  pdf:
    documentclass: scrreprt #Komascript reprt
    include-in-header:
      - text: |
          \usepackage{easy-todo}
    #pdf-engine: pdflatex 
    df-print: kable
    use-rsvg-convert: true
    #default-image-extension: png
    keep_tex: true
    
    # wenn nur PDF: in der Console: quarto::quarto_render("your_document.qmd", output_format = "pdf")
    # quarto::quarto_render("bwi_interpretationsworkshop.qmd", output_format = "revealjs")
  docx:
    toc: true
  html:
    toc: true
editor: visual
---

::: content-hidden
# librarys

{{< include librarys-datimport.qmd >}}

```{r}
library(kableExtra)
```
:::

# Diskussion zu Veröffentlichungen

-   einige Schwerpunkte auf Bund-Länderkonferenzen schon festgelegt ➯
    Auswertung Bund ➯ Detaillierung auf Landesebene?

## Vereinbarung Bund-Länder

### Von BIL und BMEL identifizierte Schwerpunkte

-   Klimakrise, Ausfall von Baumarten und Veränderungen der
    Naturnäheeinstufung
-   Fichtenrückgang
-   Verjüngung und Unterstand
-   Perspektive des neuen Waldes

### Digitales Waldomonitoring

-   FNEWs - Waldschadenskarte
-   Wald/Nichtwaldkarte nach BWI-Definition
-   Baumartenkarte
-   Biomassekarte

## Aktuell in Diskussion

-   Wald-Nichtwaldabgrenzung: Deckungsgrad, Mindestflächen werden wegen
    Klimakrise und Waldschäden unterschritten ➯ Flächenaussagen

-   Definition lebender, toter und gerade abgestorbener Bäume und die
    Bedeutung für Auswertung (Vorrat, Zuwachs, ...) - Wunsch nach
    Vitalitäserfassung (Sekundärkrone)

-   diverse digitale Produkte vorhanden - Umgang?

-   Kohlenstoffinventuren als Zwischenergebnisse - Zeitreihen

# Daten der BWI 2022 - Stand \< 11. 07. 2024

## Waldfläche

Entgegen der medialen Berichterstattung und der dadurch bedingten
»gefühlten Wahrheit« ist die Waldfläche in Brandenburg von 2012 bis 2022
weitstgehend gleich geblieben. Sie ist nach BWI 2022 um
**`r format(ver_waldfl_long %>% filter(Land == 'Brandenburg' & Kategorie == 'Wald') %>% select(Flächenänderung_ha) %>% pull(1) %>% round(1), big.mark = ".", decimal.mark = ",", scientific = FALSE)` ha**
gewachsen. Es wird empfohlen diesen Wert nicht zu kommunizieren da
Dieser einen hohen Stichprobenfehler hat (SE68 = 1707). Der wahre Wert
der Fläche liegt mit einer Wahrscheinlichkeit von 68% in einem Bereich
von 99,7 ha ± 1.707 ha. Da aber selbst ein Waldflächenverlust von 1607,3
ha nur
`r format(round(1607.3 / (waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Wald') %>% select(Fläche) %>% pull()) * 100,2), big.mark = ".", decimal.mark = ",", scientific = FALSE)`
% der Waldfläche entspricht, ist die Aussage »Waldfläche in Brandenburg
bleibt konstant« gerechtfertigt.

Brandenburg verfügt über eine Waldfläche von
**`r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Wald') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` ha**.
Forstwirtschaftlich unterteilt besteht diese aus:

-   »bestockter Holzboden«:
    `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'bestockter Holzboden') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` ha,
    das sind
    `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'bestockter Holzboden') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` %.
-   »Nichtholzboden«:
    `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Nichtholzboden') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` ha,
    das sind
    `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Nichtholzboden') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` %.
-   »Blößen«:
    `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Blöße') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` ha,
    das sind
    `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Blöße') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` %.\

| Waldspezifikation | ha | \% | Veränderungen 2012-2022 in ha |
|:---|:--:|:--:|---:|
| »bestockter Holzboden« | `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'bestockter Holzboden') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` | `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'bestockter Holzboden') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` | `r format(ver_waldfl_long %>% filter(Land == 'Brandenburg' & Kategorie == 'bestockter Holzboden') %>% select(Flächenänderung_ha) %>% pull() %>% round(1) , big.mark = ".", decimal.mark = ",", scientific = FALSE)` |
| »Nichtholzboden« | `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Nichtholzboden') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` | `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Nichtholzboden') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` | `r format(ver_waldfl_long %>% filter(Land == 'Brandenburg' & Kategorie == 'Nichtholzboden') %>% select(Flächenänderung_ha) %>% pull() %>% round(1) , big.mark = ".", decimal.mark = ",", scientific = FALSE)` |
| »Blößen« | `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Blöße') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` | `r format(waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Blöße') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` | `r format(ver_waldfl_long %>% filter(Land == 'Brandenburg' & Kategorie == 'Blöße') %>% select(Flächenänderung_ha) %>% pull() %>% round(1) , big.mark = ".", decimal.mark = ",", scientific = FALSE)` |
| **Wald** |  |  | `r format(ver_waldfl_long %>% filter(Land == 'Brandenburg' & Kategorie == 'Wald') %>% select(Flächenänderung_ha) %>% pull() %>% round(1) , big.mark = ".", decimal.mark = ",", scientific = FALSE)` |

: Waldfläche nach Kategorie {#tbl-WaldflaecheSpez}

```{r waldfl_brb, eval=FALSE, include=FALSE}
# Plot erstellen
ggplot((waldfl_spez_long %>% 
          filter(Land=='Brandenburg' & Kategorie != 'Wald' & Kategorie != 'Holzboden') 
        ), 
       aes(x = Land, y = Fläche, fill = reorder(Kategorie, Fläche, FUN = sum))) +
  geom_bar(stat = "identity", position = "stack")  +
  coord_flip() +
  labs(x = "Bundesland", y = "Fläche [ha]",  fill = "Kategorie") +
  geom_text(aes(label = paste0(round(Fläche, 0), " ha (", round(Prozent, 1), "%)")), 
             position = position_stack(vjust = 0.5), size = 3, color = "white") +
  ggtitle("Waldfläche in Brandenburg (2022)") +
  scale_y_continuous(breaks = seq(0, 1200000, by = 200000),
                     labels = dollar_format(
                              big.mark = ".",
                              decimal.mark = ",",
                              suffix = " ",
                              prefix = ""
  )
  ) +
  scale_fill_discrete(breaks = c("bestockter Holzboden", "Blöße", "Nichtholzboden")) 

```

```{r TreemapWaldspez, echo=FALSE}
ggplot((waldfl_spez_long %>% 
          filter(Land == 'Brandenburg' & Kategorie != 'Wald' & Kategorie != 'Holzboden') 
        ), aes(area = Fläche, fill = Kategorie, label = paste(Kategorie, round(Fläche), sep = "\n"))) +
  geom_treemap() +
  geom_treemap_text(colour = "white", place = "centre", #grow = TRUE, 
                    ) +
  scale_fill_brewer(palette = "Set3") +
  theme(legend.position = "right") +
  labs(title = "Treemap der Waldflächenspezifikation der BWI 2022 in Brandenburg")
```

### Veränderung gegenüber 2012

```{r ver-waldfl-bb-22, echo=FALSE}
ggplot((ver_waldfl_long %>%
          filter(Land == "Brandenburg" & !Kategorie %in% c("Wald", "Holzboden"))), 
       aes(x = reorder(Kategorie, `Flächenänderung_ha`), y = `Flächenänderung_ha`, fill = Kategorie)) +
  geom_col() +
  geom_text(aes(label = round(`Flächenänderung_ha`, 0)),
            position = position_stack(vjust = 0.5),  # Adjust position for placement inside bars
            size = 3, 
            color = "black",
            angle = 90) +
  labs(x = "Kategorie", y = "Veränderungen [ha]") +  # Achsenbeschriftungen festlegen
  ggtitle("Veränderungen der Waldfläche in Brandenburg von 2012-2022 \n Veränderung absolut: + 99,7 ha") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1),
        plot.title = element_text(hjust = 0.5),
        plot.title.position = "plot") +
  scale_fill_brewer(palette = "Greens") +  # Farbskala
  guides(fill = 'none')  # Legende entfernen
```

Veränderungen der Waldfläche v.a.

-   in der Abnahme von Nichtholzboden ⤍ Weniger Holzlagerplätze,
    Pflanzkämpe, Wildwiesen, ...?

-   in der Abnahme von Blößen ⤍ v.a. Sukzession auf ehemaligen
    Truppenübungsplätzen

-   eine Zunahme des bestockten Holzbodens

## Eigentum

-   Brandenburg bleibt Privatwaldland mit
    `r format(waldfl22_eig_long %>%     filter(Land == "Brandenburg" & Eigentumsart == 'Privatwald') %>% select(Prozent) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` %
    der Waldfläche
-   Eigentumsart »Öffentlicher Wald« verliert an Fläche
    `r format(ver_waldfl22_eig_long %>%     filter(Land == "Brandenburg" & Eigentumsart == 'Öffentlicher Wald') %>% select(Fläche) %>% pull(), big.mark = ".", decimal.mark = ",", scientific = FALSE)` ha
    (Stichprobenfehler ± 3.893 ha)

```{r waldfl_eig_gruppe_flextable, echo=FALSE}
waldfl22_eig_long %>%
    filter(Land == "Brandenburg" & Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")) %>%
  ungroup() %>%
    select(c(Eigentumsart, Fläche, Prozent)) %>%
left_join((ver_waldfl22_eig_long %>%
          filter(Land == "Brandenburg" & Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")) %>%
  select(c(Eigentumsart, Fläche)))
            , by = "Eigentumsart", suffix = c("", "_Veränderung")) %>%
  rename(Veränderung = Fläche_Veränderung)  %>%
  mutate(Veränderung = round(Veränderung, 0)) %>% 
  flextable() %>%
  colformat_double(big.mark = ".", decimal.mark = ",")
```

```{r waldfl_eig_gruppe_table, echo=FALSE}
waldfl22_eig_long %>%
    filter(Land == "Brandenburg" & Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")) %>%
  ungroup() %>%
    select(c(Eigentumsart, Fläche, Prozent)) %>%
left_join((ver_waldfl22_eig_long %>%
          filter(Land == "Brandenburg" & Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")) %>%
  select(c(Eigentumsart, Fläche)))
            , by = "Eigentumsart", suffix = c("", "_Veränderung")) %>%
  rename(Veränderung = Fläche_Veränderung)  %>%
  mutate(Veränderung = round(Veränderung, 0)) %>% 
  kbl() %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive"), full_width = F)
```

-   Privatwald über 20 bis 100 ha dominiert und nam zu
-   Privatwald bis 20 ha folgt und nam ab
-   Privatwald über 1000 ha ist die kleinste Gruppe und nahm geringfügig
    zu

```{r, echo=FALSE}
waldfl22_eig_long %>%
    filter(Land == "Brandenburg"& !Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")) %>%
  ungroup() %>%
    select(c(Eigentumsart, Fläche, Prozent)) %>%
  left_join((ver_waldfl22_eig_long %>%
          filter(Land == "Brandenburg"  & !Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")) %>%
  select(c(Eigentumsart, Fläche)))
            , by = "Eigentumsart", suffix = c("", "_Veränderung")) %>%
  rename(Veränderung = Fläche_Veränderung)  %>%
  mutate(Veränderung = round(Veränderung, 0)) %>% # Veränderung ohne Nachkommastellen
  kbl() %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive"), full_width = F)
```

```{r treemap_eig, echo=FALSE}
# Treemap erstellen
# Treemap erstellen
ggplot(waldfl22_eig_long %>%
    filter(Land == "Brandenburg"& !Eigentumsart %in% c("Öffentlicher Wald", "Privatwald")), aes(area = Fläche, fill = Eigentumsart, subgroup = Gruppe, label = paste(Eigentumsart, round(Prozent), "%"))) +
  geom_treemap() +
  geom_treemap_subgroup_border(colour = "white", size = 2) +
  geom_treemap_subgroup_text(place = "centre", grow = TRUE, alpha = 0.5, colour = "white", fontface = "italic", min.size = 0) +
  geom_treemap_text(colour = "black", place = "centre", grow = TRUE, reflow = TRUE) +
  scale_fill_brewer(palette = "Set3") +
  theme(legend.position = "bottom") +
  labs(title = "Eigentumsanteile der Waldflächen in Brandenburg (BWI 2022)")



```

```{r waldfl_eig_pie, echo=FALSE}
ggplot(
  waldfl22_eig_long %>%
    filter(Land == "Brandenburg" & !Eigentumsart %in% c("Öffentlicher Wald", "Privatwald, bis 20 ha",
                                                        "Privatwald, über 20 bis 1000 ha", "Privatwald, über 1000 ha")),
  aes(x = "", y = Prozent, fill = Eigentumsart)
) +
  geom_bar(width = 1, stat = "identity") +
  coord_polar("y") +
  labs(fill = "Eigentumsart") +
  theme_void() +
  theme(legend.position = "right") +
# Werte im Diagramm anzeigen
   geom_text(aes(label = paste(Eigentumsart,"\n", round(Prozent, 2), "%")), 
            position = position_stack(vjust = 0.5), 
            size = 3)  # Anpassung der Textgröße
```

```{r pie_privatwald, echo=FALSE}
ggplot(
  waldfl22_eig_long %>%
    filter(Land == "Brandenburg" & !Eigentumsart %in% c("Öffentlicher Wald", "Privatwald", "Staatswald (Land)", "Staatswald (Bund)", "Körperschaftswald")),
  aes(x = "", y = Prozent, fill = Eigentumsart)
) +
  geom_bar(width = 1, stat = "identity") +
  coord_polar("y") +
  labs(fill = "Eigentumsart") +
  theme_void() +
  theme(legend.position = "right") +
# Werte im Diagramm anzeigen
   geom_text(aes(label = paste(Eigentumsart,"\n", round(Prozent, 2), "%")), 
            position = position_stack(vjust = 0.5), 
            size = 3)  # Anpassung der Textgröße
```

```{r ver-waldfl_eig_plot, echo=FALSE}
ggplot((ver_waldfl22_eig_long %>%
          filter(Land == "Brandenburg" & !Eigentumsart %in% c("Öffentlicher Wald", "Privatwald"))), 
       aes(x = reorder(Eigentumsart, `Fläche`), y = `Fläche`, fill = Eigentumsart)) +
  geom_col() +
  geom_text(aes(label = round(`Fläche`, 0)),
            position = position_stack(vjust = 0.5),  # Adjust position for placement inside bars
            size = 3, 
            color = "black",
            angle = 90) +
  labs(x = "Kategorie", y = "Veränderungen [ha]") +  # Achsenbeschriftungen festlegen
  ggtitle("Veränderungen der Waldfläche nach Eigentumsart in Brandenburg von 2012-2022") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1),
        plot.title = element_text(hjust = 0.5),
        plot.title.position = "plot") +
  scale_fill_brewer(palette = "Greens") +  # Farbskala
  guides(fill = 'none')  # Legende entfernen
```

**Thesen:**

-   Staatswald (Bund) gab Flächen an Naturstiftungen ab
-   Stiftungen sind Privatrechtlich und zählen als Privatwald - daher
    Zunahme
-   Staatswald (Land) gab Flächen an andere Landnutzungen ab
-   Privatwald bis 20 ha zeitigt Eigentumsübergang
-   Körperschaftswald - Verkauf?
-   Privatwald über 1000 ha - Stiftungen und Zukäufe
-   Privatwald über 20 bis 1000 ha - Zuwachs durch Zukäufe und
    Stiftungen

## Baumartenzusammensetzung

```{r }
ggplot((waldfl22_ba_long %>%
  filter(Land == "Brandenburg" & !Ba_Wa %in% c("alle Nadelbäume", "alle Laubbäume")) %>%
  select(Ba_Wa, Fläche, Prozent)), aes(x = "", y = Fläche, fill = Ba_Wa)) +
  geom_bar(stat = "identity") +
  labs(fill = "Baumarten", y = "Fläche (ha)", x = "", title = "Flächenangaben der Baumarten in Brandenburg") +
  theme_minimal() +
  theme(axis.text.x = element_blank(), axis.ticks.x = element_blank(), legend.position = "right") +
  geom_text(aes(label = round(Fläche)), position = position_stack(vjust = 0.5), size = 3)
```

```{r}
waldfl22_ba_long %>%
  filter(Land == "Brandenburg" & !Ba_Wa %in% c("alle Nadelbäume", "alle Laubbäume")) %>%
  select(Ba_Wa, Fläche, Prozent)

```

```{r}
format(ver_waldfl_long %>% filter(Land == 'Brandenburg' & Kategorie == 'Blöße') %>% select(Flächenänderung_ha) %>% pull()  %>% 
  round(1) , big.mark = ".", decimal.mark = ",", scientific = FALSE)
```

::::: columns
::: {.column width="50%"}
![](pics/waldfl_bwi22_prozkreis.png)
:::

::: {.column width="50%"}
-   Kiefer dominiert immer noch - sinkt allerdings auf 68 %
-   Veränderungsdiagramm ...
-   nn
:::
:::::

### Eigentum

-   Privatwald dominiert immer noch
-   Eigentumsübergang in den Zahlen nur bedingt sichtbar

### Struktur

-   Mehrschichtige Bestände nehmen zu
-   Zuwachs v.a. in Laub-/Mischwäldern
-   Waldumbau ist langsam
-   Unterschiede in Eigentumsarten?

### Verjüngung

-   Was und wie kam in den letzten 10 Jahren - Bäume unter 4 m
-   Verjüngungskreis:
    -   mehr Laubholz
    -   weniger Verbiss
    -   Pflanzenzahlen nicht ausreichend

```{r}
round(1607.3 / (waldfl_spez_long %>% filter(Land=='Brandenburg' & Kategorie == 'Wald') %>% select(Fläche) %>% pull()) * 100,2)
```
