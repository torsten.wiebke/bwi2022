--WZP 

 --andere BA - Codes
 -- Pinus sylvestris: 20
 -- Fagus sylvativa: 100
 -- Quercus robur: 110
 -- Quercus petraea: 111

--alle Kiefern

select distinct wzp.tnr, 
       wzp.enr, 
       wzp.m_hoe, 
       wzp.m_hoe / 10.0 AS m_hoe_m, 
       wzp.ba, 
       x3.langd AS ba_name, 
       wzp.pk, 
       wzp.m_bhd,
       wzp.m_bhd / 10.0 as bhd_cm,
       wzp.al_ba, 
       geom.geom::geometry(Point, 25833)
FROM bwi_2022.b3v_wzp wzp
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = wzp.tnr 
    AND geom.enr = wzp.enr 
JOIN bwi_meta.x3_ba x3 
    ON wzp.ba = x3.icode
WHERE wzp.ba <= 29
  AND wzp.ba >= 20;