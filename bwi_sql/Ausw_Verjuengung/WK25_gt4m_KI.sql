--EBS Größer 4m
 
 --andere BA - Codes
 -- Pinus sylvestris: 20
 -- Fagus sylvativa: 100
 -- Quercus robur: 110
 -- Quercus petraea: 111
 
 --alle Kiefern
 -- Kopfzeile BWI EBS fehlt noch: WZP 1/2?, Anz.WZP4 Bäume

SELECT DISTINCT 
       gt4m.tnr, 
       gt4m.enr,
       gt4m.ba, 
       x3.langd AS ba_name, 
       gt4m.anz,
       gt4m.schi,
       schi.langd AS schicht,
       gt4m.sp,
       sp.langd as spiegelungsart,
       gt4m.g_ha,
       gt4m.nr, 
       geom.geom::geometry(Point, 25833),
       (SELECT SUM(gt4m_sub.anz) 
        FROM bwi_2022.b3v_gt4m_ba gt4m_sub
        WHERE gt4m_sub.tnr = gt4m.tnr 
          AND gt4m_sub.enr = gt4m.enr) AS "anz_Baeume_WZP_1/2"
FROM bwi_2022.b3v_gt4m_ba gt4m
JOIN bwi_Admin.lospunkt geom 
    ON geom.tnr = gt4m.tnr 
    AND geom.enr = gt4m.enr 
JOIN bwi_meta.x3_ba x3 
    ON gt4m.ba = x3.icode
JOIN bwi_meta.x3_schi schi
    ON gt4m.schi = schi.icode
JOIN bwi_meta.x3_sp sp
    ON gt4m.sp = sp.icode
WHERE gt4m.ba <= 29
  AND gt4m.ba >= 20
ORDER BY gt4m.tnr, gt4m.enr;