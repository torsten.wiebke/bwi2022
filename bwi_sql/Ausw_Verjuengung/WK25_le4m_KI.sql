--EBS kleiner 4m
 
--andere BA - Codes
 -- Pinus sylvestris: 20
 -- Fagus sylvativa: 100
 -- Quercus robur: 110
 -- Quercus petraea: 111

--alle Kiefern
select distinct le4m.tnr, 
       le4m.enr, 
       le4m.nr, 
     --  le4m.m_hoe / 10.0 AS m_hoe_m, 
       le4m.ba, 
       x3.langd AS ba_name, 
       le4m.anteil, 
       le4m.vart,
       le4m.vart as Verjuengungsart,
       geom.geom::geometry(Point, 25833)
FROM bwi_2022.b3v_le4m_ba le4m
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = le4m.tnr 
    AND geom.enr = le4m.enr 
JOIN bwi_meta.x3_ba x3 
    ON le4m.ba = x3.icode
join bwi_meta.x3_vart vart
	on le4m.vart = vart.icode
WHERE le4m.ba <= 29
  AND le4m.ba >= 20;
 
 
 -- -- bwi_2022.b3v_ecke_feld schile4_bedg -- Info Bedeckungsgrad Ecke EBS bis 4m
 
 SELECT DISTINCT 
       le4m.tnr, 
       le4m.enr,
       feld.schile4_bedg AS bedg_Ecke_Gesamt,
       le4m.nr, 
       le4m.ba, 
       x3.langd AS ba_name, 
       le4m.anteil, 
       le4m.vart,
       vart.langd AS verjuengungsart,
       geom.geom::geometry(Point, 25833)
FROM bwi_2022.b3v_le4m_ba le4m
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = le4m.tnr 
    AND geom.enr = le4m.enr 
JOIN bwi_meta.x3_ba x3 
    ON le4m.ba = x3.icode
JOIN bwi_meta.x3_vart vart
    ON le4m.vart = vart.icode
JOIN bwi_2022.b3v_ecke_feld feld  
    ON feld.tnr = le4m.tnr  
    AND feld.enr = le4m.enr  
WHERE le4m.ba <= 29
  AND le4m.ba >= 20
order by le4m.tnr, le4m.enr;

--Test alle Reihen, kann ja nur mehrere Reihen pro Ecke sein, wenn mehrere Kiefern...
SELECT 
       le4m.tnr, 
       le4m.enr,
       feld.schile4_bedg AS bedg_Ecke_Gesamt,
       le4m.nr, 
       le4m.ba, 
       x3.langd AS ba_name, 
       le4m.anteil, 
       le4m.vart,
       vart.langd AS verjuengungsart,
       geom.geom::geometry(Point, 25833)
FROM bwi_2022.b3v_le4m_ba le4m
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = le4m.tnr 
    AND geom.enr = le4m.enr 
JOIN bwi_meta.x3_ba x3 
    ON le4m.ba = x3.icode
JOIN bwi_meta.x3_vart vart
    ON le4m.vart = vart.icode
JOIN bwi_2022.b3v_ecke_feld feld  
    ON feld.tnr = le4m.tnr  
    AND feld.enr = le4m.enr  
WHERE le4m.ba <= 29
  AND le4m.ba >= 20
ORDER BY le4m.tnr, le4m.enr, le4m.nr;