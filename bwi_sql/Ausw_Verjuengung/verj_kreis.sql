-- New script in projekt_lfb.
-- Date: 21.01.2025
-- Time: 13:30:27
-- Database: projekt_lfb            - target database name

/* Auswertung Verjüngungskreise
 * 
 * Wo liegen die Daten?
 * bwi_2022.b3v_ecke_feld
 * bwi_2022.b3v_b0
 */

-- b3v_ecke_feld
select bvef.tnr, bvef.enr,
bvef.b0_bs,
xb.langd ,
bvef.b0_zaun, -- boolean
bvef.b0_move,
xpmm.langd ,
bvef.b0_radius, -- Radius [m] des Probekreises für Bäume der Baumgröße 0 {1 oder 2 m}
bvef.b0_hori -- Horizontalentfernung [cm] der Probekreise r=1 / 2 m (Baumgrößen 0-6)
from bwi_2022.b3v_ecke_feld bvef
join bwi_meta.x3_p1m_move xpmm on xpmm.icode = bvef.b0_move
join bwi_meta.x3_bs xb on xb.icode = bvef.b0_bs 

/* 1. bwi_2022.b3v_b0
 * 
 */
--
select 
--*
distinct(sonst) -- schutz ist 0 oder 1, sonst ist 0
from bwi_2022.b3v_b0 bvb 

select distinct vbl -- verdichtungsgebiete im bundesland
from bwi_2022.b3v_ecke_feld bvef 

-- Verbissprozent, Pflanzendichte b0
-- select b0 mit radius
select 
bvb.*,
bvef.b0_radius 
--count(*) -- 9156 mit join und left join
from bwi_2022.b3v_b0 bvb 
join bwi_2022.b3v_ecke_feld bvef on bvb.tnr = bvef.tnr and bvb.enr=bvef.enr

-- verbissprozent bagr zum Vergleich mit bwi.info - braucht Eingrenzung auf Brandenburg
select tnr 
from bwi_2022.b3f_ecke_vorkl bfev 
where bfev.bl = 12

-- Versuch 1
select distinct 
--count(*)--bvb.*,
--bvef.b0_radius,
--bvb.ba,
--ba.kurzd ,
ba.bagr, 
bvb.biss,
sum(bvb.anz) over (partition by ba.bagr),
sum(bvb.anz) over () as gesamt,
round((sum(bvb.anz) over (partition by ba.bagr)::numeric  / sum(bvb.anz) over () * 100),2) as Prozent
from bwi_2022.b3v_b0 bvb 
join bwi_2022.b3v_ecke_feld bvef on bvb.tnr = bvef.tnr and bvb.enr=bvef.enr
join bwi_meta.x3_ba ba on ba.icode = bvb.ba 
join bwi_2022.b3f_ecke_vorkl bfev on bfev.tnr = bvb.tnr and bfev.enr = bvb.enr 
where bfev.bl=12 -- weiß nicht ob das die geeignetste Einschränkung für Brandenburg ist
group by ba.bagr, bvb.biss, bvb.anz

select count(enr)
--sum(bvb.anz) -- 19967, 19643 für Brandeburg erscheint realistisch
from  bwi_2022.b3v_b0 bvb


-- Verbissprozent über die bagr
SELECT 
    DISTINCT ba.bagr,
    bvb.biss,
    SUM(bvb.anz) OVER (PARTITION BY ba.bagr, bvb.biss) AS sum_bvb_biss, -- Summe der Pflanzen für bagr und biss
    SUM(bvb.anz) OVER (PARTITION BY ba.bagr) AS sum_bvb_bagr, -- Gesamtsumme für bagr
    SUM(bvb.anz) OVER () AS gesamt, -- Gesamtsumme über alle Gruppen
    ROUND(SUM(bvb.anz) OVER (PARTITION BY ba.bagr, bvb.biss) * 100.0 / SUM(bvb.anz) OVER (), 2) AS gesamt_prozent, -- Anteil an der Gesamtsumme
    ROUND(SUM(bvb.anz) OVER (PARTITION BY ba.bagr, bvb.biss) * 100.0 / SUM(bvb.anz) OVER (PARTITION BY ba.bagr), 2) AS prozent_bagr_biss, -- Anteil innerhalb bagr
    --ROUND(SUM(bvb.anz) OVER (PARTITION BY bvb.biss) * 100.0 / SUM(bvb.anz) OVER (), 2) AS prozent_biss, -- Anteil innerhalb bagr
    count(bvb.enr) over (PARTITION BY ba.bagr, bvb.biss) as Eckenanzahl
FROM 
    bwi_2022.b3v_b0 bvb
JOIN 
    bwi_2022.b3v_ecke_feld bvef 
    ON bvb.tnr = bvef.tnr AND bvb.enr = bvef.enr
JOIN 
    bwi_meta.x3_ba ba 
    ON ba.icode = bvb.ba
JOIN 
    bwi_2022.b3f_ecke_vorkl bfev 
    ON bfev.tnr = bvb.tnr AND bfev.enr = bvb.enr
WHERE 
    bfev.bl = 12 -- Einschränkung auf Brandenburg
--group by rollup (biss,bagr, bvb.anz)
ORDER BY 
    ba.bagr NULLS FIRST, bvb.biss NULLS FIRST;
/* Ergibt
 |bagr|biss|sum_bvb_biss|sum_bvb_bagr|gesamt|gesamt_prozent|prozent_bagr_biss|eckenanzahl|
|----|----|------------|------------|------|--------------|-----------------|-----------|
|ALH |0   |2.072       |3.225       |19.643|10,55         |64,25            |820        |
|ALH |1   |537         |3.225       |19.643|2,73          |16,65            |270        |
|ALH |3   |115         |3.225       |19.643|0,59          |3,57             |68         |
|ALH |4   |501         |3.225       |19.643|2,55          |15,53            |246        |
|ALN |0   |4.510       |6.483       |19.643|22,96         |69,57            |1.833      |
|ALN |1   |781         |6.483       |19.643|3,98          |12,05            |401        |
|ALN |3   |189         |6.483       |19.643|0,96          |2,92             |135        |
|ALN |4   |1.003       |6.483       |19.643|5,11          |15,47            |494        |
|BU  |0   |903         |1.159       |19.643|4,6           |77,91            |394        |
|BU  |1   |79          |1.159       |19.643|0,4           |6,82             |60         |
|BU  |3   |73          |1.159       |19.643|0,37          |6,3              |56         |
|BU  |4   |104         |1.159       |19.643|0,53          |8,97             |71         |
|DGL |0   |40          |42          |19.643|0,2           |95,24            |26         |
|DGL |1   |1           |42          |19.643|0,01          |2,38             |1          |
|DGL |3   |1           |42          |19.643|0,01          |2,38             |1          |
|EI  |0   |2.569       |4.616       |19.643|13,08         |55,65            |1.430      |
|EI  |1   |460         |4.616       |19.643|2,34          |9,97             |341        |
|EI  |3   |370         |4.616       |19.643|1,88          |8,02             |257        |
|EI  |4   |1.217       |4.616       |19.643|6,2           |26,36            |763        |
|FI  |0   |190         |205         |19.643|0,97          |92,68            |91         |
|FI  |1   |2           |205         |19.643|0,01          |0,98             |2          |
|FI  |3   |7           |205         |19.643|0,04          |3,41             |4          |
|FI  |4   |6           |205         |19.643|0,03          |2,93             |6          |
|KI  |0   |3.642       |3.892       |19.643|18,54         |93,58            |1.030      |
|KI  |1   |109         |3.892       |19.643|0,55          |2,8              |79         |
|KI  |3   |56          |3.892       |19.643|0,29          |1,44             |37         |
|KI  |4   |85          |3.892       |19.643|0,43          |2,18             |62         |
|LAE |0   |6           |6           |19.643|0,03          |100              |6          |
|TA  |0   |11          |15          |19.643|0,06          |73,33            |8          |
|TA  |1   |1           |15          |19.643|0,01          |6,67             |1          |
|TA  |3   |1           |15          |19.643|0,01          |6,67             |1          |
|TA  |4   |2           |15          |19.643|0,01          |13,33            |2          |

TODO: Warum sind die Werte zu bwi.info verschieden?
*/

-- verbissprozent über alles
SELECT 
    DISTINCT 
    bvb.biss,
    SUM(bvb.anz) OVER (PARTITION BY bvb.biss) AS sum_bvb_biss, -- Summe der Pflanzen für bagr und biss
    SUM(bvb.anz) OVER () AS gesamt, -- Gesamtsumme über alle Gruppen
    ROUND(SUM(bvb.anz) OVER (PARTITION BY bvb.biss) * 100.0 / SUM(bvb.anz) OVER (), 2) AS gesamt_prozent, -- Anteil an der Gesamtsumme
    count(bvb.enr) over (PARTITION BY bvb.biss)
FROM 
    bwi_2022.b3v_b0 bvb
JOIN 
    bwi_2022.b3v_ecke_feld bvef 
    ON bvb.tnr = bvef.tnr AND bvb.enr = bvef.enr
JOIN 
    bwi_meta.x3_ba ba 
    ON ba.icode = bvb.ba
JOIN 
    bwi_2022.b3f_ecke_vorkl bfev 
    ON bfev.tnr = bvb.tnr AND bfev.enr = bvb.enr
WHERE 
    bfev.bl = 12 -- Einschränkung auf Brandenburg
--group by rollup (biss,bagr, bvb.anz)
ORDER BY 
    bvb.biss NULLS FIRST;  
/* Kommt auf
|biss|sum_bvb_biss|gesamt|gesamt_prozent|count|
|----|------------|------|--------------|-----|
|0   |13.943      |19.643|70,98         |5.638|
|1   |1.970       |19.643|10,03         |1.155|
|3   |812         |19.643|4,13          |559  |
|4   |2.918       |19.643|14,86         |1.644|

TODO: Warum sind die Werte zu bwi.info verschieden? Liegt das an der Bundeslandzuordnung?
**/

   -- Verbissprozent an der Ecke über die bagr
SELECT 
--    DISTINCT 
    bei.tnr,
    bei.enr,
    bei.ist_geom ,
    ba.bagr,
    bvb.ba,
    bvb.biss,
    bvb.anz 
--    SUM(bvb.anz) OVER (PARTITION BY ba.bagr, bvb.biss) AS sum_bvb_biss, -- Summe der Pflanzen für bagr und biss
--    SUM(bvb.anz) OVER (PARTITION BY ba.bagr) AS sum_bvb_bagr, -- Gesamtsumme für bagr
--    SUM(bvb.anz) OVER () AS gesamt, -- Gesamtsumme über alle Gruppen
--    ROUND(SUM(bvb.anz) OVER (PARTITION BY ba.bagr, bvb.biss) * 100.0 / SUM(bvb.anz) OVER (), 2) AS gesamt_prozent, -- Anteil an der Gesamtsumme
--    ROUND(SUM(bvb.anz) OVER (PARTITION BY ba.bagr, bvb.biss) * 100.0 / SUM(bvb.anz) OVER (PARTITION BY ba.bagr), 2) AS prozent_bagr_biss, -- Anteil innerhalb bagr
    --ROUND(SUM(bvb.anz) OVER (PARTITION BY bvb.biss) * 100.0 / SUM(bvb.anz) OVER (), 2) AS prozent_biss, -- Anteil innerhalb bagr
--    count(bvb.enr) over (PARTITION BY ba.bagr, bvb.biss) as Eckenanzahl
FROM 
    bwi_2022.b3v_b0 bvb
JOIN 
    bwi_2022.b3v_ecke_feld bvef 
    ON bvb.tnr = bvef.tnr AND bvb.enr = bvef.enr
JOIN 
    bwi_meta.x3_ba ba 
    ON ba.icode = bvb.ba
JOIN 
    bwi_2022.b3f_ecke_vorkl bfev 
    ON bfev.tnr = bvb.tnr AND bfev.enr = bvb.enr
join bwi_r.bwi2022_ecke_istgeom bei on bei.tnr = bvb.tnr and bei.enr=bvb.enr
WHERE 
    bfev.bl = 12 -- Einschränkung auf Brandenburg
--group by rollup (biss,bagr, bvb.anz)
ORDER BY 
    ba.bagr NULLS FIRST, bvb.biss NULLS FIRST;

/* b1 -6
* sollte man die Tabellen nicht zusammenbringen? siehe bwi_rel/bwi_rel_anlage.sql
*/

select
bvb.*,
j.b0_bs,
j.b0_zaun,
j.b0_radius,
bei.ist_geom
from bwi_r.jungpfl_22 bvb
JOIN 
    bwi_r.jung_22 j  
    ON bvb.tnr = j.tnr AND bvb.enr = j.enr
JOIN 
    bwi_meta.x3_ba ba 
    ON ba.icode = bvb.ba
join bwi_r.bwi2022_ecke_istgeom bei on bei.tnr = bvb.tnr and bei.enr=bvb.enr

with jungpfl as
(select 
h3_lat_lng_to_cell(bei.ist_geom,7) as h3_index,
bvb.ba,
bvb.gr,
bvb.biss,
bvb.schael,
bvb.schu,
bvb.anz, 
j.b0_bs,
j.b0_zaun,
j.b0_radius
from bwi_r.jungpfl_22 bvb
JOIN 
    bwi_r.jung_22 j  
    ON bvb.tnr = j.tnr AND bvb.enr = j.enr
JOIN 
    bwi_meta.x3_ba ba 
    ON ba.icode = bvb.ba
join bwi_r.bwi2022_ecke_istgeom bei on bei.tnr = bvb.tnr and bei.enr=bvb.enr)
select 
jungpfl.h3_index,
jungpfl.biss,
jungpfl.ba,
SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.h3_index, jungpfl.biss) AS sum_biss, -- Summe der Pflanzen für h3_index, bagr und biss
SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.ba) AS sum_jungpfl_ba, -- Gesamtsumme für bagr
SUM(jungpfl.anz) OVER () AS gesamt, -- Gesamtsumme über alle Gruppen
ROUND(SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.h3_index, jungpfl.ba, jungpfl.biss) * 100.0 / SUM(jungpfl.anz) OVER (), 2) AS gesamt_prozent_h3, -- Anteil an der Gesamtsumme
ROUND(SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.ba, jungpfl.biss) * 100.0 / SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.ba), 2) AS prozent_bagr_biss, -- Anteil innerhalb bagr
ROUND(SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.biss) * 100.0 / SUM(jungpfl.anz) OVER (), 2) AS prozent_biss -- Anteil innerhalb bagr
from jungpfl
;

-- versuch 2
WITH jungpfl AS (
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index, -- H3-Index der Ebene 7
        bvb.ba,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz, 
        j.b0_bs,
        j.b0_zaun,
        j.b0_radius
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_r.jung_22 j  
        ON bvb.tnr = j.tnr AND bvb.enr = j.enr
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
)
SELECT distinct
    jungpfl.h3_index,
    jungpfl.biss,
    jungpfl.ba,
    SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.h3_index, jungpfl.biss) AS sum_biss, -- Summe der Pflanzen für h3_index und biss
    SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.h3_index, jungpfl.ba) AS sum_ba_h3,  -- Summe der Pflanzen für h3_index und ba
    SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.ba) AS sum_jungpfl_ba,               -- Gesamtsumme für ba
    SUM(jungpfl.anz) OVER () AS gesamt,                                             -- Gesamtsumme über alle Gruppen
    ROUND(SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.h3_index, jungpfl.ba, jungpfl.biss) * 100.0 / 
          SUM(jungpfl.anz) OVER (), 2) AS gesamt_prozent_h3, -- Anteil an der Gesamtsumme pro h3_index, ba und biss
    ROUND(SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.ba, jungpfl.biss) * 100.0 / 
          SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.ba), 2) AS prozent_ba_biss,  -- Anteil innerhalb ba und biss
    ROUND(SUM(jungpfl.anz) OVER (PARTITION BY jungpfl.biss) * 100.0 / 
          SUM(jungpfl.anz) OVER (), 2) AS prozent_biss -- Anteil innerhalb biss über alle Gruppen
FROM 
    jungpfl
group by jungpfl.h3_index,jungpfl.biss,jungpfl.ba, jungpfl.anz
ORDER BY 
    h3_index, biss, ba;

-- Aggregierung
WITH jungpfl AS (
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index, -- H3-Index der Ebene 7
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz, 
        j.b0_bs,
        j.b0_zaun,
        j.b0_radius
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_r.jung_22 j  
        ON bvb.tnr = j.tnr AND bvb.enr = j.enr
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
),
aggregated_data AS (
    SELECT 
        h3_index,
        biss,
        ba,
        bagr,
        gr,
        SUM(anz) over (partition by ba,biss) AS sum_ba,
        SUM(anz) over (partition by ba,biss) AS sum_ba_biss,
        SUM(anz) over (partition by bagr, biss) AS sum_bagr_biss,
        ROUND(SUM(anz) * 100.0 / SUM(anz) OVER (), 2) AS gesamt_prozent_h3 -- Anteil an der Gesamtsumme pro h3_index, ba und biss
    FROM 
        jungpfl
    GROUP BY 
        h3_index, ba, bagr,gr,biss,  anz
)
SELECT distinct
    aggregated_data.*,
    h3_cell_to_boundary(aggregated_data.h3_index)::geometry(polygon,4326) AS hexagon_geom, -- Polygon hinzufügen
    ROW_NUMBER() OVER (ORDER BY aggregated_data.h3_index, aggregated_data.biss, aggregated_data.ba) AS id -- ID erzeugen
FROM 
    aggregated_data
where h3_index = '871f1ddb6ffffff'
    ORDER BY 
    h3_index, biss, ba
;

WITH jungpfl AS (
    SELECT 
    	bvb.tnr,
    	bvb.enr,
        bei.ist_geom,
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index, -- H3-Index der Ebene 7
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz, 
        j.b0_bs,
        j.b0_zaun,
        j.b0_radius
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_r.jung_22 j  
        ON bvb.tnr = j.tnr AND bvb.enr = j.enr
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
),
aggregated_data AS (
    SELECT 
        h3_index,
        ist_geom,
        tnr,
        enr,
        ba,
        bagr,
        biss,
        gr,
        SUM(anz) over (partition by tnr,enr, ba) as sum_ba_tnr_enr, -- anzahl bäume, baumart pro ecke
        round(SUM(anz) over (partition by tnr,enr, ba,biss,gr)::numeric  / SUM(anz) over (partition by tnr,enr) *100,2) as ba_anteil, 
--        b0_radius,
--        pi() * b0_radius ^ 2 as plotarea, -- A = pi x r ²
        round(SUM(anz) over (partition by tnr,enr, ba)::numeric  / (pi() * b0_radius ^ 2)::numeric,2) as pflanzendichte_ba_qm,
        round(SUM(anz) over (partition by tnr,enr, bagr)::numeric  / (pi() * b0_radius ^ 2)::numeric,2) as pflanzendichte_bagr_qm,
        SUM(anz) over (partition by tnr,enr, ba, biss) as sum_ba_tnr_enr_biss,
        round(
        	SUM(anz) over (partition by tnr,enr, ba, biss)::numeric  / sum(anz) over (partition by ba, tnr, enr) *100
        		,2) 
        		as ba_anteil_ba_biss
--        SUM(anz) over (partition by tnr,enr, ba,biss) AS sum_ba_tnr_enr -- Summe aller Bäume mit Bisswert in der Ecke
--        SUM(anz) over (partition by tnr, ba,biss) AS sum_ba_tnr, -- Summe aller Bäume mit Bisswert im Transekt
--        sum(anz) over (partition by ba) as sum_ba, -- Summe aller Bäume
--        SUM(anz) over (partition by bagr, biss) AS sum_bagr_biss -- Summe aller Bäume der Baumartengurppe mit Bisswert
--        ROUND(SUM(anz) * 100.0 / SUM(anz) OVER (), 2) AS gesamt_prozent -- Anteil an der Gesamtsumme pro h3_index, ba und biss
    FROM 
        jungpfl
--    GROUP BY 
--        h3_index, ba, bagr,gr,biss,  anz
)
SELECT 
--distinct
    aggregated_data.*
--    h3_cell_to_boundary(aggregated_data.h3_index)::geometry(polygon,4326) AS hexagon_geom, -- Polygon hinzufügen
--    ROW_NUMBER() OVER (ORDER BY aggregated_data.h3_index, aggregated_data.biss, aggregated_data.ba) AS id -- ID erzeugen
FROM 
    aggregated_data
where
tnr = 37417 and enr = 1 and ba = 100
--h3_index = '871f1ddb6ffffff'
--ORDER BY 
--h3_index, biss, ba
;

/* 
 * Ziel:
 * Erstellung eines Views mit den relevanten Ergebnissen auf h3-7 aggregiert zur weiteren Auswertung
 */
-- Zusammenfassen auf Verbiss = 0 und Verbiss zwischen 1 und 4, Abbildung der Baumartenanteile
drop view bwi_r.v_jungpfl_h3_7;

--create view bwi_r.v_jungpfl_h3_7 as
WITH plotareas AS (
    -- Berechnung der Gesamtfläche der Plots pro h3_index7
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7,
        SUM(pi() * j.b0_radius^2)::numeric  AS total_plot_area -- Summe der Flächeninhalte
    FROM 
        bwi_r.jung_22 j
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON j.tnr = bei.tnr AND j.enr = bei.enr
    WHERE 
        bei.bl_bwi = '12'
    GROUP BY 
        h3_lat_lng_to_cell(bei.ist_geom, 7)
),
jungpfl AS (
    -- Auswahl der Baumdaten
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7,
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
    WHERE 
        bei.bl_bwi = '12'
)
SELECT 
    jp.h3_index7,
    xb.kurzd,
    CASE 
        WHEN jp.gr BETWEEN 0 AND 1 THEN '0-1' -- Zusammenfassung der Größenklassen 0 und 1
        WHEN jp.gr BETWEEN 2 AND 9 THEN '2-9' -- Zusammenfassung der Größenklassen 2 bis 9
        ELSE 'Andere' -- Optional: Falls andere Werte auftreten
    END AS gr_group, -- Neue Gruppierung der Größenklassen
    SUM(jp.anz) AS anz_ba, -- Gesamtanzahl der Bäume im Hexagon
    ROUND(SUM(jp.anz)::numeric / SUM(SUM(jp.anz)) OVER (PARTITION BY jp.h3_index7) * 100, 2) AS perc_ba, -- Prozentanteil der Baumart
    ROUND(SUM(CASE WHEN jp.biss = 0 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss0,
    ROUND(SUM(CASE WHEN jp.biss BETWEEN 1 AND 4 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss_1_4,
    SUM(CASE WHEN jp.schu = 1 THEN jp.anz ELSE 0 END) AS ba_anz_schutz,
    SUM(CASE WHEN jp.schael = 1 THEN jp.anz ELSE 0 END) AS ba_anz_schael,
    ROUND(SUM(CASE WHEN jp.biss = 0 THEN jp.anz ELSE 0 END)::numeric / pa.total_plot_area, 2) * 10000 AS pfldichte_verb0_ba_ha, -- Pflanzendichte in Bäumen/ha
    ROUND(SUM(jp.anz)::numeric / pa.total_plot_area, 2) * 10000 AS pfldichte_ba_ha, -- Pflanzendichte in Bäumen/ha
    ROUND(sum(SUM(jp.anz)) over (partition by jp.h3_index7) ::numeric / pa.total_plot_area, 2) * 10000 AS pfldichte_ha
FROM 
    jungpfl jp
JOIN 
    plotareas pa 
    ON jp.h3_index7 = pa.h3_index7
JOIN 
    bwi_meta.x3_ba xb 
    ON xb.icode = jp.ba
GROUP BY 
    jp.h3_index7, jp.gr,xb.kurzd, pa.total_plot_area;

