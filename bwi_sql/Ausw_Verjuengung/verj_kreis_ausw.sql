-- New script in projekt_lfb.
-- Date: 24.01.2025
-- Time: 22:38:04
-- Database: projekt_lfb            - target database name

/*
 * Kannst Du mir die Baumartenanteile, 
 * gern auch gleich als Grafik für die 
 * Trauben- 111
 * und Stieleiche 110
 * aus der BWI (Oberstand) und 
 * Verjüngung (ggf. auch Grafik aus VWM) an 
 * meine HU-Adresse ralf.kaetzel@hu-berlin.de schicken und 
 * auf das Laufwerk O:\ABT4\1_Org\Veranstaltungen\Waldkolloquium\2025 Jubilaeum 20 Jahre\Beiträge_Impulse_für_Vorträge\4_Eiche stellen. 
 * Jens Schröder benötigt das auch für die 
 * Kiefer 20
 * und Eric Turm für die Buche 100
 * . Das könnte dann auch auf die parallelen Ordner …Buche; resp. Kiefer.
 */
-- von bwi_r.v_jungpfl_h3_7

-- Ausgangsquery
select vjh.*
from bwi_r.v_jungpfl_h3_7 vjh 
join bwi_meta.x3_ba xb on vjh.ba = xb.icode 
join bwi_meta.x3_gr xg on vjh.gr = xg.icode 
join bwi_meta.x3_verbiss xv on vjh.biss = xv.icode 
--where vjh.ba 
--= 110
----in (
----908, -- Ei spec
----110,
----111
----)

-- jungkr
select xb.kurzd, j2.gr, xg.kurzd as grd, j2.biss, j2.anz
--,j2.*
from bwi_r.jung_22 j 
join bwi_r.jungpfl_22 j2 on j.tnr=j2.tnr and j.enr=j2.enr 
join bwi_meta.x3_ba xb on j2.ba = xb.icode
join bwi_meta.x3_gr xg on j2.gr = xg.icode

-- jungkr-dichte
select 
j2.tnr, j2.enr, 
xb.kurzd, j2.gr, xg.kurzd as grd, j2.biss, 
j2.anz,
sum(j2.anz) over (partition by j2.tnr, j2.enr) bäume_pro_ecke,
sum(j2.anz) over (partition by j2.tnr, j2.enr) / (pi() * j.b0_radius ^ 2)::numeric as pflanzendichte_qm,
sum(j2.anz) over (partition by j2.tnr, j2.enr,xb.kurzd) bäume_pro_baumartundecke,
sum(j2.anz) over (partition by j2.tnr, j2.enr,xb.kurzd) bäume_pro_baumartundecke,
j.b0_zaun,
j.b0_radius  
--,j2.*
from bwi_r.jung_22 j 
join bwi_r.jungpfl_22 j2 on j.tnr=j2.tnr and j.enr=j2.enr 
join bwi_meta.x3_ba xb on j2.ba = xb.icode
join bwi_meta.x3_gr xg on j2.gr = xg.icode

-- jungkr-dichte 1
SELECT 
    j2.tnr, 
    j2.enr, 
    xb.kurzd, 
    j2.gr, 
    xg.kurzd AS grd, 
    j2.biss, 
    j2.anz,
    j.b0_zaun,
    j.b0_radius  
FROM 
    bwi_r.jung_22 j 
JOIN 
    bwi_r.jungpfl_22 j2 ON j.tnr = j2.tnr AND j.enr = j2.enr 
JOIN 
    bwi_meta.x3_ba xb ON j2.ba = xb.icode
JOIN 
    bwi_meta.x3_gr xg ON j2.gr = xg.icode;

-- jungkr-dichte 2
WITH aggregated AS (
    SELECT 
        j2.tnr, 
        xb.kurzd, 
        j2.gr, 
        xg.kurzd AS grd, 
        j2.biss, 
        j.b0_zaun,
        sum(j2.anz) AS sum_anz,
        sum((pi() * j.b0_radius ^ 2)::numeric) AS fläche
    FROM 
        bwi_r.jung_22 j 
    JOIN 
        bwi_r.jungpfl_22 j2 ON j.tnr = j2.tnr AND j.enr = j2.enr 
    JOIN 
        bwi_meta.x3_ba xb ON j2.ba = xb.icode
    JOIN 
        bwi_meta.x3_gr xg ON j2.gr = xg.icode
    GROUP BY 
        j2.tnr, xb.kurzd, j2.gr, xg.kurzd, j2.biss, j.b0_zaun
)
SELECT 
    *,
    sum(sum_anz) over (partition by tnr,kurzd,gr, biss),
    sum(sum_anz) over (partition by tnr,kurzd,gr, biss) / fläche * 10000 AS pflanzendichte_baumart_gr_biss_trakt
FROM aggregated;

-- Gesamt Ei
select 
--vjh.*
h3_cell_to_boundary_geometry(vjh.h3_index7),
sum(anz_ba),
avg(perc_ba) durchschnanteil
--sum(anz_ba_gr_biss) --7290 Eichen insgesamt
from bwi_r.v_jungpfl_h3_7 vjh 
join bwi_meta.x3_ba xb on vjh.kurzd = xb.kurzd 
--join bwi_meta.x3_gr xg on vjh.gr = xg.icode 
--join bwi_meta.x3_verbiss xv on vjh.biss = xv.icode
where vjh.kurzd in ('TEI', 'SEI', 'Ei')
group by vjh.h3_index7
--group by vjh.h3_index7,
--xb.kurzd
--order by vjh.h3_index7

-- mit gewichtetem Durchschnitt
SELECT 
    h3_cell_to_boundary_geometry(vjh.h3_index7) AS hexagon_geom,
    SUM(vjh.anz_ba) AS total_anz_ba, -- Gesamtanzahl der Bäume im Hexagon
    AVG(vjh.perc_ba) AS avg_perc_ba, -- Durchschnittlicher Anteil der Eichen (arithmetisch)
    SUM(vjh.perc_ba * vjh.anz_ba) / SUM(vjh.anz_ba) AS weighted_avg_perc_ba -- Gewichteter Anteil der Eichen
FROM 
    bwi_r.v_jungpfl_h3_7 vjh
JOIN 
    bwi_meta.x3_ba xb 
    ON vjh.kurzd = xb.kurzd 
WHERE 
    vjh.kurzd IN ('TEI', 'SEI', 'Ei') -- Nur Eichenarten
GROUP BY 
    vjh.h3_index7;


WITH jungpfl AS (
    SELECT 
    	h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7, -- H3-Index der Ebene 7
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz, 
        j.b0_bs,
        j.b0_zaun,
        j.b0_radius
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_r.jung_22 j  
        ON bvb.tnr = j.tnr AND bvb.enr = j.enr
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
   where bei.bl_bwi='12'
),
aggregated_data AS (
    SELECT 
        h3_index7,
        ba,
        bagr,
        biss,
        gr,
        SUM(anz) over (partition by h3_index7, ba) as sum_ba, -- anzahl bäume pro baumart im hexagon
        SUM(anz) over (partition by h3_index7, ba,gr) as sum_ba_gr, -- anzahl bäume pro baumart und größe im hexagon
        SUM(anz) over (partition by h3_index7, ba,gr,biss) as sum_ba_gr_biss, -- anzahl bäume pro baumart mit größe und biss im hexagon
        sum(CASE WHEN schu = 1 THEN anz ELSE 0 END) over (partition by h3_index7, ba,gr,biss) as sum_ba_gr_biss_schutz,
        sum(CASE WHEN schael = 1 THEN anz ELSE 0 END) over (partition by h3_index7, ba,gr,biss) as sum_ba_gr_biss_schael,
        round(SUM(anz) over (partition by h3_index7, ba,gr, biss)::numeric  / (pi() * b0_radius ^ 2)::numeric,2) as pflanzendichte_ba_gr_biss_qm
    FROM 
        jungpfl
)
SELECT 
--    aggregated_data.*
h3_index7,
ba, bagr, biss, gr,
sum(sum_ba) as anz_ba, --Anzahl der Bäume dieser Baumart
sum(sum_ba_gr) anz_ba_gr, -- Anzahl der Bäume dieser Baumart und Größenklasse
sum(sum_ba_gr_biss) as anz_ba_gr_biss,
sum(sum_ba_gr_biss_schutz) as ba_anz_schutz,
sum(sum_ba_gr_biss_schael) as ba_anz_schael,
sum(pflanzendichte_ba_gr_biss_qm) as pfldichte_qm_ba_gr_biss -- Pflanzendichte von Bäumen dieser Größenklasse und dieses Verbisses
FROM 
    aggregated_data
--where tnr = 37417 and enr = 1-- and ba = 100
where h3_index7 = '871f1ddb6ffffff'
group by 
h3_index7, ba, bagr, biss, gr;

-- das scheint der richtige Weg zu sein:
WITH jungpfl AS (
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7, -- H3-Index der Ebene 7
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz, 
        j.b0_bs,
        j.b0_zaun,
        j.b0_radius
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_r.jung_22 j  
        ON bvb.tnr = j.tnr AND bvb.enr = j.enr
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
   WHERE bei.bl_bwi = '12'
)
SELECT 
    h3_index7,
    ba,
    bagr,
    biss,
    gr,
    SUM(anz) AS anz_ba, -- Anzahl der Bäume dieser Baumart
    SUM(CASE WHEN schu = 1 THEN anz ELSE 0 END) AS ba_anz_schutz,
    SUM(CASE WHEN schael = 1 THEN anz ELSE 0 END) AS ba_anz_schael,
    ROUND(SUM(anz)::numeric / (pi() * MAX(b0_radius)^2)::numeric, 2) AS pfldichte_qm_ba_gr_biss -- Pflanzendichte
FROM 
    jungpfl
WHERE h3_index7 = '871f1ddb6ffffff'
GROUP BY 
    h3_index7, ba, bagr, biss, gr;

-- jetzt mit case when aggregieren:
WITH jungpfl AS (
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7, -- H3-Index der Ebene 7
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz, 
        j.b0_bs,
        j.b0_zaun,
        j.b0_radius
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_r.jung_22 j  
        ON bvb.tnr = j.tnr AND bvb.enr = j.enr
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
   WHERE bei.bl_bwi = '12'
)
SELECT 
    h3_index7,
    ba,
    bagr,
--    biss,
    gr,
    SUM(anz) AS anz_ba, -- Anzahl der Bäume dieser Baumart
    sum(case when biss=0 then anz else 0 end) as anz_biss0,
    sum(case when biss=1 then anz else 0 end) as anz_biss1,
    sum(case when biss=2 then anz else 0 end) as anz_biss2,
    sum(case when biss=3 then anz else 0 end) as anz_biss3,
    sum(case when biss=4 then anz else 0 end) as anz_biss4,
    SUM(CASE WHEN schu = 1 THEN anz ELSE 0 END) AS ba_anz_schutz,
    SUM(CASE WHEN schael = 1 THEN anz ELSE 0 END) AS ba_anz_schael,
    ROUND(SUM(anz)::numeric / (pi() * MAX(b0_radius)^2)::numeric, 2) AS pfldichte_qm_ba_gr_biss -- Pflanzendichte
FROM 
    jungpfl
WHERE h3_index7 = '871f1ddb6ffffff'
GROUP BY 
    h3_index7, ba, bagr, gr;

-- plotarea berechnen - es scheint als wäre die Angabe in objekte egal  
select distinct 
j.b0_radius, j.b0_vorh, j.b1_6_vorh  
from 
bwi_r.jungpfl_22 jpfl 
join 
bwi_r.jung_22 j  
on jpfl.tnr=j.tnr and jpfl.enr = j.enr; 


WITH plotareas AS (
    -- Berechnung der Gesamtfläche der Plots pro h3_index7
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7,
        SUM(pi() * j.b0_radius^2)::numeric  AS total_plot_area -- Summe der Flächeninhalte
    FROM 
        bwi_r.jung_22 j
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON j.tnr = bei.tnr AND j.enr = bei.enr
    WHERE 
        bei.bl_bwi = '12'
    GROUP BY 
        h3_lat_lng_to_cell(bei.ist_geom, 7)
),
jungpfl AS (
    -- Auswahl der Baumdaten
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7,
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
    WHERE 
        bei.bl_bwi = '12'
)
SELECT 
    jp.h3_index7,
    xb.kurzd,
    SUM(jp.anz) AS anz_ba, -- Gesamtanzahl der Bäume im Hexagon
    SUM(CASE WHEN jp.biss = 0 THEN jp.anz ELSE 0 END) AS anz_biss0,
    SUM(CASE WHEN jp.biss = 1 THEN jp.anz ELSE 0 END) AS anz_biss1,
    SUM(CASE WHEN jp.biss = 2 THEN jp.anz ELSE 0 END) AS anz_biss2,
    SUM(CASE WHEN jp.biss = 3 THEN jp.anz ELSE 0 END) AS anz_biss3,
    SUM(CASE WHEN jp.biss = 4 THEN jp.anz ELSE 0 END) AS anz_biss4,
    ROUND(SUM(CASE WHEN jp.biss = 0 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss0,
    ROUND(SUM(CASE WHEN jp.biss = 1 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss1,
    ROUND(SUM(CASE WHEN jp.biss = 2 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss2,
    ROUND(SUM(CASE WHEN jp.biss = 3 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss3,
    ROUND(SUM(CASE WHEN jp.biss = 4 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss4,
    SUM(CASE WHEN jp.schu = 1 THEN jp.anz ELSE 0 END) AS ba_anz_schutz,
    SUM(CASE WHEN jp.schael = 1 THEN jp.anz ELSE 0 END) AS ba_anz_schael,
    pa.total_plot_area,
    ROUND(SUM(jp.anz)::numeric / pa.total_plot_area, 2)*10000 AS pfldichte_ba_ha -- Pflanzendichte
FROM 
    jungpfl jp
JOIN 
    plotareas pa 
    ON jp.h3_index7 = pa.h3_index7
join 
	bwi_meta.x3_ba xb 
	on xb.icode = jp.ba
WHERE 
    jp.h3_index7 = '871f1ddb6ffffff' and jp.gr between 1 and 3
GROUP BY 
    jp.h3_index7, xb.kurzd,pa.total_plot_area;
   
-- Zusammenfassen auf Verbiss = 0 und Verbiss zwischen 1 und 4, Abbildung der Baumartenanteile
drop view bwi_r.v_jungpfl_h3_7;

--create view bwi_r.v_jungpfl_h3_7 as
WITH plotareas AS (
    -- Berechnung der Gesamtfläche der Plots pro h3_index7
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7,
        SUM(pi() * j.b0_radius^2)::numeric  AS total_plot_area -- Summe der Flächeninhalte
    FROM 
        bwi_r.jung_22 j
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON j.tnr = bei.tnr AND j.enr = bei.enr
    WHERE 
        bei.bl_bwi = '12'
    GROUP BY 
        h3_lat_lng_to_cell(bei.ist_geom, 7)
),
jungpfl AS (
    -- Auswahl der Baumdaten
    SELECT 
        h3_lat_lng_to_cell(bei.ist_geom, 7) AS h3_index7,
        bvb.ba,
        ba.bagr,
        bvb.gr,
        bvb.biss,
        bvb.schael,
        bvb.schu,
        bvb.anz
    FROM 
        bwi_r.jungpfl_22 bvb
    JOIN 
        bwi_meta.x3_ba ba 
        ON ba.icode = bvb.ba
    JOIN 
        bwi_r.bwi2022_ecke_istgeom bei 
        ON bei.tnr = bvb.tnr AND bei.enr = bvb.enr
    WHERE 
        bei.bl_bwi = '12'
)
SELECT 
    jp.h3_index7,
    xb.kurzd,
    CASE 
        WHEN jp.gr BETWEEN 0 AND 1 THEN '0-1' -- Zusammenfassung der Größenklassen 0 und 1
        WHEN jp.gr BETWEEN 2 AND 9 THEN '2-9' -- Zusammenfassung der Größenklassen 2 bis 9
        ELSE 'Andere' -- Optional: Falls andere Werte auftreten
    END AS gr_group, -- Neue Gruppierung der Größenklassen
    SUM(jp.anz) AS anz_ba, -- Gesamtanzahl der Bäume im Hexagon
    ROUND(SUM(jp.anz)::numeric / SUM(SUM(jp.anz)) OVER (PARTITION BY jp.h3_index7) * 100, 2) AS perc_ba, -- Prozentanteil der Baumart
--    SUM(CASE WHEN jp.biss = 0 THEN jp.anz ELSE 0 END) AS anz_biss0,
--    SUM(CASE WHEN jp.biss BETWEEN 1 AND 4 THEN jp.anz ELSE 0 END) AS anz_biss_1_4,
    ROUND(SUM(CASE WHEN jp.biss = 0 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss0,
    ROUND(SUM(CASE WHEN jp.biss BETWEEN 1 AND 4 THEN jp.anz ELSE 0 END)::numeric / SUM(jp.anz) * 100, 2) AS perc_biss_1_4,
    SUM(CASE WHEN jp.schu = 1 THEN jp.anz ELSE 0 END) AS ba_anz_schutz,
    SUM(CASE WHEN jp.schael = 1 THEN jp.anz ELSE 0 END) AS ba_anz_schael,
--    pa.total_plot_area,
    ROUND(SUM(jp.anz)::numeric / pa.total_plot_area, 2) * 10000 AS pfldichte_ba_ha, -- Pflanzendichte in Bäumen/ha
    ROUND(sum(SUM(jp.anz)) over (partition by jp.h3_index7) ::numeric / pa.total_plot_area, 2) * 10000 AS pfldichte_ha
FROM 
    jungpfl jp
JOIN 
    plotareas pa 
    ON jp.h3_index7 = pa.h3_index7
JOIN 
    bwi_meta.x3_ba xb 
    ON xb.icode = jp.ba
where xb.kurzd in ('TEI', 'SEI', 'Ei')
--WHERE 
--    jp.h3_index7 = '871f1ddb6ffffff' --AND 
--    jp.gr BETWEEN 0 -- 20 bis < 50 cm Höhe
--     AND 1 --50 bis 130 cm Höhe
GROUP BY 
    jp.h3_index7, jp.gr,xb.kurzd, pa.total_plot_area;
   




