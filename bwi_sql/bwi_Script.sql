-- CONNECTION: name=kubupostgis
--punkte aus Koordinaten erzeugen
select tnr, enr, 
st_setsrid(st_makepoint(to_number(
soll_x_wgs84,
--13,09852461
'99D99999999'), to_number(soll_y_wgs84,'99D99999999')),4326)
from bwiti.b3_ecke
where tnr=47305

/*relevante Informationen für vorklärung in bwi.b3_ecke_202212 unterbringen
 * 
 */
ALTER TABLE bwi.b3_ecke_202212 
ADD   wa int4 NULL,
ADD    begehbar int4 NULL,
ADD    kreis int4 NULL,
ADD    gemeinde int4 NULL,
ADD    ffh int4 NULL,
ADD    vogelsg int4 NULL,
ADD    nationalp int4 NULL,
ADD    naturp int4 NULL,
ADD    biosphaere int4 NULL,
ADD    natursg int4 NULL,
ADD    fa int4 NULL,
ADD    nathoe int4 NULL,
ADD    eg int4 NULL,
ADD    eggrkl int4 NULL,
ADD    natwgv int4 NULL,
ADD    wlt_v int4 NULL,
ADD    wlt_wiev int4 NULL,
ADD    ne int4 NULL,
ADD    neursachenb int4 NULL,
ADD    neursacheb int4 NULL,
ADD    lanu int4 NULL,
ADD    ez1 int4 NULL,
ADD    st_methode int4 NULL,
ADD    st_quelle int4 NULL,
ADD    st_b int4 NULL,
ADD    st_ks_hs int4 NULL,
ADD    st_ks_fs int4 NULL,
ADD    st_n int4 NULL,
ADD    st_nz int4 NULL,
ADD    st_wh_fg int4 NULL,
ADD    st_wh_fz int4 NULL,
ADD    st_wh_e int4 NULL,
ADD    st_var varchar NULL,
ADD    kon int4 NULL,
ADD    gestein int4 NULL,
ADD    kueste int4 NULL,
ADD    lsg int4 NULL,
ADD    histwald int4 NULL,
ADD    nenschutz int4 NULL,
ADD    neswald int4 NULL,
ADD    neewald int4 NULL,
ADD    nesabursach int4 NULL,
ADD    nesplitter int4 NULL,
ADD    nestreu int4 NULL,
ADD    neunerschlies int4 NULL,
ADD    negeleig int4 NULL,
ADD    negerertrag int4 NULL,
ADD    neeigenbin int4 NULL,
ADD    nesibursach int4 NULL;

update bwi.b3_ecke_202212 be
set 
wa=bev.wa,
begehbar = bev.begehbar 
from bwi_b_vorkl.b3f_ecke_vorkl bev
where be.tnr=bev.tnr and be.enr=bev.enr;

update bwi_b_vorkl.b3f_ecke_vorkl bev
set
ne = bev.ne ,
nesabursach = 1
from bwi.b3_ecke_202212 be
where be.tnr = bev.tnr and be.enr=bev.enr 
and be.ne = 2

create table pnv_pom (
karteinh varchar primary key,
pnv_bez text
)

alter table lfe.stgr_pnv_bb  
set schema bwi

select  be.tnr, be.enr, s.kl_feuchtest , s.stogrriek, sb.* 
from bwi.b3_ecke_20200510 be, bwi.stogrpolyr s right join bwi.stgr_pnv_bb sb on s.kl_feuchtest = sb.klimast and s.stogrriek = concat(sb.ngr, sb.feuchte) 
where 
st_dwithin(be.geom,s.geom,0)


select *
from  bwi.df d; 

UPDATE bwi.df d1  
SET document_tokens = to_tsvector(d1."Eig.Schreibweise GB")  
FROM bwi.df d2;

CREATE INDEX tsvind ON bwi.df USING GIN (document_tokens);


SELECT d."Eig.Schreibweise GB" ,d."BWI-EA" 
FROM bwi.df d 
WHERE document_tokens @@ to_tsquery('GmbH | gmbh'); 

update bwi.df 
set "BWI-EA" = 4
WHERE document_tokens @@ to_tsquery('GmbH | gmbh')
--('GmbH & Co')
; 

update bwi.df 
set "BWI-EA" = 4
where document_tokens @@ to_tsquery('mbH') -- 'GmbH' 'KG' 'e.G.' 'm.b.H'

select *
from bwi.df d 
where d.document_tokens @@ to_tsquery('GmbH') 

alter extension h3
set schema bwi

create extension h3_postgis

SELECT i, h3_num_hexagons(i)
    FROM generate_series(0, 15, 3) i
;
set search path bwi

--- tnr to h3
--verworfen:
 with tnrpoly as (select b3.tnr, st_convexhull(st_collect(geom))::geometry(polygon,4326) geom
 from bwi.b3_ecke_20200510 b3
group by b3.tnr)
select tnr, h3_polygon_to_cells(geom,8), h3_cell_to_boundary_geometry((h3_polygon_to_cells(geom,8)),8) 
from tnrpoly

create table tnr_h3_8 as
with tnrpoly as (select b3.tnr, st_convexhull(st_collect(geom))::geometry(polygon,4326) geom
 from bwi.b3_ecke_20200510 b3
group by b3.tnr)
select tnr,h3_cell_to_boundary_geometry(h3_lat_lng_to_cell((st_centroid(geom)),8))::geometry(POLYGON,4326) geom
from tnrpoly

-- Ecke auf H3
create table tnr_enr_h3_8 as
select b3.tnr, b3.enr, h3_cell_to_boundary_geometry(h3_lat_lng_to_cell(b3.geom,10))::geometry(POLYGON,4326) geom
from bwi.b3_ecke_20200510 b3


----Verjüngungskreise
--20221006
select b0.tnr,b0.enr,b0.ba,s.kurzd,b0.biss, xv.kurzd ,b0.schu, b0.anz 
from 
bwi."l9095_tabelle_b3v_b0_20221006" b0 
--left join bwi.lospunkte l on b0.tnr=l.tnr and b0.enr=l.enr
join bwi.x3_verbiss xv on xv.icode = b0.biss
join bwi.s_ba s on s.icode_ba = b0.ba
where b0.tnr=39138

--create foreign table b0_20221006o as 
select distinct b0.tnr,b0.enr,concat(b0.tnr,b0.enr) tnrenr, 
--b0.ba,
s.kurzd as ba_d,
--b0.biss, 
xv.kurzd as biss_d ,b0.schu,
sum(b0.anz) over(partition by b0.ba,b0.biss) n_biss, --Verbissene Bäume der Baumart insgesamt
sum(b0.anz) over (partition by b0.ba,b0.tnr,b0.enr) n_ba_ecke, -- Anzahl der Bäume nach Baumart auf Ecke
round((((sum(b0.anz) over (partition by b0.ba,b0.tnr,b0.enr))::numeric/(sum(b0.anz) over (partition by b0.tnr,b0.enr))*100)),2) ba_anteil_ecke,
sum(b0.anz) over (partition by b0.ba,b0.tnr) n_ba_trakt, -- Anzahl der Bäume nach Baumart im Trakt
round((((sum(b0.anz) over (partition by b0.ba,b0.tnr))::numeric/(sum(b0.anz) over (partition by b0.tnr))*100)),2) ba_anteil_trakt,
sum(b0.anz) over (partition by b0.tnr) n_trakt, --Anzahl aller Bäume im Trakt
round(((sum(b0.anz) over (partition by b0.tnr))::numeric/(pi()*2^2))::numeric,2) pfldichte_qm,
sum(b0.anz) over (partition by b0.ba) n_ba,
round((((sum(b0.anz) over (partition by b0.ba))::numeric/((sum(b0.anz) over ()))*100)),2) ba_anteil_gesamt,
 sum(b0.anz) over () n_baum_gesamt
from 
bwi."l9095_tabelle_b3v_b0_20221006" b0 
--left join bwi.lospunkte l on b0.tnr=l.tnr and b0.enr=l.enr
join bwi.x3_verbiss xv on xv.icode = b0.biss
join bwi.s_ba s on s.icode_ba = b0.ba
--group by b0.tnr,b0.enr,b0.ba,b0.biss,b0.schu,s.kurzd, xv.kurzd,b0.anz
where b0.tnr=39138
--where b0.ba = 140
--)
--	server bwidatagpkg;

-- nach versehentlicher postgiserweiterungsdeinstallation
alter table bwi.b3_ecke_20200510 add column geom geometry(POINT,4326);

update bwi.b3_ecke_20200510 b3 set geom = st_setsrid(st_makepoint(b3.soll_x_wgs84, b3.soll_y_wgs84),4326)

-- ogr-fdw geopackage
create server bwidatagpkg
	foreign data wrapper ogr_fdw
	options (
		datasource '/home/torsten/Nextcloud/LFE/Informationsdashboard/BWI_LWI/bwi_daten.gpkg',
		format 'GPKG');
	
create schema bwidatagpkg;

import foreign schema ogr_all
	from server bwidatagpkg
	into bwidatagpkg;

/* import und Prüfung aus TI-Datenbank nach bwiti
 * Import von 
 * - Komplettsystem 27.11.22
 */
 select distinct xb.langd, count(bvw.*)
 from bwiti.b3v_wzp bvw join bwiti.x3_ba xb on bvw.ba = xb.icode 
 group by xb.langd 
 order by  count(bvw.*)
 
select * 
from bwiti.b3v_bemerkung bvb
where bvb.tnr in (
 select bvw.tnr
 from bwiti.b3v_wzp bvw
where bvw.pk  < 2
and bvw.m_bhd is null) 

select be.tnr,be.enr, be.wa,be.begehbar, be.datumfelde,  
from 
bwiti.bv_ecke be join bwiti.x3_wa xw on be.wa =xw.icode 

/* Eigentumszuordnung
 * 
 */
update bwi.df d 
set "BWI-EA" = 4
where document_tokens @@to_tsquery('sielmann & stiftung')

select d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
--*
--d."Eig.Schreibweise GB" , d.document_tokens 
from bwi.df d 
where --d.document_tokens similar to '%agrargenossenschaft%'
--d.document_tokens @@to_tsquery('gemeind') and
d.eig_schr_gb similar to '%AFB Agrar%' --and
--d.bwi_ea is null
or d.adresse_nr = '36471498'

select distinct d."Adresse_Nr"  
from bwi.df d 

alter table lfe.pnv_pom 
set schema bwi

--pnv
select be.tnr,be.enr, bp.code_brdbg, bp.kartierung, wg.kl_feuchte   
from bwi.b3_ecke_20200510 be,  lfe.bfn_pnv_20230105 bp, lfe.wgwb_16 wg
where st_dwithin(be.geom,bp.geom,0) and 
st_dwithin(be.geom, wg.geom,0)

--netz für rainer
select bt.tnr, bt.netz,xn.kurzd,  bt.netz64, be.enr, be.geom  
from bwi.b3_tnr bt
join bwi.x3_netz xn on bt.netz =xn.icode 
left join bwi.b3_ecke_202212 be on bt.tnr=be.tnr
where be.enr=1
