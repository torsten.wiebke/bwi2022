-- New script in lfe_repos/bwi2022/bwi_sql
-- Date: ${date}
-- CONNECTION: name=projekt_lfb
-- Database: projekt_lfb           - target database name

/* lospunkt kommt aus waldinv und wurde benutzt um die Lose zuzuordnen und administrative Aufgaben zu erledigen
 * 
 */
-- index nach kopie von anderer Datenbank
-- pk auf tnr und enr
alter table bwi_admin.lospunkt
add constraint pk_lospunkt primary key (tnr, enr);

alter table bwi_admin.status
add constraint pk_status primary key (id_status);

alter table bwi_admin.statusadd
add constraint pk_statusadd primary key (id_statusadd);


-- räumlicher Index
create index idx_lospunkt_geom on bwi_admin.lospunkt using gist (geom);

-- update statusadd in lospunkt wegen zusammenführung der Schlüsseltabellen
update bwi_admin.lospunkt
set statusadd = 410
where statusadd = 41

select count(*)
from bwi_admin.lospunkt l 
left join bwi_admin.s_status s on s.id_status  = l.status
left join bwi_admin.s_statusadd sa on sa.id_statusadd = l.statusadd 
left join bwi_2022.b3f_ecke_vorkl bfev on bfev.tnr =l.tnr and bfev.enr =l.enr 
join bwi_meta.x3_wa xw on xw.icode = bfev.wa 

-- anzahl der Lospunkte nach status und los
select l.losnr, s.statusbes,sa.statusaddbes, l.aktion, xw.kurzd,  count(*) 
from bwi_admin.lospunkt l 
left join bwi_admin.s_status s on s.id_status  = l.status
left join bwi_admin.s_statusadd sa on sa.id_statusadd = l.statusadd 
left join bwi_2022.b3f_ecke_vorkl bfev on bfev.tnr =l.tnr and bfev.enr =l.enr 
left join bwi_meta.x3_wa xw on xw.icode = bfev.wa 
group by  ROLLUP (l.losnr, s.statusbes, sa.statusaddbes, l.aktion, xw.kurzd);

SELECT 
    l.losnr, 
    s.statusbes, 
    sa.statusaddbes, 
    l.aktion, 
    xw.kurzd,  
    COUNT(*) AS punktanzahl, 
    SUM(COUNT(*)) OVER () AS gesamtanzahl,  -- Gesamtsumme aller Punkte in lospunkt
    SUM(COUNT(*)) OVER (PARTITION BY l.losnr) AS anzahl_pro_losnr,  -- Summe pro losnr, mit spezieller Berücksichtigung für NULL-Werte
    SUM(COUNT(*)) OVER (PARTITION BY l.losnr, s.statusbes) AS anzahl_pro_losnr_status,  -- Summe pro losnr und statusbes
    SUM(COUNT(*)) OVER (PARTITION BY l.losnr, s.statusbes, xw.kurzd) AS anzahl_pro_losnr_status_kurzd  -- Summe pro losnr, statusbes und kurzd
FROM 
    bwi_admin.lospunkt l 
LEFT JOIN 
    bwi_admin.s_status s ON s.id_status = l.status
LEFT JOIN 
    bwi_admin.s_statusadd sa ON sa.id_statusadd = l.statusadd 
LEFT JOIN 
    bwi_2022.b3f_ecke_vorkl bfev ON bfev.tnr = l.tnr AND bfev.enr = l.enr 
left JOIN 
    bwi_meta.x3_wa xw ON xw.icode = bfev.wa 
GROUP BY     l.losnr, s.statusbes, sa.statusaddbes, l.aktion, xw.kurzd
--ORDER BY     l.losnr, s.statusbes, xw.kurzd;


SELECT 
    SUM(COUNT(*)) OVER () AS gesamtanzahl,  -- Gesamtsumme aller Punkte in lospunkt,
    l.losnr,
    SUM(COUNT(*)) OVER (PARTITION BY l.losnr) AS anzahl_pro_losnr,  -- Summe pro losnr
   xw.kurzd,
    SUM(COUNT(*)) OVER (PARTITION BY l.losnr, xw.kurzd) AS anzahl_pro_losnr_kurzd
FROM 
    bwi_admin.lospunkt l 
LEFT JOIN 
    bwi_admin.s_status s ON s.id_status = l.status
LEFT JOIN 
    bwi_admin.s_statusadd sa ON sa.id_statusadd = l.statusadd 
LEFT JOIN 
    bwi_2022.b3f_ecke_vorkl bfev ON bfev.tnr = l.tnr AND bfev.enr = l.enr 
left JOIN 
    bwi_meta.x3_wa xw ON xw.icode = bfev.wa 
GROUP BY     l.losnr, kurzd--, s.statusbes, sa.statusaddbes, l.aktion, xw.kurzd
--ORDER BY     l.losnr, s.statusbes, xw.kurzd;

SELECT DISTINCT( l.losnr),
    COUNT(*) OVER (PARTITION BY l.losnr) AS anzahl_pro_losnr,  -- Summe pro losnr
    xw.kurzd,
    COUNT(*) OVER (PARTITION BY l.losnr, xw.kurzd) AS anzahl_pro_losnr_kurzd  -- Summe pro losnr und kurzd
    ,SUM(1) OVER () AS gesamtanzahl  -- Gesamtsumme aller Punkte in lospunkt
    , count(*) FILTER (where l.losnr > 11 ) over () as eigenaufnahmenall
    , count(*) FILTER (where l.losnr between 1 and 11) over () as vergabepunkte
    , count(*) filter (where xw.kurzd = 'Nichtwald') over () as nichtwaldall
    , count(*) filter (where bfev.wa between 1 and 5) over () as waldall
    , count(*) FILTER (where l.losnr not between 1 and 11) over (partition by l.losnr) as eigenaufnahmen 
    , count(*) FILTER (where l.losnr not between 1 and 11) over () as eigenaufnahmenall
FROM 
    bwi_admin.lospunkt l 
LEFT JOIN 
    bwi_admin.s_status s ON s.id_status = l.status
LEFT JOIN 
    bwi_admin.s_statusadd sa ON sa.id_statusadd = l.statusadd 
LEFT JOIN 
    bwi_2022.b3f_ecke_vorkl bfev ON bfev.tnr = l.tnr AND bfev.enr = l.enr 
LEFT JOIN 
    bwi_meta.x3_wa xw ON xw.icode = bfev.wa
   order by l.losnr ;
  
  select distinct l.losnr, count(*)
  from bwi_admin.lospunkt l 
  group by l.losnr
  
select distinct l.losnr, 
count(*) OVER (PARTITION BY l.losnr) AS anzahl_pro_losnr
  from bwi_admin.lospunkt l
  order by l.losnr 

select --distinct bftw.anat 
	count (distinct bfev.tnr) filter (where bfev.wa between 0 and 5) as ausschreibungstrakte,
	COUNT(distinct bfev.tnr) filter (where bfev.wa = 0) as nichtwaldtrakte 	-- Anzahl der Traktnummern mit bfev.wa = 0
	,COUNT(distinct bfev.tnr) filter (where bfev.wa between 1 and 5) as waldtrakte	-- Anzahl der Traktnummern mit bfev.wa zwischen 1 und 5
	,count(bfev.enr) filter (where bfev.wa between 0 and 5 and bftw.anat between 1901 and 1916) as begutachtungsecken
	,count(bfev.enr) filter (where bfev.wa between 0 and 5 and bftw.anat between 1901 and 1911) as ausschreibungsecke -- siehe bwi_meta.k3_login
	,count(bfev.enr) filter (where bfev.wa between 0 and 5 and bftw.anat between 1912 and 1916) as lfebearbeitungsecke 
	,count(bfev.enr) filter (where bfev.wa between 1 and 5) as waldecke
	--,count(bvef.enr) as realwaldecke -- todo: warum geht das nicht
from
	bwi_2022.b3f_ecke_vorkl bfev
	--join bwi_admin.lospunkt l on l.tnr = bfev.tnr
join bwi_2022.b3f_tnr_vorkl bftv on 	bftv.tnr = bfev.tnr
join bwi_2022.b3f_tnr_work bftw on	bftw.tnr = bfev.tnr
-- ,bwi_2022.b3v_ecke_feld bvef -- todo: warum geht das nicht
where
	bftv.trstatus between 1 and 3 -- waldtrakte oder ungewiss = x3_tr_status icode=3
	and bftw.anat between 1901 and 1916
--order by bftw.anat 

select *
from bwi_meta.x3_tr_status xts 


select icode, kurzd,langd 
from 
bwi_meta.k3_ez9 ke 
where ke.bl = 12

56789
