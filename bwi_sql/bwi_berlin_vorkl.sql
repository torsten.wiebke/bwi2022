-- CONNECTION: name=kubupostgis
create table bwi_b_vorkl.b3f_ecke_vorkl as
table bwi_vorkl.b3f_ecke_vorkl 
with no data;

create table bwi_b_vorkl.b3f_tnr_vorkl as 
table bwi_vorkl.b3f_tnr_vorkl 
with no data;

--1. löschen vor import weiterer gebiete
delete from bwi_b_vorkl.b3f_ecke_vorkl bfev 

delete from bwi_b_vorkl.b3f_tnr_vorkl 

delete from bwi_vorkl.b3f_ecke_vorkl 

delete from bwi_vorkl.b3f_tnr_vorkl 

--übertragen der Waldeigenschaft und der Begehbarkeit aus der TI-Vorklärungstabelle
update bwi.b3_ecke_202212 be 
set
wa =bev.wa, 
begehbar = bev.begehbar 
from bwi_vorkl.b3f_ecke_vorkl bev
where bev.tnr = be.tnr and bev.enr=be.enr 

--übertragen der eingetragenen standortshöhen und feuchtestufe aus TI-Vorklärungstabelle
update bwi.b3_ecke_202212  bev
set 
st_ks_hs = be.st_ks_hs,
st_ks_fs = be.st_ks_fs 
from 
bwi_vorkl.b3f_ecke_vorkl be
where be.tnr = bev.tnr and be.enr=bev.enr 

select bfev.*,be.geom 
from bwi_b_vorkl.b3f_ecke_vorkl bfev 
left join bwi.b3_ecke_20200510 be on be.tnr =bfev.tnr and be.enr =bfev.enr 
--where-- be.geom is null 

select bftv.*--,bfev.*,bb.notiz 
from bwi_b_vorkl.b3f_tnr_vorkl bftv 
--join bwi_b_vorkl.b3f_ecke_vorkl bfev on bftv.tnr = bfev.tnr 
--right join bwi_b_vorkl.bv_bemerkung bb on bb.tnr =bftv.tnr 
where bftv.trstatus > 2

--Berlin
update bwi_b_vorkl.b3f_tnr_vorkl
set 
--rot_wild = 0,
--dam_wild =0,
muf_wild =0,
gams_wild =0
where trstatus <3

--alle Muffel- und Gamswild 
update bwi_vorkl.b3f_tnr_vorkl 
set 
--rot_wild = 0,
--dam_wild =0,
muf_wild =0,
gams_wild =0
where trstatus <3

select distinct  bfev.wa, bftv.trstatus  
from bwi_b_vorkl.b3f_ecke_vorkl bfev 
join bwi_b_vorkl.b3f_tnr_vorkl bftv on bfev.tnr = bftv.tnr 

--wa auf 0 wo traktstatus nichtwald
update bwi_b_vorkl.b3f_ecke_vorkl be
set 
wa = 0
from bwi_b_vorkl.b3f_tnr_vorkl bt
where be.tnr =bt.tnr and bt.trstatus > 3

select distinct  be.fa --, bftv.trstatus  
from bwi_b_vorkl.b3f_ecke_vorkl be

-- abfrage tnr,enr foa
select be.tnr,be.enr,  be.fa --, bftv.trstatus  
from bwi_b_vorkl.b3f_ecke_vorkl be

with foa as
(select to_number('1200'||foa.obf,'99999') as fa, be.tnr, be.enr 
--foa.obf 
from 
lfe.sde_fuek_obf_20230114 foa, 
bwi_b_vorkl.b3f_ecke_vorkl be join bwi.b3_ecke_20200510 be2 on be.tnr =be2.tnr and be.enr = be2.enr 
where
st_dwithin(be2.geom,foa.geom,0)) 
update bwi_b_vorkl.b3f_ecke_vorkl be
set 
fa = foa.fa
from foa 
where foa.tnr = be.tnr and foa.enr=be.enr 

-- update der vebleibendenden mit 11000 für Berlin
update bwi_b_vorkl.b3f_ecke_vorkl
set 
fa=11000
where fa = -1

--join mit Eigentumstabelle
select bev.tnr ,bev.enr,bev.eg,bev.eggrkl,bev.fa, d.eig_schr_gb, d.bwi_ea, d.eiggrkl eiggrkld,d.sicher,d.id_df  
from 
bwi_b_vorkl.b3f_ecke_vorkl bev 
join bwi.df d on d.tnr=bev.tnr and d.enr =bev.enr

-- wir machen aus allen Waldecken in Berlin zum Landeswald
update bwi_b_vorkl.b3f_ecke_vorkl
set
eg = 2,
eggrkl = 7
where fa= 11000

select distinct bev.natwgv, bev.wlt_v,bev.wlt_wiev  
from bwi_b_vorkl.b3f_ecke_vorkl bev

--wlt-v auf 1 und wlt-wie auf 60
update bwi_b_vorkl.b3f_ecke_vorkl
set
wlt_v = 1,
wlt_wiev = 60
where wlt_v = -1 and wlt_wiev = -1

select bev.tnr, bev.natwgv,sbp.code_brdbg, pp.karteinh,pp.bwi_natwgv , bev.wlt_v,bev.wlt_wiev  
from bwi_b_vorkl.b3f_ecke_vorkl bev
join bwi.b3_ecke_20200510 be on bev.tnr =be.tnr and bev.enr = be.enr
, lfe.sde_bfn_pnv_20230105 sbp join bwi.pnv_pom pp   on sbp.code_brdbg=pp.karteinh  
where
--bev.natwgv = -1 and
st_dwithin(be.geom,sbp.geom,0)


--update natwg von pnv
with natw as 
(select bev.tnr, bev.enr, pp.bwi_natwgv
from bwi_b_vorkl.b3f_ecke_vorkl bev
join bwi.b3_ecke_20200510 be on bev.tnr =be.tnr and bev.enr = be.enr
, lfe.sde_bfn_pnv_20230105 sbp join bwi.pnv_pom pp   on sbp.code_brdbg=pp.karteinh  
where
bev.natwgv = -1
and st_dwithin(be.geom,sbp.geom,0)
)
update bwi_b_vorkl.b3f_ecke_vorkl bev
set natwgv = natw.bwi_natwgv
from natw
where 
natwgv = -1 and 
bev.tnr=natw.tnr and 
bev.enr =natw.enr

--update natwg von pnv auf bwi.b3_ecke_202212 bev
with natw as 
(select bev.tnr, bev.enr,bev.natwgv bevnatwgv, 
pp.bwi_natwgv
from bwi.b3_ecke_202212  bev
, lfe.sde_bfn_pnv_20230105 sbp join bwi.pnv_pom pp   on sbp.code_brdbg=pp.karteinh  
where
bev.natwgv is null and 
bev.wa = 5
and st_dwithin(bev.geom,sbp.geom,0)
)
update bwi.b3_ecke_202212 bev
set natwgv = natw.bwi_natwgv
from natw
where 
--natwgv = -1 and 
bev.tnr=natw.tnr and 
bev.enr =natw.enr

--nathöhe
select distinct  bev.nathoe, count(*) over() 
from bwi_b_vorkl.b3f_ecke_vorkl bev 

update bwi_b_vorkl.b3f_ecke_vorkl
set nathoe = 1
where nathoe =-1


--natürliche Höhenstufe und Standortsklimafeuchtestufe
update bwi.b3_ecke_202212  bev
set 
nathoe  = 1
where nathoe =-1 or nathoe is null

update bwi.b3_ecke_202212 bev 
set st_ks_hs = 1 where st_ks_hs = - 1 or st_ks_hs is null 

update bwi.b3_ecke_202212 
set st_ks_fs = 7 where st_ks_fs = -1 or st_ks_fs is null 

select distinct st_ks_hs, st_ks_fs  
from bwi.b3_ecke_202212 


/* Nutzungseinschränkungen
| ursache                    | layer   | nutzung | nutzungscode | ne-ursache                   |
|----------------------------+---------+---------+--------------+------------------------------|
| exponierte Lage Schutzwald | wfk-bel | 2/3     | ne=4         | neeswald=1 und nesabursach=1 |
| trinkasserschutzgebiet     | lfu-wsg | 2/3     | ne=4         | neswald=1 und nesabursach=1 |
| trinkwasserschutzzone 1    | lfu_wsg | 0/3     | ne=2         | neswald=1 und nesabursach=1 |
| totalreservat              | lfu_trv | 0/3     | ne=2         | nesabursach=1 nenschutz=1    |
| naturwald                  | wfk_nwr | 0/3     | ne=2         | nesabursach=1 nenschutz=1    |
| nicht betretbar            | wfk_nbt | 0/3     | ne=2         | nesabursach=1                |
* 
 */

select distinct ne,neursachenb,neursacheb, neswald, nesabursach, nenschutz  
from bwi.b3_ecke_202212 

--exponierte lage - Updated Rows  204
update bwi.b3_ecke_202212  bev 
set 
ne = 4,
neswald = 1,
nesabursach =1
from lfe.sde_wfk_bel_20230114 bel
where wa=5 and st_dwithin(bev.geom,bel.geom,0)

--trinkwasserschutzgebiete - Updated Rows   535
update bwi.b3_ecke_202212  bev 
set 
ne = 4,
neswald = 1,
nesabursach =1
from lfe.sde_lfu_wsg_20230105 ws
where wa=5 and st_dwithin(bev.geom,ws.geom,0)

--trikwasserschutzgebiete zone 1 - Updated Rows 0
select *
from lfe.sde_lfu_wsg_20230105 slw 
where slw.schutzzone = 'Zone I'

update bwi.b3_ecke_202212  bev 
set 
ne = 2,
neswald = 1,
nesabursach =1
from lfe.sde_lfu_wsg_20230105 ws
where wa=5 and st_dwithin(bev.geom,ws.geom,0) and ws.schutzzone = 'Zone I'

--totalreservat - Updated Rows 0
update bwi.b3_ecke_202212  bev 
set 
ne = 2,
neswald = 1,
nenschutz  =1
from lfe.sde_lfu_trv_20230128  ws
where wa=5 and st_dwithin(bev.geom,ws.geom,0)

--naturwald - Updated Rows 0
update bwi.b3_ecke_202212  bev 
set 
ne = 2,
neswald = 1,
nesabursach =1
from lfe.sde_lfu_trv_20230128  ws
where wa=5 and st_dwithin(bev.geom,ws.geom,0)


--berlin
select 
bev.tnr,
bev.enr,
ne
,neursachenb 
,neursacheb 
,nenschutz 
,neswald 
,neewald 
,nesabursach 
,nesplitter 
,nestreu 
,neunerschlies 
,negeleig 
,negerertrag 
,neeigenbin 
,nesibursach 
from bwi_b_vorkl.b3f_ecke_vorkl bev 
join bwi.b3_ecke_20200510 be on bev.tnr =be.tnr and bev.enr = be.enr
,lfe.sde_lfu_wsg_20230105 wsg
where 
st_dwithin(be.geom,wsg.geom,0) 
and wsg.schutzzone like 'Zone I'

--in qgis laden:
select 
ne
,neursachenb 
,neursacheb 
,nenschutz 
,neswald 
,neewald 
,nesabursach 
,nesplitter 
,nestreu 
,neunerschlies 
,negeleig 
,negerertrag 
,neeigenbin 
,nesibursach 
from bwi_b_vorkl.b3f_ecke_vorkl bev 
where bev.tnr=21148

update bwi_b_vorkl.b3f_ecke_vorkl
set
ne =-1
,neursachenb  =-1
,neursacheb  =-1
,nenschutz  =-1
,neswald  =-1
,neewald  =-1
,nesabursach  =-1
,nesplitter  =-1
,nestreu  =-1
,neunerschlies  =-1
,negeleig  =-1
,negerertrag  =-1
,neeigenbin  =-1
,nesibursach  =-1
where 
ne < 0

--vorkl ecke 20230223
CREATE TABLE bwi_vorkl.b3f_ecke_vorkl20230223
as table bwi_vorkl.b3f_ecke_vorkl 
with no data

select * from 
bwi_vorkl.b3f_ecke_vorkl20230223 bfev 
where tnr=21148

select * 
from bwi.b3_ecke_202212 be 
where tnr=21148

select *
from bwi_b_vorkl.b3f_ecke_vorkl bfev 
where tnr = 21148

