-- CONNECTION: name=kubupostgis
-- import der Waldfläche nach Alkis Adresse
delete from bwi.summewaldfl_zu7598adr_nr 


update bwi.summewaldfl_zu7598adr_nr 
set summe_wald_qm = null 
where summe_wald_qm like '%-%'

update bwi.summewaldfl_zu7598adr_nr 
set summe_wald_qm = replace(summe_wald_qm,'.',)



select *
from bwi.summewaldfl_zu7598adr_nr
--where summe_wald_qm like '%-%'
--where summe_wald_qm is null 

update bwi.summewaldfl_zu7598adr_nr
set sume_wald_ha = (summe_wald_qm/10000)

select df.adresse_nr, df.flstckt, df.eig_schr_gb  
from 
bwi.df df join bwi.lospunkte2022 lp on df.tnr =lp.tnr and df.enr =lp.enr

select lp.tnr, lp.enr,df.adresse_nr, df.flstckt, df.eig_schr_gb,lp.geom,lp.tid   
from 
bwi.df df join bwi.lospunkte2022 lp on df.tnr =lp.tnr and df.enr =lp.enr

-- df hat space in eig_schr_gb
select distinct  trim(d.eig_schr_gb)
from bwi.df d 

update bwi.df
set eig_schr_gb = trim(eig_schr_gb)

select d.tnr, d.eig_schr_gb, d.document_tokens, d."Summe_A_Wald", d.bwi_ea, d.eiggrkl, d.sicher, d.adresse_nr, d.eig_nr   
from 
bwi.df d 
where d.eig_schr_gb similar to '%Land Brandenburg (Landesforstverwaltung)%'

select sum(szan.summe_wald_qm)/10000::real as ha 
from bwi.summewaldfl_zu7598adr_nr szan
where szan.adr_nr in (
    select  d.adresse_nr  
    from 
    bwi.df d 
    where 
    d.eig_schr_gb similar to '%Wald- und Grundbesitz GmbH%'
--  d.eig_schr_gb similar to '%Verein der Freunde des Deutsch-Polnischen Europa Nationalpar%'
    --d.eig_schr_gb similar to '%BVVG Bodenverwertungs%'
    )

    
update bwi.df 
set 
bwi_ea = 2,
eiggrkl = 7
where eig_schr_gb similar to '%Land Brandenburg (Landesforstverwaltung)%'

    
select d.tnr,d.enr , d.eig_schr_gb, d.document_tokens, d."Summe_A_Wald", d.bwi_ea, d.eiggrkl, d.sicher, d.adresse_nr, d.eig_nr   
from 
bwi.df d 
where tnr=36958
    --d.eig_schr_gb similar to '%Hilfswerk-Siedlung GmbH%'
--d.eig_schr_gb similar to '%Agrargenossenschaft Züllsdorf%'

select sum(szan.summe_wald_qm)/10000::real as ha 
from bwi.summewaldfl_zu7598adr_nr szan
where szan.adr_nr in (
    select  d.adresse_nr  
    from 
    bwi.df d 
    where 
    d.eig_schr_gb similar to '%Boldt Matthias  17268 Templin%'--'%Willberger Eberhardt%'
    --'%von Schönfels Friedrich %'
    --'%Schwiekers Oskar%'
    --'%BVVG%'--'%Krause Gustav%'
    --'%Domanski Bernhard Baumeisterstr%'
    --'%Stadt Angermünde%'--'%Gresmeier Renate  32689 %'--'%Bäsig Olaf%'--'%Hilfswerk-Siedlung%'
    --d.eig_schr_gb similar to '%Agrargenossenschaft Züllsdorf%'
    )
    
select d.tnr, d.eig_schr_gb, d.document_tokens, d."Summe_A_Wald", d.bwi_ea, d.eiggrkl, d.sicher, d.adresse_nr, d.eig_nr   
from 
bwi.df d 
where d.eig_schr_gb similar to '%Lausitzer und%'

select sum(szan.summe_wald_qm)/10000::real as ha 
from bwi.summewaldfl_zu7598adr_nr szan
where szan.adr_nr in (
    select  d.adresse_nr  
    from 
    bwi.df d 
    where d.eig_schr_gb similar to '%Lausitzer und%')

-- Feldberger Förderverein
select (sum(d."Summe_A_Wald") over ()/10000), d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
--*
--d."Eig.Schreibweise GB" , d.document_tokens 
from bwi.df d 
where --d.document_tokens similar to '%agrargenossenschaft%'
--d.document_tokens @@to_tsquery('gemeind') and
d.eig_schr_gb similar to '%Feldberg - Uckermärkisch%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'


update bwi.df d 
set bwi_ea = 4,
eiggrkl = 7,
sicher = 1
where d.eig_schr_gb similar to '%Feldberg - Uckermärkisch%'
--document_tokens @@to_tsquery('forderverein & seenlandschaf & templin & feldberg')

-- Thaer´sche Gutsverwaltung Möglin GmbH   15345 Reichenow-Möglin Hauptstraße 19
select (sum(d."Summe_A_Wald") over ()/10000), d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
--*
--d."Eig.Schreibweise GB" , d.document_tokens 
from bwi.df d 
where --d.document_tokens similar to '%agrargenossenschaft%'
--d.document_tokens @@to_tsquery('gemeind') and
d.eig_schr_gb similar to '%Gutsverwaltung%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'


update bwi.df d 
set bwi_ea = 4,
eiggrkl = 7,
sicher = 1
where d.eig_schr_gb similar to '%Feldberg - Uckermärkisch%'
--document_tokens @@to_tsquery('forderverein & seenlandschaf & templin & feldberg')



/* Eigentum Bundesländer
 * 
 */
-- Land Brandenburg - diverse Eigentümereintragungen
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
--*
--d."Eig.Schreibweise GB" , d.document_tokens 
from bwi.df d 
where --d.document_tokens similar to '%agrargenossenschaft%'
--d.document_tokens @@to_tsquery('gemeind') and
d.eig_schr_gb similar to '%Land Brandenburg%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'


update bwi.df d 
set bwi_ea = 2, --Staatswald
eiggrkl = 7, --eigentlich unnötig
eig_nr = 34659539, -- um zu einer einheitlichen Nummerierung zu kommen
bl=11 -- Länderschlüssel
,sicher = 2
where d.eig_schr_gb similar to '%Berliner Stadtgüter GmbH%'


--document_tokens @@to_tsquery('forderverein & seenlandschaf & templin & feldberg')
-- Berliner Stadgüter - wichtig um später in der Auswertung Berlin und Brandenburg auseinanderzuhalten
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
--*
--d."Eig.Schreibweise GB" , d.document_tokens 
from bwi.df d 
where 
d.eig_schr_gb similar to '%Berliner Stadtgüter%' 
--d.document_tokens similar to '%agrargenossenschaft%'
--d.document_tokens @@to_tsquery('gemeind') and
--d.eig_schr_gb similar to '%Berliner Stadtgüter GmbH%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'

update bwi.df d 
set bwi_ea = 2, --Staatswald
eiggrkl = 7, --eigentlich unnötig
eig_nr = 34659539, -- um zu einer einheitlichen Nummerierung zu kommen
bl=11 -- Länderschlüssel
,sicher = 2
where 
--d.eig_schr_gb similar to '%Berliner Stadtgüter GmbH%'
d.eig_schr_gb similar to '%Berliner Stadtgüter%'
--document_tokens @@to_tsquery('forderverein & seenlandschaf & templin & feldberg')

--Land Berlin   10179 Berlin Klosterstr. 59
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where 
--d.document_tokens @@to_tsquery('gemeind') and
d.eig_schr_gb similar to '%Land Berlin   10179 Berlin Klosterstr. 59%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'

update bwi.df d 
set bwi_ea = 2, --Staatswald
eiggrkl = 7, --eigentlich unnötig
eig_nr = 35685422, -- um zu einer einheitlichen Nummerierung zu kommen
bl=11 -- Länderschlüssel
,sicher = 2
where d.eig_schr_gb similar to '%Land Berlin   10179 Berlin Klosterstr. 59%';

-- Land Berlin
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where
d.eig_schr_gb like 'Land Berlin'
--d.eig_schr_gb similar to '%Land Berlin%' 
--d.document_tokens @@to_tsquery('berlin & land')

update bwi.df
set bwi_ea = 2, --Staatswald
eiggrkl = 7, --eigentlich unnötig
eig_nr = 35481187, -- um zu einer einheitlichen Nummerierung zu kommen
bl=11 -- Länderschlüssel
,sicher = 2
where
eig_schr_gb like 'Land Berlin'

--Land Berlin   10178 Berlin Karl-Liebknecht-Str. 31/33
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where
--d.eig_schr_gb like 'Land Berlin   10178 Berlin Karl-Liebknecht-Str. 31/33'
d.eig_schr_gb similar to '%10178 Berlin%' 
--d.document_tokens @@to_tsquery('berlin & 10178 &')

update bwi.df
set bwi_ea = 2, --Staatswald
eiggrkl = 7, --eigentlich unnötig
eig_nr = 35481187, -- um zu einer einheitlichen Nummerierung zu kommen
bl=11 -- Länderschlüssel
,sicher = 2
where
eig_schr_gb like 'Land Berlin'

--
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where
--d.eig_schr_gb like 'Land Berlin'
d.eig_schr_gb similar to '%Land Berlin%' 
--d.document_tokens @@to_tsquery('berlin & land')

update bwi.df
set bwi_ea = 2, --Staatswald
eiggrkl = 7, --eigentlich unnötig
eig_nr = 35481187, -- um zu einer einheitlichen Nummerierung zu kommen
bl=11 -- Länderschlüssel
,sicher = 2
where
eig_schr_gb like 'Land Berlin'

select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where 
d.document_tokens @@to_tsquery('berlin & !bundesfinanzverwalt & !bundesanstalt') -- and
--d.eig_schr_gb similar to '%Land Berlin   10179 Berlin Klosterstr. 59%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'


/*Körperschaftswald
 * 
 */
-- Städte
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
--*
--d."Eig.Schreibweise GB" , d.document_tokens 
from bwi.df d 
where --d.document_tokens similar to '%agrargenossenschaft%'
--d.document_tokens @@to_tsquery('gemeind') and
d.eig_schr_gb similar to '%Stadt Wittstock/Dosse%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'


update bwi.df d 
set bwi_ea = 3, --Körperschaftswald
eiggrkl = 7, --über 1000 ha
eig_nr = 34459709 -- um zu einer einheitlichen Nummerierung zu kommen
--bl=11 -- Länderschlüssel
,sicher = 1
where d.eig_schr_gb similar to '%Stadt Wittstock/Dosse%'

-- BVVG
select (sum(d."Summe_A_Wald") over ()/10000) waldsumme, d."Summe_A_Wald", d.eig_schr_gb, d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where
--d.eig_schr_gb like 'Land Berlin'
--d.eig_schr_gb similar to '%BVVG Bodenverwertungs- und -verwaltungs%' 
d.eig_schr_gb similar to '%Bodenverwertungs- und -verwaltungs GmbH%'
--d.document_tokens @@to_tsquery('berlin & land')

update bwi.df
set bwi_ea = 5, --Treuhand
--eiggrkl = 7, --eigentlich unnötig
eig_nr = 35905323 -- um zu einer einheitlichen Nummerierung zu kommen
--bl=11 -- Länderschlüssel
,sicher = 1
where
--eig_schr_gb similar to '%BVVG Bodenverwertungs- und -verwaltungs%' 
eig_schr_gb similar to '%Bodenverwertungs- und -verwaltungs GmbH%'


-- namensauswahltabelle
select be.tnr, be.enr,be.eg,be.eggrkl,bfev.wa,bfev.eg as ti_eg, bfev.eggrkl as ti_egrkl, d.bwi_ea  as d_eg, d.eiggrkl as d_egrkl,d.sicher,d.eig_schr_gb, d.eig_schreib_vereinh, d.document_tokens,  d.flstckt  
from bwi.b3_ecke_202212 be 
join bwi_vorkl.b3f_ecke_vorkl bfev on be.tnr=bfev.tnr and be.enr=bfev.enr 
join bwi.df d on d.tnr=be.tnr and d.enr=be.enr 
where bfev.wa between 1 and 5
and
bfev.eg=4
and d.sicher is null 
and d.ort is  not null 
--and bfev.eg is null
--order by d.enr asc, d.tnr

update bwi.b3_ecke_202212 be 
set 
eg = d.bwi_ea
,eggrkl = d.eiggrkl  
from bwi.df d 
where 
d.tnr=be.tnr and d.enr=be.enr
and d.sicher between 1 and 4
--=1

select d.name_1 
from bwi.df d 
order by d.name_1 

