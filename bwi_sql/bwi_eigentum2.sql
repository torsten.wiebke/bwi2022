-- CONNECTION: name=kubupostgis
-- New script in kubupostgis localhost  ${server} kubupostgis .
-- from torsten 
-- in /home/torsten/.local/share/DBeaverData/workspace6/bwi   
-- Date: 06.03.2023
-- Time: 18:46:50

-- Sciherungskopie von df am 13.03.2023
drop table bwi.dfc 

create table bwi.dfc as
select * from bwi.df d 

select *  from bwi.dfc

/* Waldfläche in df
 * to_tsvector('german', meine_tsvector_spalte) @@ to_tsquery('german', 'suchbegriff')
--AND similarity(meine_tsvector_spalte::text, 'suchbegriff') >= 0.5;
 * Die tsquery-Syntax verwendet Boolesche Operatoren wie & (AND), | (OR) und ! (NOT), um die Suchbegriffe zu kombinieren.
 * to_tsvector('german', dokument_tokens) @@ to_tsquery('german', 'suchbegriff1 & suchbegriff2')
AND similarity(dokument_tokens::text, 'suchbegriff1 & suchbegriff2') >= 0.5;
 */ --(sum(d."Summe_A_Wald") over ()/10000) waldsumme, 
select 
d."Summe_A_Wald", d.eig_schr_gb, d.eig_schreib_vereinh,--  d.document_tokens ,
d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where
d.document_tokens @@to_tsquery('Kirche & Dümde ')--Dietrich& Angelika')--Pfarre   & Hohenbucko')--Dennert')--Dahms&sylvia')--Guionneau & werner&london')--Buschmann&jörg')--Büttner& Ralf ')--Busse &Herbert')--schneider&günther')--Burkhardt &torsten')--Ribbeck&ingeborg')--
--Hilmer&antonia&kirchahorn')--Bundesrepublik& Deutschland ')--Stadt &Gransee')--Gemeinde & Wusterwitz')--Buschfeld &Henning ')--Bültermann')--Brune & renate')--Droste& Benedikt')--Burisch &Julian')--Dahlen& Frieder')--Buschmann &Hanna')--Dietzel &Ricarda')--Polz &Gisela')--Buder&wolfgang')--Buchholz &Thomas-Gisbert')--Brumm &Andreas')--Bruhn&ernst-august')--Brüggemann &Heike ')--Brückner& Norbert')--Brückl& Dominik')--Bruchmüller& Birgit')--Broel &Liselotte')--Brodach& Gottfried ')--Brockmann &Dieter')--Brockhaus&Karl-Heinz')--Brockdorff &Donata ')--Brinkhege& Peter')--Gemeinde & Kloster ')--Budack& Marco')--Brünneck')--Brink& Berthold ')--Briesenick & christel')--Briedigkeit ')--Bretzke& Jörg')--Bremer  ')--Bree& Jürgen')--Land&Brandenburg&Landesstraßenverwaltung')--Bremer& Wilfried ')--Alte &Calauer')--Bratke & Siegfried')--Bundesverkehrsverwaltung')--Bräunig &Dirk') 
--Braun & Astrid ')--Braumann& Thea')--Bräuer &Erika ')--Brätsch &Olaf ')--Brasse &Eberhard')--Brand-Welteke ')--Branding &Christoph')--Brandenstein-Zeppelin')--Brand &Dirk')--Brake &adolf')--Werner & erich')--Brachwitz')--Bredow& Mathias &potsdam ')--Botzet& Matthias ')--Breulmann& Marcus')--Brettin & Lutz')--Brach &Christian ')--Bozanic')--Böwe& Inge')--Bours &Georg')--Böttcher & Manfred')--Botanischer& Verein')--Bösenberg &Antje')--Bornkessel& Rene ')--Bork& reinhard ')--Borgmann &Margit')--Börgen & Anne')--Borchhardt&erhard')--Borchert &marcus')--Bolze & Steffen')
--Sayn-Wittgenstein-Berleburg & stanislaus')--Liebich &Margarete')--Hundertmark')--Graevenitz & Beatrice')--Barsewisch & julian &gisbert')--barsewisch&Jasper &gisbert')--Evangelische &Boitzenburger& Land')--diergardt & Leopold''')--Andre-von& Arnim&  Ferdinand')--Zahnow &Günter')--Forstwirtschaft& Jännersdorf')--Zahnow &Christa')--Ungnade & Ortwin')--Trägerwerk & Georg')--Straßburg &Harald') 
--Borchardt & heidemarie')--Bönsch &Gabriela')--Bolsmann & Gisela')--Bollmann &Rathenow')--Bolechowski')--Boldt& Matthias ')--Bolder & Lambertus ')--Bölcker')--Boissier& Doris')--Bohnhorst& Helmut')--Böhner& Peter')--Böhmer & Rita')--Böhme& Nadja')--Böhm& Mario ')--Bohm& Bruno ')--Böhlick &Ilona ')--Böhlcke ')--Böhla &Hermann')--Bogsch& Uwe')--Boge & irmgard')--Boge & norbert ')--Olearius& Joachim')--Naturschutzbund  & Prenzlau ')--Kolberg& Frank')--Boelk& Rico ')--Brauer& Elke')--Boelk ')--Boeck &Jürgen ')--Bochow &Manfred ')--Boche& Ronny')--Bochan& Annegret')--Bober & Jörg ')--Blankenhagen& Heiko ')--Bischoff &  Haus & Grund')--Blanchard ')--Binte')--Bindel& Walter')--kopka')--Crone&julia')--Buchholz &klaus ')--Bretschneider&matthias')
--Breßler')--Brechmann')--Brandt&cornelius')--Bösel')--Hempel')--Borkoisky ')--Böhmert&renate')--boden ')--bock')-- UweBlümel')-- Albert Block&hans')--Bliesener&stefanie')--Bleicke ')--Bleich ')--Blaszyk')--Blaske')--Blänkner &martin')--Bittner & ute')--Bischoff & Klaus ')--Birkenhagen ')--seehaus & hannelore')--Billert &volker ')--Bilke & gernod ')--Bierstedt ')--Bierhold')--Bienecke ')--Bielefeld ')--Bieck')--Beyer&heinrich')--Beyer &  14823')--Beyer &  15907')--Beutler & Neuruppin')--Bette & Uebigau ')--Bessert&dietmar')--Berteit &Berlin')--Bernott&Schwedt-Vierraden  ')--Berner & Hagen')--Berndt &Bad &Saarow')--Berndes & Nuthe-Urstromtal ')--Bodenkamp & Samern ')--Berghäuser & Berlin ')--Berger & udo ')--Berger & ingrid')--reinhard--Berger & Dietmar')--Berg &regine')--Benz & Schwanau ')--Benter &Spreenhagen')--Bensemann & Templin')--Bennewitz & Berlin')
--Benke & Ziezow ')--Benk & Rheinsberg')--Benischek & Kroppen')--Bengs& Bestensee')--Bertmaring &marita')--Bendrich')--Ben &tanfous')--Belß ')--Bellin &bernd')--Bela & Cottbus')--Bela &volkmar')--Beiche')--Behring &ralf')--Behrens & Hannelore  ')--Behrens &elke')--Rhode &therese')-- Elmar  12559 BerlinBehrendt & klaus')--Behr & 15926 ')--Behnke & Wildau ')--Behling & Liepe ')
--Behkalam & Berlin ')--Beelitz &  christoph ')--Beelitz &Ludwigshafen')--Beckmann &Bad& Wilsnack')--Beckmann & Brieselang')--Becker &wolfgang ')--Becker &Heidesee ')--Becker & robert')--Becker &  15299 ')--Becker & Bliesdorf')--Becker &  4936')--Becker & Ziltendorf')--Becker &Velbert ')--Becker & Bad &Hersfeld ')--Becker & heidemarie ')--Becker & udo ')--('Becker & gisela ')--Becker & bettina')--Becker & Gummersbach')--Beck & Frauendorf')--Bechtel &  16928')
--Beaucamp & Sailauf &martin')--Rhinower & Grundstücksgesellschaft')--Bayer & Kamern ')--Bayer &  16845 ')--Bauunternehmung & Jung')--Baumgartner & Offenburg')--Baumgart & Schlaubetal')--Baumgart & Ludwigsfelde ')--Baumann &  16230 ')--Bauerngesellschaft & Ziltendorfer & Niederung')--Baufeld & Kleissl & Vermögensverwaltungs')
--Bäuerliche & Produktionsgemeinschaft & Saßleben ')--Baum & sonja')--Seifert& Evelise ')--Baumgart & Berlin ')--Baumann & Kremitzaue')--Bauch & Cumlosen')--Bathe & Schönwalde-Glien')--Seifert & annerose')--Bäuerliche & Produktionsgemeinschaft & Beiersdorf/Freudenberg')--Baudissin-Zinzendorf & Hamburg')
--Batereau & Freital')--Basselet & München')--Batereau & Freital')--Bast & Gollenberg ')--Basselet & München')--Basigkow &  14778 ')--Baschin &  15913')--Bartsch & Rühstädt ')--Bartmann & Forch ')--Bartmann & Potsdam')--Bartig & Cottbus')--Bärtich &  4936 ')--Barth &  14469 ')--Barth &  42653 ')--Bartels &  14793')
--Barow &  15868  ')--Barth &  3222')--Bartels & 26532')--Bartel &  17291')--Bartel &  3159')--Bartel &  1990 ')--Bartel &  41540 ')--Barow &  15299 ')--Baronius &  gebhard ')--('Baronius ')--Baron &  17268')--Barnick & 16515')--Barnetz &  14827 ')--Barnetz &  christian ')--Bärkholz &  14806')--Baran &  3130')--Bangert &  14641 ')--Bambek &Tanja')--Bambek &  4849')--Balzke &  67259')--Balzke &  3159')--Balzer &  39319')--Balzer &  3130')--Balzer &  15848')--Balzer &  64319')--Baltot &  3130 ')--Balke &  15848')--Baldt &  12203')--Baldauf &  61191')--Baldauf &  15518 ')--Baldamus&  15848 ')
--Baier & 98574')--Bahr &  14669')--Baewert &  14793 ')--Badlehner &  82377')--Babing &  15926')--Babenz & 3229 ')--Baatz &  14822 ')--Averdunk &  12205 ')--Aurast &  16359 ')--Auras &  3249 ')--Auffermann &  14089')--Auert &  15936')--Arts &!gesellschaft  ')--Arts &  3044')--Arnold-Tauschhuber ')--Arnold &  12529 ')
--Arning &  31582')--Assmann &nicole')--Arnim &marie-luise&  51373')--Arnim &  76228 ')--Arndt &  Volkmar & werner')--Schneider &  19406 ')--Arndt &  16766')--Arndt &  14913 ')--Aust &  14823 ')--Arndt &  4916 ')--Arndt &  16278 ')--Arndt &  15936 ')--Arndt & Theisa')--Arndt &  16909')--Arlt & Bautzen ')--Appetz &  16278 ')--Antrick &  17268 ')--Axenbeck &  84546')--Baatz &  16845 ')--Baatz &  16259')--Antrup &  49525')--Antonius &  14943 ')--Andres & Jüterbog')--Amsel & Senftenberg')--Amsel &  15848')--Ameling &  48712') 
--Baresel &  16792 ')--Balzer &  64673')--Baier & 99444')--Bahro &  15890')--Bähr-Jurack & Guben ')--Bähr &   Schenkendöbern ')--Bähr &  guben &!5')-- Bahns & 26197')--Bahns &  65385 ')--Bahns &  15518 ')--Bagola & manfred')--Bader &  15837 &luckenwalder')--Backhaus & Stolzenau')--Bach & dresden') 
--Badke & roswitha')--Ambos & Frankfurt')--Andres & katrin')--Alvensleben &15518')--Ambrosius &Neiße-Malxetal')--Altrichter & Schwarzbach')--Altrichter & Wernigerode')--Altmann & Luckau/Uckro')--Alandt & Beelitz')-- AGW')--AGT')--Agro-Glien')



update bwi.df d
set
bwi_ea =2,
eiggrkl = 7,
sicher=4
where 
d.document_tokens @@to_tsquery('Land & Brandenburg')-- & landesforstverwalt &! grundstucksfond')
and sicher is null 

update bwi.df d
set
sicher=1
where 
d.document_tokens @@to_tsquery('bundesfinanzverwalt')

select d.*
from bwi.df d 
where d.tnr=47496 and d.enr =3

select --(sum(d."Summe_A_Wald") over ()/10000) waldsumme, 
d."Summe_A_Wald", d.eig_schr_gb, d.eig_schreib_vereinh,  d.document_tokens ,d.adresse_nr, d.eig_nr, d.bwi_ea,d.eiggrkl, d.sicher
from bwi.df d 
where --d.document_tokens similar to '%evangel%'
--d.eig_schr_gb % '%evangel%' AND similarity(d.eig_schr_gb, 'evangel') >= 0.1 -- findet alle Einträge mit einer Ähnlichkeitsbewertung von mindestens 0,5 mit dem Suchbegriff 
--d.document_tokens % '%evangel%' AND similarity(d.eig_schr_gb, 'evangel') >= 0.1
--to_tsvector('german', d.eig_schr_gb) @@ to_tsquery('german', 'Domanski & Bernhard') AND similarity(d.eig_schr_gb, 'Domanski') >= 0.01
-- '1':10 '1968':7 'bergbau':5 'bergbau-verwaltungsgesellschaf':4 'knappenstrass':9 'lausitz':1 'mitteldeutsch':3 'senftenberg':8 'verwaltungsgesellschaf':6
--d.document_tokens @@to_tsquery('Schulze & Gehren') -- Herbert  15926 Heideblick Gehren Gerostraße 3 A--Schulz & schönwald')--Herbert  15910 Schönwald/Schönwalde Bahnhofstraße 42--gemeind & drebkau')--jerischk') 
d.document_tokens @@to_tsquery('Altenkirch & planebruch')--Jens  14822 Planebruch OT Cammer Hauptstraße 40 a--Altenburg & schipkau')--Frieda  1998 Schipkau --Allgaier & blaubeuren')--Ernst  89143 Blaubeuren Sotzenhauser Str. 16--Albrecht & Rietzneuendorf-Staakow')--Norbert  15910 Rietzneuendorf-Staakow Dorfstraße 48--Albrecht & neustadt | albrecht &neustadt/dosse')--Gunnar  16845 Neustadt (Dosse) Kyritzer Straße 4--Albertziok & planetal')--Dagmar  14806 Planetal Dorfstr. 11
--Albath & mühlenberge')-- Markus Jan Willi Heinz  14715 Kotzen Bahnhofstraße 6--Aland & jutta')--  16845 Sieversdorf-Hohenofen GT Sieve Pohlweg 11Airport')--Airport Development A/S   1306 Kopenhavn K Kronprinsessegade 36--Ahrens & cloppenburg')--Richard  49661 Cloppenburg Bahnhofstraße 2
--Ahrens &Lastrup')--Helmut  49688 Lastrup Ebenesch 6--Ludowig &brodau')--Ilka  23730 Brodau Zum Gut 1-- AGRO & senftenberg')-- Landwirtschaftliche Inventar Verpachtungs- und Treuhand   1945 Senftenberg Schwarzbacher Straße 1--Albrecht & Kolkwitz')--Sven Thomas  3099 Kolkwitz Grabenstraße 12
--Agrofarm & Sonnewalde')--Goßmar eG   3249 Sonnewalde OT Goßmar Finsterwalder Landstraße 2Agraria & Schwarzenfeld')--Grundstücks GbR   92521 Schwarzenfeld Buchtal 1--AGRARGESELLSCHAFT & UCKERLAND')-- mbH   17268 Gerswalde Ort Weiler 12Agrar-GmbH & Grieben')--   16775 Löwenberger Land Dorfstraße 35
--Agrargesellschaft & Milsa')-- mbH   17326 Brüssow Bagemühler Str. 3 a--Agrargesellschaft & Sperenberg')--   15838 Am Mellensee Baruther Landstraße 2--Saldern & Berlin')-- Lewin  12161 Berlin Goßlerstraße 21--Agrargenossenschaft & Züllsdorf')-- eG   4916 Herzberg Hinter den Gärten 6
--Agrarproduktivgenossenschaft & Viesecke')--   19336 Plattenburg Viesecker Straße 6Agrargenossenschaft & Trebbin')-- eG   14959 Trebbin Trebbiner Straße 12Agrargenossenschaft & Thomas')-- Müntzer Krahne eG   14797 Kloster Lehnin Krahner Hauptstr. 3Agrargenossenschaft & Stölln')-- eG   14728 Gollenberg Apfelallee 8
--Agrargenossenschaft &  unterspreewald')-- Radensdorf eG   15907 Lübben (Spreewald) --Airport & Kopenhavn')--Development A/S   1306 Kopenhavn K Kronprinsessegade 36--Agrargenossenschaft &Schlaubetal')-- Kieselwitz eG   15890 Schlaubetal LPG-Weg 1
--Agrargenossenschaft & Havelaue')-- Hohennauen eG   14715 Havelaue Spaatzer Hauptstr. 1--Albrecht & elona')-- Elona  16866 Kyritz Fichtengrund 44--Rex & frankfurt')-- Peter  15234 Frankfurt (Oder) Witebsker Str. 27--Zuydtwyck & lanz')-- Maximilian  19309 Lanz Bärwinkel 1
--Waltasaari & berlin')--Wittkowski & kantow')-- Zitta  16845 Kantow Dorfstraße 26--Wishöth &kremmen ')--Veikko  16766 Kremmen Seestraße 34--Wiesejahn & spremberg')-- Helga  3130 Spremberg Schillerstraße 2Waltasaari & berlin')-- Anika  10115 Berlin Elisabethkirchstraße 2
--Voß & Merzhausen/Breisgau')-- Arend  79249 Merzhausen/Breisgau Von-Schnewlin-Str. 3 BLudowig & bordau')-- Gustav  23730 Bordau Zum Gut 1--Voigt & halbe')-- Lothar  15757 Halbe Schweriner Straße 30 B--Vettin &gumtow')-- Christa  16866 Gumtow Lindenallee 39--Venzke & rüdnitz')-- Hartmut  16321 Rüdnitz Bernauer Str. 10
--Uckro & hanns-detlef')-- Hanns-Detlef  15926 Luckau Paseriner Weg 1Näthe & rüdnitz')-- Claudia  16321 Rüdnitz Sechsrutenweg 4--Müller-Brause & rostock')-- Susan  18057 Rostock Stampfmüllerstraße 37
--Müller & ebstorf')-- Helmut  29574 Ebstorf Am Weinberg 9--Muhr & attendorn & gbr')-- Thomas  57439 Attendorn Mühlhardt 45--Meyer-Johann & !hermann')-- Carsten  32130 Enger Am Mühlenbruch 21--Tätzel ')--& dieter')-- Dieter  15926 Luckau Kaden 40
--Summerer &andernach')--Günther  56626 Andernach Antel 112--Stock & berlin')--Joachim  10719 Berlin Pariser Str. 9--Stiftung & Bier')-- August Bier für Ökologie und Medizin   50937 Köln Kerpener Straße 62--Stengel &stadtlohn')--Viktoria  48703 Stadtlohn Kreuzstraße 40
--Stegemann & Kerstenbruch')-- Emil  0 Kerstenbruch --Stanzeit & brück')-- Uwe  14822 Brück Gartenweg 2--Skalda & wandlitz')-- Stephan  16348 Wandlitz Prenzlauer Chaussee 155--Sieber & Dresden')-- Jörg Ernst  1127 Dresden Erfurter Straße 3--Schulz & gerdshagen')-- August  16928 Gerdshagen --Scherer & Falkensee')
-- Anja  14612 Falkensee Wismarer Str. 5--Petschick & Golßen/Zützen')-- Ingrid  15938 Golßen/Zützen Springweg 3--Pabst & Ingelsheim')-- Ernst  0 Ingelsheim --Ostman & osnabrück')-- Dominik  49090 Osnabrück --Mattes & Neu-Ulm')--Berta  0 Neu-Ulm --Mariak & laatzen')-- Mareikje  30880 Laatzen An der Dehne 2 D--Maraun & rathenow')-- Ute  14712 Rathenow Waldemarstr. 18Lucke & Ingolstadt')
--Monty  85049 Ingolstadt Ingolstädter Straße 140--Lippe & Detmold')--Stephan  32756 Detmold Schloßplatz 1--Liljenqvist & Lich')--Caroline  35423 Lich --Lichtenberg & Barßel')--Tino  26676 Barßel Skagerrakstraße 8--Lausitz & Energie & Bergbau')-- AG   3050 Cottbus Vom-Stein-Straße 39--Lange& Pankow')-- Frank  16928 Groß Pankow OT Helle Alte Dorfstr. 3
--Sader & Storkow')-- Christiane  15859 Storkow Herweghstr. 2--Land &Brandenburg')-- (Landesforstverwaltung)   14473 Potsdam Heinrich-Mann-Allee 103 Haus 5--Rosin & Brigitte')    --Nicol & Max')--    --Materock & Elke')    --Lorenz & Heinz')--    --Lehmann & Otto')    --Lazarus & Neuhausen/Spree')-- Richard  3058 Neuhausen/Spree 
--Pey & Breese')-- Günter  19322 Breese Rundling 11--Lau & Kaufering')--Karin  86916 Kaufering Höflebogen 34--Lange & Elsterwerda')-- Klaus  4910 Elsterwerda Bergstr. 45--Lange & cottbus')-- Ines  3048 Cottbus Lerchenstraße 42--Lämmermann &rheinsberg')-- Ralph  16818 Rheinsberg Dorfstraße 32--Lamm & Rehfelde')-- Kathrin  15345 Rehfelde Alt Werder 6--Lahn & wollin')--Marko  14778 Wollin Friesdorf Nr. 1 A
--Kühnlein & Dittenheim')--Alexander  91723 Dittenheim Am Hilpert 12--Krüger & Siehdichum')--Frank  15890 Siehdichum Zum Waldhaus 4--Kraatz & havelsee')-- Simone  14798 Stadt Havelsee OT Fohrde Fohrder Hauptstr. 13--Kösters & Rheine')--Ewald  48432 Rheine Goldbergstraße 113--Knuppe & lebusa')--Bärbel  4936 Lebusa Dorfstraße 17
--Schäff & Potsdam')--Bernd  14469 Potsdam Langhansstraße 8--Schack-Hartmann')-- Brigitte    --Rosin & Rita')--    Agrar & Rheinsberg')-- GmbH   16837 Rheinsberg Wittstocker Straße 1--Falkenstein & Obersulm')-- Forst GmbH & Co. KGaA   74182 Obersulm Danziger Straße 28--Bösing & vreden')-- Immobilien GmbH   48691 Vreden Ammeloe 13--BLUMAG')-- Marktfrucht GmbH   16928 Heiligengrabe Siedlung 3
--Behnke & Potsdam')-- Recycling GmbH   14476 Potsdam Ketziner Straße 32 d--BBRE')-- Berlin-Brandenburg Real Estate GmbH   13509 Berlin Holzhauser Straße 9--Reschke & friedland')--Kerstin  15848 Friedland Lindow 7--Remy & Gabriele')-- Dorothee Beatrix    Rakow & Rathenow')-- Jörg  14712 Rathenow Rheinstr. 35--Queling &Belzig')-- André  14806 Bad Belzig Wallgasse 1--Prinz & Recklinghausen')-- Johan Jimi  45657 Recklinghausen Hagemannstraße 10
--Wilke & Dieter')--    --Pflaumer &Arnsberg')-- Wulf-Heinz  59757 Arnsberg Im Stadtwald 1--Agrargenossenschaft & Gräfendorf')-- eG   4916 Herzberg (Elster) Postbergaer Weg 16Agrargenossenschaft & Giesensdorf')-- Goldene Ähre Giesensdorf eG   15848 Tauche Brieschter Weg 8
--Agrargenossenschaft & Neuzelle')-- eG   15898 Neuzelle Lindenpark 1--KR & Fläming | Stiftung & heilig')-- Forst GbR     --Kenzler & Heiligengrabe')-- Gerda  16928 Heiligengrabe Straße der Einheit 63--Kapitzke &Grünefeld')-- Alexandra  14641 Grünefeld Bäckerweg 6Kalz & Berlin')-- Annett  12169 Berlin Bergstraße 70
--Roßmann & Gräben') --Uwe  14793 Gräben Kietz 39--Hummel & Ziesar')--Lothar  14793 Ziesar Dorfstr. 20 E--Hoffmann & Berlin')-- Walter  14109 Berlin Bismarckstraße 21--Hilmer & Antonia & Torgau')    --Hohmann & Rene')    --Henkel & Wilfried')--Wilfried  15848 Beeskow Kohlsdorfer Str. 27--Helm & holger ') --Gerald  16845 Wusterhausen/Dosse Seestraße 3
--Harlinghausen & Tecklenburg')-- Werner  49545 Tecklenburg Bogenstraße 6--Hantelmann & Jutta')--    --Hanel & Günther')--  0 Berlin --Habel & Jennifer')--Neuzelle')-- Nicole  15898 Neuzelle Kruggasse 30--Haase & Peter')    
--Grabs & Karl |Schulze & Ferdinand| Külper& Gutthilf| Weill & Siegmund |Daßmann & Bertha | Grabs & Martha | Alisch & Emil |Steinicke & Ernst | Schulze & Herbert ')    --Richtberg & Wenzenbach')-- Britta  93173 Wenzenbach An der Zell 3--Schumann & Marie')    --Gebhardt & Ventschow')-- Waltraud  19417 Ventschow Dorfstraße 8--Gaude & Templin')-- Eckhard  17268 Templin Petersilienweg 2--Gärtner & Frauendorf')-- Anja  1945 Frauendorf Hauptstraße 49
--Fuchs & Kolkwitz')-- Johann  3099 Kolkwitz 
--Richtberg & chambesy')--Bettina  1292 Chambesy --Friedrich & planetal')-- Andrea  14806 Planetal Mühlenstr. 8--Frankowiak & Marie')    --Förster & Heidesee')-- Isabell  15754 Heidesee Gussower Dorfstraße 7--Förderverein & Feldberg')--Uckermärkische Seenlandschaft e. V   17268 Templin Am Markt 13--Schumann & liane')-- Liane  3253 Dob.-Kirchhain --Fölster & Gerd & gesellschaft')--('Fölster & Gabriele & !gesellschaft')--  20149 Hamburg Abteistr. 33--Fischbeck')-- Alfred    --Gerstädt')-- Wilhelm    --Erdmann & Jörg')-- Horst Georg  58135 Hagen Kipperstraße 80--Ehrhardt & Antje')--  15913 Schwielochsee Ressener Dorfstraße 3--'Doerks')-- Ralf  16928 Bölzke Heidelberger Weg 1
--'altbelgern')--'pfarre &altbelgern')--'stiftung & sielmann')--'Derwis')-- Jens  15377 Ringenwalde --'Riewe')-- Reinhard  10115 Berlin Schönholzer Straße 4--'Ribbeck & nikolaus')--Ribbecke & Horst')    --Buttgereit')-- Stefan  58579 Schalksmühle Eichendorffstraße 23--Drenske')-- Willi    --Wendt & Carl')-- Carl Friedrich    
--'stiftung & sielmann')--Heinz-Sielmann-Stiftung   37115 Duderstadt --gemeinde & saarow')-- & bollensdorf')--gemeinde & neuhausen/spree')--gemeind & fünfeichen | gemeinde & schlaubetal')-- & hohenlobbese')-- & drahnsdorf')-- & protzel')-- & breitenau')-- & kieselwitz')-- | edv')-- RT Rat der Gemeinde Jerischke   3159 Döbern --Bruchmüller& Erika ')
--    Bradatsch')-- Kathrein  1998 Schipkau Markt 2--Borchers')-- Karsten  27612 Loxstedt An der Reitbahn 9Bredow & friedrichsthal')-- Ingo  66299 Friedrichsthal Berner & hepp')-- Wolfgang  59505 Bad Sassendorf Hepper Straße 5
--Bergmann & Stefan')-- Wolfgang  15374 Müncheberg Elisenhof 1Bergmann & Eckhard')--  4916 Herzberg (Elster) Züllsdorfer Mittelstraße 19--Bergholz & ot')-- Rosemarie  14823 Rabenstein/Fläming OT Garrey Dorfstr. 22Bausamer')-- Werner  14552 Saarmund Kolonie 2
--Land & Brandenburg & landesforstverwalt &! grundstucksfond')
--Bauch & Otto | Schmidt & Milly')
--bundesfinanzverwalt')
--Beuster & Guido')
--'Bleike & Werner')
--'Bahl')-- Wilfried  3238 Lichterfeld Dorfstraße 18
--'Aschenborn & Minna')
--'bergbau-verwaltungsgesellschaf') 
--or similarity(d.eig_schr_gb, 'lausitz') >= 0.5
--or similarity(d.eig_schr_gb, 'lmbv') >= 0.1
--'evangel & eberswalde & st.marien') --AND similarity(d.eig_schr_gb, 'evangel &eberswalde') >= 0.01
--,and
--d.eig_schr_gb similar to '%Stadt Wittstock/Dosse%' --and
--d.bwi_ea is null
--or d.adresse_nr = '36471498'
--and d.sicher is null 