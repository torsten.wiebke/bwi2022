-- CONNECTION: name=kubupostgis
-- New script in kubupostgis localhost  ${server} kubupostgis .
-- from torsten 
-- in /home/torsten/.local/share/DBeaverData/workspace6/bwi   
-- Date: 06.03.2023
-- Time: 21:24:44

/*Eigentum
 * 
 */
-- join mit Daten in DB
-- weiß nicht mehr was ich damit gemacht hatte
select d.tnr,d.enr, d.eig_schr_gb,d.adresse_nr,  d.bwi_ea d_bwi_ea,bfev.eg ecke_eg,d.eiggrkl d_eiggrkl, bfev.eggrkl ecke_eiggrkl, 
(d."Summe_A_Wald"/10000) d_wald,(szan.summe_wald_qm/10000) ad_wald,
(sum(d."Summe_A_Wald") over (partition by d.adresse_nr)/10000) d_wald
,(sum(szan.summe_wald_qm) over (partition by szan.adr_nr)/10000) ad_wald  
from 
bwi.df d 
join bwi_vorkl.b3f_ecke_vorkl bfev on bfev.tnr = d.tnr and bfev.enr =d.enr
join bwi.summewaldfl_zu7598adr_nr szan on szan.adr_nr = d.adresse_nr 
where 
d.bwi_ea <> bfev.eg
order by (sum(szan.summe_wald_qm) over (partition by szan.adr_nr)/10000) desc

-- Waldfläche aus allen Waldbesitzerdaten
CREATE OR REPLACE FUNCTION get_adressen_by_texts(texts TEXT) RETURNS TABLE(adresse_nr INTEGER) AS $$
BEGIN
    RETURN QUERY
    SELECT d.adresse_nr
    FROM bwi.df d
    WHERE d.document_tokens @@ to_tsquery(texts);
END;
$$ LANGUAGE plpgsql;

select * from public.get_adressen_by_texts(Bindel& Walter)

SELECT 
    CASE 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 5 THEN '11: bis 5 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 10 THEN '12: > 5 bis 10 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 20 THEN '13: > 10 bis 20 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 50 THEN '2: > 20 bis 50 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 100 THEN '3: > 50 bis 100 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 200 THEN '4: > 100 bis 200 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 500 THEN '5: > 200 bis 500 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 1000 THEN '6: > 500 bis 1000 ha' 
        ELSE '7: > 1000 ha' 
    END AS klassen,
    SUM(szan.summe_wald_qm)/10000::real AS flaeche
FROM bwi.summewaldfl_zu7598adr_nr szan
WHERE szan.adr_nr IN (select  d.adresse_nr--,d.*  
    from 
    bwi.df d 
    where --d.eig_schr_gb similar to '%Lausitzer und%'
    d.document_tokens @@to_tsquery('Kirche & Dümde ')--('Kirchengemeinde & Görzke')--('Dewitz')--('Devrient')--('Deter&ingeborg')--('Donner &götz')--('Donner & uwe')--('Giroverband')--('Deutsch & Thomas')--('Deter & Günter ')--('Depping')--('Deparade')--('Denk')--('Demgensky')
    --('Delger')--('Deichsler')--('Deichsel&martina')--('Degler')--('Degelow')--('Decke & Sabine')--('Decke & Christian')--('Bruyn')--('Daubitz')--('Daube')--('Dänschel')--('Danne')--('Daniels&hans')--('Dames')--('Dahse')--('Dahlitz')--'Domstift')--('Day&josef')--('Day&eva')--('Dabo')
    --('Czerwonka')--('Czaskowski')--('Czaja')--('CVI')--('Conrady-Kaiser')--('Conrady&michael')--('Conrad& Thomas  ')--('Conrad& Ronny  ')--'Colmsee-Sauer ')--('Clausing&christine')--('Clausen&jens')--('Ciesla')--('Dietrich& Angelika')--('Dietrich& Edelgard')
    --('Dietrich& Hannelore')--('Dietrich& Martin& 3172')--('Dietrich& Martin&niesky ')--('Pfarre   & Hohenbucko')--('Dennert')--('DB&!seegers')--('Czudnochowski&sabine')--('Czudnochowski&janina')--('Daher& Erika')--('Daher& Adolf')--('Dahms&sylvia')--('Cierzniak')--('Cielek')--('Christ')
    --('Carasusßn')--('Campbell-Arnim')--('Caleta')--('Guionneau & werner&london')--('Guionneau & werner&!london')--('Guionneau & C.')--('Buschmann&jörg')--('Büttner& Ralf ')--('Büttner& Monika ')--('Büttner& Karin ')--('Büttner& Heidi ')--('Büttner& Bernhard ')--('Busse &Herbert')
    --('Busse &Anke')--('schneider&günther')--('Buschow&mario')--('Buschack')--('Dähne&günter')--('Burisch&jörn')--('Bürger & Horst')--('Bürger & Elisabeth')--('Burchartz')--('Büning')--('Burkhardt &torsten')--('Burkhardt&yvonne')--('Ribbeck&ingeborg')--('Ribbeck&heinz')--('Ribbeck&dirk')--('Hilmer&antonia&kirchahorn')--('Hilmer&günther')--('Stadt &Gransee')
    --('Gemeinde & Wusterwitz')--('Bulligk')--('Bullan')--('Budzinski')--'Buchholz&René')--('Buchholz&Eberhard')--('Buchholz&gernot')--('Buchholz&christel')--('Bublitz')--B('Brüssow&frank&luckenwalde')--('Brunsch')--('Brunnert')--('Brüning&gerd')--('Christiani')--'Chriske')--('Buschfeld &Henning ')--('Bültermann&karin')--('Bünau& Rudolf ')--('Brune & renate')--('Brune & falk')--('Brune&claudia')--('Brilke')
    --('Dahlen& Frieder')--('Droste& Benedikt')--('Burisch &Julian')--('Dahlen& Frieder')--('Buschmann& Hanna')--('Dietzel &Ricarda')--('Polz &Gisela')--('Buder&wolfgang')--('Buder&dieter')--('Buchholz &Thomas-Gisbert')--('Buchholz &Roswitha')--('Brumm &Andreas')--('Bruhn&ernst-august')--('Brüggemann &Heike ')--('Brückner& Norbert')
    --('Brückl')--('Bruchmüller& Birgit')--('Broel &Liselotte')--('Brodach& Gottfried ')--('Brockmann &Dieter')--('Brockhaus&Karl-Heinz')--('Brockdorff &Donata ')--('Brinkhege& Peter')--('Gemeinde & Kloster ')--('Gemeinde & Kleinmachnow ')--('Gemeinde & Kölzig ')--('Gemeinde & Karstädt ')
    --('Gemeinde & Hohenbucko ')--('Gemeinde & Hirschfelde ')--('Gemeinde & Herzberg&lindow')--('Gemeinde & Heiligengrabe')--('Gemeinde &Kreutz/Havel')--('Gemeinde &Gerswalde')--('Gemeinde &Briesen')--('Gemeinde &Bremsdorf')--'Gemeinde &Proschim')--('Gemeinde &Hohenbucko')--('Gemeinde &Hohenfinow')--('Budack& Marco')
    --('Brünneck')--('Brink& Berthold ')--('Briesenick & christel')--('Briesenick & brunhilde')--('Briedigkeit& Elke ')--('Bretzke& Jörg')--('Bremer & Renate ')--('Bree& Jürgen')--('Bredel &Thorsten')--('Bremer& Wilfried ')--('Bratke & Siegfried')--('Bräuning-Krätzig')--('Bräunig &Dirk') --('Braun & Astrid ')
    --('Braumann& Thea')--('Bräuer &Erika ')--('Brätsch &Olaf ')--('Brasse &Eberhard')--('Brand-Welteke ')--('Branding &Christoph')--('Brandenstein-Zeppelin')--('Brand &Dirk')--('Brake &adolf')--('Werner & erich')--('Brachwitz')--('Bredow& Mathias &potsdam ')--('Bredow& Mathias &!potsdam ')--('Botzet& Matthias ')
        --('Breulmann& Marcus')--('Brettin & Lutz')--('Brach &Christian ')--('Bozanic')--('Bozanic& Ante')--('Böwe& Inge')--('Bours &Georg')--('Böttcher & Manfred')--('Botanischer& Verein')--('Bösenberg &Antje')--('Bornkessel& Rene ')--('Bork& reinhard ')--('Bork& Monika ')--('Borgmann &Margit')--('Börgen & Anne')
    --('Borchhardt&erhard')--('Borchhardt&anita')--('Borchert &marcus')--('Borchert &Burkhard ')--('Bolze & Steffen')--('Sayn-Wittgenstein-Berleburg & stanislaus')--('Sayn-Wittgenstein-Berleburg & almut ')--('Sayn-Wittgenstein-Berleburg &constantin ')--('Liebich &Margarete')--('Hundertmark')
    --('Graevenitz & Beatrice')--('Barsewisch & julian &gisbert')--('Barsewisch & julian &!gisbert')--('Barsewisch &York ')--('barsewisch&Jasper &gisbert')--('barsewisch&Jasper &!gisbert')--('Evangelische &Boitzenburger& Land')--('diergardt & Leopold''')--('Andre-von& Arnim&  Ferdinand')--('Zahnow &Günter')--('Forstwirtschaft& Jännersdorf')
    --('Forstgut & Jännersdorf')--('Zahnow &Christa')--('Ungnade & Ortwin')--('Trägerwerk & Georg')--('Straßburg &Harald')--('Borchardt & heidemarie')--('Borchardt & Andre')--('Bönsch &Gabriela')--('Bolsmann & Gisela')--('Bollmann &Rathenow')--('Bolechowski &Marianne')--('Boldt& Matthias ')--('Bolder & Lambertus ')--('Bölcker')--('Boissier& Doris')--('Bohnhorst& Helmut')--('Böhner& Peter')--('Böhmer & Rita')
    --('Böhme& Nadja')--('Böhm& Mario ')--('Bohm& Bruno ')--('Böhlick &Ilona ')--('Böhlcke&karl ')--('Böhla &Hermann')--('Bogsch& Uwe')--('Bogen& Norbert')--('Boge & norbert ')--('Boge & lotar ')--('Boge & lars ')--('Boge &Irmgard ')--('Olearius& Joachim')--('Naturschutzbund  & Prenzlau ')
        --('Kolberg& Frank')--('Brauer& Elke')--('Boelk ')--('Boelk &Andreas')--('Boeck &Jürgen ')--('Bochow &Manfred ')--('Boche& Ronny')--('Bochan& Annegret')--('Bober & Jörg ')---('Blankenhagen& Heiko ')--('Bischoff &  Haus & Grund')--('Blanchard ')--('Binte')--('Bindel& Walter')
    )
--GROUP BY klassen;

select round(sum(szan.summe_wald_qm)/10000::real) as ha,sum(szan.summe_wald_qm)/10000::real as komma_ha 
from bwi.summewaldfl_zu7598adr_nr szan
where szan.adr_nr in (
    select  d.adresse_nr--,d.*  
    from 
    bwi.df d 
    where --d.eig_schr_gb similar to '%Lausitzer und%'
    d.document_tokens @@to_tsquery('Bindel& Walter')--('Crone&julia')--('Crone&michael')--('Crone&katja')--('Crone&katja')--('Buchholz &klaus ')--('Bretschneider&matthias')--('Bretschneider&dirk')--('Bretschneider &claudia')--('Breßler')--('Brechmann')--('Brandt&cornelius')
    --('Brandt&felix')--('Brandt&dirk')--('Brandt&reinhard')--('Bösel')--('Hempel')--('Borkoisky ')--('Böhmert&renate')--('Böhmert&manfred')--('boden ')--('Bödeker ')--('bock')--('blutke')--('blunk')--('Blumrodt')--('blume')--('Blohm')--('Blümel')--('Block&hans')--('Block&wolfgang')--('Block&reinhardt')
        --('Block&jürgen')--('Bliesener&stefanie')--('Bliesener&Roland')--('Bleicke ')--('Bleich ')--('Blaszyk')--('Blaske & Burkhard')--('Blänkner &martin')--('Blänkner & Angelika ')--('Bittner & ute')--('Bittner & reinhard')--('Bittner & Heinz')--('Bischoff & Klaus ')--('Birkenhagen & Lothar ')--('seehaus & hannelore')
    --('Birk & rita ')--('Birk & Manfred ')--('Billert &volker ')--('Billert &olaf ')--('Billert & Gabriele')--('Bilke & gernod ')--('Bilke & Astrid ')--('Bierstedt ')--('Bierhold')--('Bienecke & Michael')--('Bielefeld & Ruth &33611')--('Bielefeld & Ruth &16928 ')--('Bieck &Heidemarie ')
    --('Beyer&heinrich')--('Beyer&karsten')--('Beyer& Heike')--('Beyer &  14823')--('Beyer &  15907')--('Beutler & Neuruppin')--('Bette & Uebigau ')--('Bessert&dietmar')--('Bessert& christine')--('Berteit &Berlin')--('Bernott&Schwedt-Vierraden  ')--('Berner & Hagen')--('Berndt &Bad &Saarow')
    --('Berndes & Nuthe-Urstromtal ')--('Bodenkamp & Samern ')--('Berghäuser & Berlin ')--('Berger & udo ')--('Berger &Torsten ')--('Berger & ingrid')--('Berger & reinhard')--('Berger & heiko')--('Berger & Dietmar')--('Berg &regine')--('Berg& thomas')--('Benz & Schwanau ')
    --('Benter &Spreenhagen')--('Bensemann & Templin')--('Bennewitz & Berlin')--('Benke & Ziezow ')--('Benk & Rheinsberg')--('Benischek & Kroppen')--('Bengs& Bestensee')--('Bertmaring &marita')--('Bertmaring &Paul')--('Bendrich')--('Ben &tanfous')--('Belß ')--('Bellin &bernd')--('Bellin & 14797')--('Bela & Cottbus')
    --('Bela &volkmar')--('Bela&margit')--('Beiche')--('Behring &ralf')--('Behrens & Hannelore  ')--('Behrens &elke')--('Rhode &therese')--('Rhode & gerd')--('Rhode & elmar')--('Behrendt & klaus')--('Behrendt &hans-joachim')--('Behrendt & guido ')--('Behrendt &Gisela')--('Behrendt &  bodo')--('Behr & 15926 ')
    --('Behnke & Wildau ')--('Behling & Liepe ')--('Behkalam & Berlin ')--('Beelitz &  christoph ')--('Beelitz &Ludwigshafen')--('Beckmann &Bad& Wilsnack')--('Beckmann & Brieselang')--('Becker &wolfgang ')--('Becker &Heidesee ')--('Becker & robert')--('Becker &  15299 ')--('Becker & Bliesdorf')
    --('Becker &  4936')--('Becker & Ziltendorf')--('Becker &Velbert ')--('Becker & Bad &Hersfeld ')--('Becker &Potsdam ')--('Becker & heidemarie ')--('Becker & Sonnewalde ')--('Becker & udo ')--('Becker & gisela ')--('Becker & Perleberg ')--('Becker & bettina')
    --('Becker & Gummersbach')--('Beck & Frauendorf')--('Bechtel &  16928')--('Beaucamp & Sailauf &martin')--('Beaucamp & Sailauf &maria')--('Rhinower & Grundstücksgesellschaft')--('Bayer & Kamern ')--('Bayer &  16845 ')--('Bauunternehmung & Jung')
    --('Baumgartner & Offenburg')--('Baumgart & Schlaubetal')--('Baumgart & Ludwigsfelde ')--('Baumann &  16230 ')--('Bauerngesellschaft & Ziltendorfer & Niederung')--('Baufeld & Kleissl & Vermögensverwaltungs')--('Bäuerliche & Produktionsgemeinschaft & Saßleben ')
    --('Baum & sonja')--('Baum & silke')--('Seifert& Evelise ')--('Baumgart & Berlin ')--('Baumann & Kremitzaue')--('Bauch & Cumlosen')--('Bathe & Schönwalde-Glien')--('Seifert & annerose')--('Bäuerliche & Produktionsgemeinschaft & Beiersdorf/Freudenberg')--('Baudissin-Zinzendorf & Hamburg')
    --('Basselet & München')--Batereau & Freital')--('Bast & Gollenberg ')--('Basselet & München')--('Basigkow &  14778 ')--('Baschin &  15913')--('Bartsch & Rühstädt ')--('Bartmann & Forch ')--('Bartmann & Potsdam')--('Bartig & Cottbus')--('Bärtich &  4936 ')
    --('Barth &  14469 ')--('Barth &  42653 ')--('Bartels &  14793')--('Barow &  15868  ')--('Barth &  3222')--('Bartels & 26532')--('Bartel &  17291')--('Bartel &  3159')--('Bartel &  1990 ')--('Bartel &  41540 ')--('Barow &  15299 ')--('Baronius &  gebhard ')--('Baronius & kristin')
    --('Baronius &  gebhard ')--('Baron &  17268')--('Barnick & 16515')--('Barnetz &  14827 & !dorfstr')--('Barnetz &  christian & 15926 & Luckau')--('Barnetz &  christian & 15926 & !Luckau')--('Barnetz &  christian & 15834')--('Barnetz &  christian & 14827')--('Bärkholz &  14806')
    --('Baran &  3130')--('Bangert &  14641 ')--('Bambek &Tanja')--('Bambek &  4849')--('Balzke &  67259')--('Balzke &  3159')--('Balzer &  39319')--('Balzer &  3130')--('Balzer &  15848')--('Balzer &  64319')--('Baltot &  3130 ')--('Balke &  15848')--('Baldt &  12203')--('Baldauf &  61191')--('Baldauf &  15518 ')--('Baldamus&  15848 ')--('Baier & 98574')
    --('Bahr &  14669')--('Baewert &  14793 ')--('Badlehner &  82377')--('Babing &  15926')--('Babenz & 3229 ')--('Baatz &  14822 ')--('Averdunk &  12205 ')--('Aurast &  16359 ')--('Auras &  3249 ')--('Auffermann &  14089')--('Auert &  15936')--Arts &!gesellschaft  ')--('Aßmann &  heinz')--('Arts &  3044')--('Arnold-Tauschhuber ')--('Arnold &  12529 ')
    --('Arning &  31582')--('Assmann &nicole')--('Assmann &monika')--('Assmann &kai')--('Assmann &herta ')--('Assmann & heinz')--('Assmann &  ilse ')--('Arnim &marie-luise&  51373')--('Arnim & Dietlof&  51373')--('Arnim &  76228 ')--('Arndt &  Volkmar & werner')--('Arndt &  Volkmar &!werner ')
    --('Schneider &  19406 ')--('Arndt &  16766')--('Arndt &  14913 ')--('Aust &  14823 ')--('Arndt &  4916 ')--('Arndt &  16278 ')--('Arndt &  15936 ')--('Arndt & Theisa')--('Arndt &  16909')--('Arlt & Bautzen ')--('Appetz &  16278 ')--('Antrick &  17268 ')--('Axenbeck &  84546')
    --('Baatz &  16845 ')--('Baatz &  16259')--('Antrup &  49525')--('Antonius &  14943 ')--('Andres & Jüterbog')--('Amsel & Senftenberg')--('Amsel &  15848')--('Ameling &  48712') --('Baresel &  16792 ')--('Balzer &  64673')--('Baier & 99444')--('Bahro &  roland')--('Bähr-Jurack & Guben ')
    --('Bähr &   Schenkendöbern ')--('Bähr &  guben &!5')--('Bahns & 26197')--('Bahns &  65385 ')--('Bahns &  15518 ')--('Bagola & manfred')--('Bagola & erika')--('Bader &  15837 &luckenwalder')--('Bader &  15837 & kiefernweg')--('Backhaus & Stolzenau')--('Bach & dresden') --('Bach & Stuttgart') 
    --('Badke & roswitha')--'Badke & hartmuth')--('Ambos & Frankfurt')
    --('Andres & katrin')--('Andres & michael')--('Alvensleben &15518')--('Ambrosius &Neiße-Malxetal')--('Altrichter & Schwarzbach')--('Altrichter & Wernigerode')--('Altmann & Luckau/Uckro')--('Alandt & Beelitz')--('AGW')--('AGT')--('Agro-Glien')
    --('Altenkirch & planebruch')--('Altenburg & schipkau')--('Allgaier & blaubeuren')--('Albrecht & Rietzneuendorf-Staakow')--('Albrecht & neustadt | albrecht &neustadt/dosse')--('Albertziok & planetal')--('Albath & mühlenberge')--('Albath & Martina')--('Albath & markus')
    --('Aland & jutta')--('Aland & carmen')--('Aland &Anett ')--('Aland & Hans & Dieter')--('Airport')--('Ahrens & cloppenburg')--('Ahrens &Lastrup')--('Ludowig &brodau')--('AGRO & senftenberg')--('Albrecht & Kolkwitz')--('Agrofarm & Sonnewalde')--('Agraria & Schwarzenfeld')--('AGRARGESELLSCHAFT & UCKERLAND')--('Agrar-GmbH & Grieben')
    --('Agrargesellschaft & Milsa')--('Agrargesellschaft & Sperenberg')--('Saldern & Berlin')--('Agrargenossenschaft & Züllsdorf')--('Agrarproduktivgenossenschaft & Viesecke')--('Agrargenossenschaft & Trebbin')--('Agrargenossenschaft & Thomas')--('Agrargenossenschaft & Stölln')
    --('Agrargenossenschaft &  unterspreewald')--('Agrargenossenschaft & radensdorf')--('Airport & Kopenhavn')--('Agrargenossenschaft &Schlaubetal')--('Agrargenossenschaft & Havelaue')--('Albrecht & elona')--('Rex & frankfurt')--('Zuydtwyck & lanz')--('Wittkowski & kantow')
    --('Wishöth &kremmen ')--('Wiesejahn & spremberg')--('Waltasaari & berlin')--('Voß & Merzhausen/Breisgau')--('Ludowig & bordau')--('Voigt & halbe')--('Vettin &gumtow')--('Venzke & rüdnitz')--('Uckro & hanns-detlef')--('Näthe & rüdnitz')--('Müller-Brause & rostock')
    --('Müller & ebstorf')--('Muhr & attendorn & gbr')--('Muhr & attendorn & bettina')--('Muhr & attendorn &thomas')--('Meyer-Johann & hermann')--('Meyer-Johann & !hermann')--('tätzel')--('Tätzel & dieter')--('Summerer &andernach')--('Stock & berlin')--('Stiftung & Bier')
    --('Stengel &stadtlohn')--('Stegemann & Kerstenbruch')--('Stanzeit & brück')--('Skalda & wandlitz')--('Sieber & Dresden')--('Schulz & gerdshagen')--('Scherer & Falkensee')--('Petschick & Golßen/Zützen')--'Pabst & Ingelsheim')--('Ostman & osnabrück')--('Mattes & Neu-Ulm')--('Mariak & laatzen')--('Maraun & rathenow')--('Lucke & Ingolstadt')
    --Lippe & Detmold')--('Liljenqvist & Lich')--('Lichtenberg & Barßel')--('Lausitz & Energie & Bergbau')--('Lange& Pankow')--('Sader & Storkow')--('Rosin & Brigitte')--('Nicol & Max')--('Materock & Elke') --('Lorenz & Heinz')--('Lehmann & Otto')--('Lazarus & Neuhausen/Spree')
    --'Pey & Breese')--('Lau & Kaufering')--('Lange & Elsterwerda')--('Lange & cottbus')--('Lämmermann &rheinsberg')--('Lamm & Rehfelde')--('Lahn & wollin')--('Kühnlein & Dittenheim')--('Krüger & Siehdichum')--('Kraatz & havelsee')--('Kösters & Rheine')--('Knuppe & lebusa')
    --('Schäff & Potsdam')--('Schack-Hartmann')--('Rosin & Rita')--('Agrar & Rheinsberg')--('Falkenstein & Obersulm')--('Bösing & vreden')--('BLUMAG')--'Behnke & potsdam')--('BBRE')--('Reschke & friedland')--('Remy & Gabriele')--('Rakow & Rathenow')
    --('Queling &Belzig')--('Prinz & Recklinghausen')--('Wilke & Dieter')--('Pflaumer &Arnsberg')--('Agrargenossenschaft & Gräfendorf')--('Agrargenossenschaft & Giesensdorf')--('Agrargenossenschaft & Neuzelle')--('KR & Fläming | Stiftung & heilig')--('Kenzler & Heiligengrabe')--('Kapitzke &Grünefeld')--('Kalz & Berlin')
    --('Roßmann & Gräben')--('Hummel & Ziesar')--('Hoffmann & Berlin')--('Hilmer & Antonia & Torgau')--('Hohmann & Rene') --('Henkel & Wilfried')--('Helm & holger ') --('Helm & gerald ')--('Harlinghausen & Tecklenburg')--('Hantelmann & Jutta')--'Hanel & Günther')--('Habel & Jennifer')--('Habel & Nicole')--('Haase & Peter') 
    --('Grabs & Karl |Schulze & Ferdinand| Külper& Gutthilf| Weill & Siegmund |Daßmann & Bertha | Grabs & Martha | Alisch & Emil |Steinicke & Ernst | Schulze & Herbert ') --('Schulze & Gehren') --('Schulz & schönwald')--('Grabs & Karl') --('Richtberg & Wenzenbach')--('Schumann & Marie') --('Gebhardt & Ventschow')--('Gaude & Templin')--('Gärtner & Frauendorf')
    --('Fuchs & Kolkwitz')--('Richtberg & chambesy')--('Friedrich & planetal')--('Frankowiak & Marie')--('Förster & Heidesee')--('Schumann & liane')--('Fölster & Gerd & gesellschaft')--('Fölster & Gerd & !gesellschaft')--('Fölster & Gabriele & !gesellschaft')--('Fischbeck')
    --('Gerstädt')--('Erdmann & Jörg')--('Ehrhardt & Antje')--('Doerks')--('pfarre &altbelgern')--('stiftung & sielmann')--('Derwis')--('Riewe')--('Ribbeck & nikolaus')--('Ribbecke & Horst')--('Buttgereit')--('Drenske')--('Wendt & Carl')
    --('stiftung & sielmann')--('gemeinde & saarow')--('gemeinde & bollensdorf')--('gemeinde & neuhausen/spree')--('gemeind & fünfeichen | gemeinde & schlaubetal')--('gemeind & hohenlobbese')--('gemeind & drahnsdorf')--('gemeind & protzel')--('gemeind & breitenau')--('gemeind & kieselwitz')
    --d.document_tokens @@to_tsquery('gemeind & drebkau')
    --d.document_tokens @@to_tsquery('gemeind &jerischk')
    --d.document_tokens @@to_tsquery('Bruchmüller& Erika ')
    --d.document_tokens @@to_tsquery('Bradatsch')
    --d.document_tokens @@to_tsquery('Borchers')
    --d.document_tokens @@to_tsquery('Bredow & friedrichsthal')
    --d.document_tokens @@to_tsquery('Berner & hepp')
    --d.document_tokens @@to_tsquery('Bergmann & Stefan')
    --d.document_tokens @@to_tsquery('Bergmann & Eckhard')
    --d.document_tokens @@to_tsquery('Bergholz & ot')
    --d.document_tokens @@to_tsquery('Bausamer')
    --d.document_tokens @@to_tsquery('Schmidt & Milly')--Bauch & Otto')-- | Schmidt & Milly')
    --d.document_tokens @@to_tsquery('Beuster & Guido')
    --d.document_tokens @@to_tsquery('Bleike & Werner')
    --d.document_tokens @@to_tsquery('Bahl')
    --d.document_tokens @@to_tsquery('Aschenborn & Minna')
   --d.document_tokens @@to_tsquery('bergbau-verwaltungsgesellschaf') 
   -- to_tsvector('german', d.eig_schr_gb) @@ to_tsquery('german', 'evangel & eberswalde & st.marien')
    )