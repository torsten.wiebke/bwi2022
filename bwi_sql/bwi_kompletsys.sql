/* sql um den Export des Komplettsystems 2023 in bwiti
 * 
 */
-- delete in tables vor import
delete from bwiti.b3v_ecke_feld 

--sicherheitskopie
create table bwiti.b3v_wzp_202212 as
select * from bwiti.b3v_wzp bvw 

delete from bwiti.b3v_wzp 

delete from bwiti.b3v

/* Ziel:
 * import der Tabellen die für die Analyse der Baumartenhäufigkeiten an den Trakten wichtig sind
 * b3v_ecke_feld - Zusatz- und Beschreibungsdaten für Bäume die für die Bestandesbeschreibung in WZP2 oder WZP1 erfasst wurden
 * b3v_gt4,_ba - Bäume die für die Bestandesbeschreibung in WZP2 oder WZP1 erfasst wurden
 * b3v_le4m_ba 
 * b3v_objekte
 * b3v_wzp
 * b3v_gps
 */

select bvw.tnr, bvw.enr, bvw.ba, count(bvw.ba) as baum_n-- over (partition by bvw.ba,bvw.tnr,bvw.enr) 
,xba.kurzd, bvgmb.ba ebs_ba,  bvgmb.anz ebs_anz
from
bwiti.b3v_wzp bvw
--join bwiti.x3_ba xba on bvw.ba=xba.icode
join bwiti.b3v_gt4m_ba bvgmb on bvgmb.tnr =bvw.tnr and bvgmb.enr = bvw.enr 
join bwiti.x3_ba xba on bvw.ba=xba.icode and bvgmb.ba=xba.icode
--join bwiti.x3_ba xba on bvgmb.ba=xba.icode 
group by bvw.tnr, bvw.enr, bvw.ba,xba.kurzd, bvgmb.ba,bvgmb.anz
order by bvw.tnr, bvw.enr, bvw.ba,bvgmb.ba
