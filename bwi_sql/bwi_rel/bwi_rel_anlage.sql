-- CONNECTION: url=jdbc:postgresql://localhost:5432/projekt_lfb
-- New script in projekt_lfb.
-- Date: 17.01.2025
-- Time: 23:57:31
-- Database: projekt_lfb            - target database name

-- Aufbau relationales Schema bwi_r

create schema bwi_r

/* 1. Grundtabelle Trakt erstellen aus 
 * - Tnr, Enr, Netz */

-- einlesen der vorhandenen Daten

-- bv_tnr ?
select 
--count(*)  --15002
*
from bwi_2022.bv_tnr bt 

select 
--count(*)  --7668
--*
distinct soll_rechtst 
from bwi_2022.b3_tnr bt 

-- Abfragekonstrukiton für b3_tnr
SELECT 
	tnr,
    aufnbl, 
    standardbl, 
    TRIM(BOTH ' # ' FROM laender) AS laender, 
    netz, 
    netz64, 
    ktg,
    CASE 
        WHEN soll_rechtst::text LIKE '4%' THEN 
            ST_Transform(ST_SetSRID(ST_MakePoint(soll_rechtst, soll_hocht), 31468), 4326)
        WHEN soll_rechtst::text LIKE '5%' THEN 
            ST_Transform(ST_SetSRID(ST_MakePoint(soll_rechtst, soll_hocht), 31469), 4326)
    END AS soll_geom
FROM 
    bwi_2022.b3_tnr bt;

-- Tabellenspeicherung für b3_tnr
drop table bwi_r.b3_tnr_geom

create table bwi_r.b3_tnr_geom as
SELECT 
    tnr,
    aufnbl, 
    standardbl, 
    TRIM(BOTH ' # ' FROM laender) AS laender, 
    netz, 
    netz64, 
    ktg
FROM 
    bwi_2022.b3_tnr bt;
   
 
-- Primärschlüssel hinzufügen
ALTER TABLE bwi_r.b3_tnr_geom
ADD CONSTRAINT b3_tnr_geom_pkey PRIMARY KEY (tnr);

-- Geometriespalte registrieren
SELECT AddGeometryColumn('bwi_r', 'b3_tnr_geom', 'soll_geom', 4326, 'POINT', 2);

-- Geometriespalte mit Daten füllen
UPDATE bwi_r.b3_tnr_geom AS target
SET soll_geom = CASE 
    WHEN source.soll_rechtst::text LIKE '4%' THEN 
        ST_Transform(ST_SetSRID(ST_MakePoint(source.soll_rechtst, source.soll_hocht), 31468), 4326)
    WHEN source.soll_rechtst::text LIKE '5%' THEN 
        ST_Transform(ST_SetSRID(ST_MakePoint(source.soll_rechtst, source.soll_hocht), 31469), 4326)
END
FROM bwi_2022.b3_tnr AS source
WHERE target.tnr = source.tnr;

-- Räumlichen Index erstellen
CREATE INDEX b3_tnr_geom_soll_geom_idx
ON bwi_r.b3_tnr_geom
USING GIST (soll_geom);

/* 2. Grundtabelle Ecke erstellen aus 
 * - tnr,    enr,    wg,    wb,    hoenn */
-- Tabellenspeicherung für b3_ecke
drop table bwi_r.b3_ecke_geom

create table bwi_r.b3_ecke_geom as
SELECT --*
    tnr,
    enr,
    wg,
    wb,
    hoenn
--    aufnbl, 
--    standardbl, 
--    TRIM(BOTH ' # ' FROM laender) AS laender, 
--    netz, 
--    netz64, 
--    ktg
FROM 
    bwi_2022.b3_ecke be ;
   
 
-- Primärschlüssel hinzufügen
ALTER TABLE bwi_r.b3_ecke_geom
ADD CONSTRAINT b3_tnr_ecke_pkey PRIMARY KEY (tnr, enr);

-- Geometriespalte registrieren
SELECT AddGeometryColumn('bwi_r', 'b3_ecke_geom', 'soll_geom', 4326, 'POINT', 2);

-- Geometriespalte mit Daten füllen
UPDATE bwi_r.b3_ecke_geom AS target
SET soll_geom = ST_SetSRID(
    ST_MakePoint(
        REPLACE(source.soll_x_wgs84, ',', '.')::numeric,
        REPLACE(source.soll_y_wgs84, ',', '.')::numeric
    ), 
    4326
)
FROM bwi_2022.b3_ecke AS source
WHERE target.tnr = source.tnr AND target.enr = source.enr;


-- Räumlichen Index erstellen
CREATE INDEX b3_ecke_geom_soll_geom_idx
ON bwi_r.b3_ecke_geom
USING GIST (soll_geom);

/* 3. Grundtabelle bwi_2022_Ist-Ecke erstellen aus 
 * - Tnr, Enr, Netz */

select *
from bwi_2022.b3v_gps bvg 

drop table bwi_r.bwi2022_ecke_istgeom;

create table bwi_r.bwi2022_ecke_istgeom as 
select 
tnr, enr
from bwi_2022.b3v_gps bvg 

-- Primärschlüssel hinzufügen
ALTER TABLE bwi_r.bwi2022_ecke_istgeom
ADD CONSTRAINT bwi2022_ecke_istgeom_pkey PRIMARY KEY (tnr, enr);

-- Geometriespalte registrieren
SELECT AddGeometryColumn('bwi_r', 'bwi2022_ecke_istgeom', 'ist_geom', 4326, 'POINT', 2);

-- Geometriespalte mit Daten füllen
UPDATE bwi_r.bwi2022_ecke_istgeom AS target
SET ist_geom = CASE
    WHEN source.lat_med IS NOT NULL AND source.lon_med IS NOT NULL AND 
         REPLACE(source.lat_med, ',', '.') ~ '^[0-9]+(\.[0-9]+)?$' AND 
         REPLACE(source.lon_med, ',', '.') ~ '^[0-9]+(\.[0-9]+)?$'
    THEN ST_SetSRID(
        ST_MakePoint(
            REPLACE(source.lon_med, ',', '.')::numeric,
            REPLACE(source.lat_med, ',', '.')::numeric
        ), 
        4326
    )
    ELSE NULL
END
FROM bwi_2022.b3v_gps AS source
WHERE target.tnr = source.tnr AND target.enr = source.enr;

-- Räumlichen Index erstellen
CREATE INDEX bwi2022_ecke_istgeom_idx
ON bwi_r.bwi2022_ecke_istgeom
USING GIST (ist_geom);

-- Spalte für ist_geom updaten
update bwi_r.bwi2022_ecke_istgeom 
set geomquelle_id = 1
where ist_geom IS NOT NULL AND NOT ST_IsEmpty(ist_geom); -- Eine NULL-Geometrie bedeutet, dass gar kein Wert vorhanden ist. Eine leere Geometrie (ST_IsEmpty) ist eine Geometrie, die existiert, aber keine tatsächlichen räumlichen Daten enthält (z. B. ein leerer Punkt

-- fehlende Geometrien aus bwi_2022_bv_gps = Vorgänger GPS
-- fehlende tnr, enr und geometrien aus  bwi_2022_bv_gps = Vorgänger GPS wenn nicht vorhanden
INSERT INTO bwi_r.bwi2022_ecke_istgeom (tnr, enr, ist_geom, geomquelle_id)
SELECT 
    source.tnr,
    source.enr,
    CASE
        WHEN source.lat_med IS NOT NULL AND source.lon_med IS NOT NULL AND 
             REPLACE(source.lat_med, ',', '.') ~ '^[0-9]+(\.[0-9]+)?$' AND 
             REPLACE(source.lon_med, ',', '.') ~ '^[0-9]+(\.[0-9]+)?$'
        THEN ST_SetSRID(
            ST_MakePoint(
                REPLACE(source.lon_med, ',', '.')::numeric,
                REPLACE(source.lat_med, ',', '.')::numeric
            ), 
            4326
        )
        ELSE NULL
    END AS ist_geom,
    2 AS geomquelle_id
FROM 
    bwi_2022.bv_gps AS source
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM bwi_r.bwi2022_ecke_istgeom AS target
        WHERE target.tnr = source.tnr AND target.enr = source.enr
    );


-- update geometry aus bwi_2022_bv_gps = Vorgänger GPS
UPDATE bwi_r.bwi2022_ecke_istgeom AS target
SET 
    ist_geom = CASE
        WHEN source.lat_med IS NOT NULL AND source.lon_med IS NOT NULL AND 
             REPLACE(source.lat_med, ',', '.') ~ '^[0-9]+(\.[0-9]+)?$' AND 
             REPLACE(source.lon_med, ',', '.') ~ '^[0-9]+(\.[0-9]+)?$'
        THEN ST_SetSRID(
            ST_MakePoint(
                REPLACE(source.lon_med, ',', '.')::numeric,
                REPLACE(source.lat_med, ',', '.')::numeric
            ), 
            4326
        )
        ELSE NULL
    END,
    geomquelle_id = 2 -- Angabe, dass die Quelle bv_gps ist
FROM bwi_2022.bv_gps AS source
WHERE 
    (ist_geom IS NULL OR ST_IsEmpty(ist_geom)) -- Nur wenn keine Geometrie vorhanden ist
    AND target.tnr = source.tnr
    AND target.enr = source.enr;
 
-- Geometrie aus sollgeom
UPDATE bwi_r.bwi2022_ecke_istgeom AS target
SET 
    ist_geom = source.soll_geom, 
    geomquelle_id = 3
from bwi_r.b3_ecke_geom as source
WHERE 
    (target.ist_geom IS NULL OR ST_IsEmpty(target.ist_geom)) -- Nur wenn keine Geometrie vorhanden ist
    AND target.tnr = source.tnr
    AND target.enr = source.enr;

-- update wa_22 and begehbar_22
UPDATE bwi_r.bwi2022_ecke_istgeom AS target
set 
wa_2022 = source.wa, 
begehbar_2022 = source.begehbar 
from bwi_2022.b3f_ecke_vorkl as source
where source.tnr = target.tnr 
and source.enr = target.enr 

-- fehlende Punkte in bwi2022_ecke_istgeom?
select 
count(*)
from bwi_r.bwi2022_ecke_istgeom istg
left join bwi_2022.b3f_ecke_vorkl bfev on bfev.tnr = istg.tnr and bfev.enr = istg.enr
where bfev.wa between 1 and 5 and istg.ist_geom is null

-- Amtlichen Regionalschlüssel zuordnen
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD bundesland int NULL;
COMMENT ON COLUMN bwi_r.bwi2022_ecke_istgeom.bundesland IS 'Kennzahl des Landes nach amtlichen Regionalschlüssel (ARS)';
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD regierungsbezirk int NULL;
COMMENT ON COLUMN bwi_r.bwi2022_ecke_istgeom.regierungsbezirk IS 'Kennzahl des Regierungsbezirkes nach amtlichen Regionalschlüssel (ARS)';
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD landkreis int NULL;
COMMENT ON COLUMN bwi_r.bwi2022_ecke_istgeom.landkreis IS 'Kennzahl des Landkreises nach amtlichen Regionalschlüssel (ARS)';
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD verwaltungsgemeinschaft1 int NULL;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD verwaltungsgemeinschaft2 int NULL;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD gemeinde int NULL;
-- führende Nullen und im Original auch als varchar
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ALTER COLUMN bundesland TYPE varchar USING bundesland::varchar;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ALTER COLUMN regierungsbezirk TYPE varchar USING regierungsbezirk::varchar;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ALTER COLUMN landkreis TYPE varchar USING landkreis::varchar;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ALTER COLUMN verwaltungsgemeinschaft1 TYPE varchar USING verwaltungsgemeinschaft1::varchar;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ALTER COLUMN verwaltungsgemeinschaft2 TYPE varchar USING verwaltungsgemeinschaft2::varchar;
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ALTER COLUMN gemeinde TYPE varchar USING gemeinde::varchar;

-- update der Datenfelder nach vg_250_ew
-- test
SELECT concat(target.tnr, target.enr) , source.land, source.regierungsbezirk, source.kreis, 
       source.verwaltungsgemeinschaftteil1, source.verwaltungsgemeinschaftteil2,source.gemeinde,
       concat(source.land, source.regierungsbezirk, source.kreis, 
       source.verwaltungsgemeinschaftteil1, source.verwaltungsgemeinschaftteil2,source.gemeinde) as ars
FROM bwi_r.bwi2022_ecke_istgeom AS target
JOIN waldinv_geo.v_vg250_gem AS source
ON ST_Within(ST_Transform(target.ist_geom , 25832), source.geom)
where source.verwaltungsgemeinschaftteil2::integer=052;

-- update der geographischen position
update bwi_r.bwi2022_ecke_istgeom AS target
set 
bundesland = source.land::int,
regierungsbezirk = source.regierungsbezirk,
landkreis = source.kreis,
verwaltungsgemeinschaft1 = source.verwaltungsgemeinschaftteil1,
verwaltungsgemeinschaft2 = source.verwaltungsgemeinschaftteil2,
gemeinde = source.gemeinde  
from waldinv_geo.v_vg250_gem as source
where st_within(st_transform(target.ist_geom, 25832), source.geom)

-- bwi-Bundesland hinzufügen
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD bl_bwi varchar NULL;
comment on column bwi_r.bwi2022_ecke_istgeom.bl_bwi IS 'Bundesland für das die Aufnahme gilt?';
ALTER TABLE bwi_r.bwi2022_ecke_istgeom ADD aufnbl_bwi varchar NULL;
comment on column bwi_r.bwi2022_ecke_istgeom.aufnbl_bwi is 'Aufnehmendes Bundesland, 19 ist Brb';

-- update bwi-bl
update bwi_r.bwi2022_ecke_istgeom AS target
set 
bl_bwi = source.bl 
from bwi_2022.b3f_ecke_vorkl as source
where target.tnr = source.tnr and target.enr = source.enr 

-- update bwi-aufnbl
update bwi_r.bwi2022_ecke_istgeom AS target
set 
blb3ecke = source.bl, 
aufnbl_bwi = source.aufnbl  
from bwi_2022.b3_ecke  as source
where target.tnr = source.tnr and target.enr = source.enr 

select --tnr,enr 
count(*) -- 0 war wahrscheinlich nicht sinnvoll beide Datenfelder anzufügen
from bwi_r.bwi2022_ecke_istgeom bei 
where bei.bl_bwi <> bei.blb3ecke 

/* jung nach bwi_r
 *  
 */
-- b3v_ecke_feld bvef
create table bwi_r.jung_22 as
select bvef.tnr, bvef.enr,
bvef.b0_bs,
--xb.langd ,
bvef.b0_zaun, -- boolean
bvef.b0_move,
--xpmm.langd ,
bvef.b0_radius, -- Radius [m] des Probekreises für Bäume der Baumgröße 0 {1 oder 2 m}
bvef.b0_hori -- Horizontalentfernung [cm] der Probekreise r=1 / 2 m (Baumgrößen 0-6)
from bwi_2022.b3v_ecke_feld bvef
join bwi_meta.x3_p1m_move xpmm on xpmm.icode = bvef.b0_move
join bwi_meta.x3_bs xb on xb.icode = bvef.b0_bs 

COMMENT ON COLUMN bwi_r.jung_22.b0_zaun IS 'boolean';
COMMENT ON COLUMN bwi_r.jung_22.b0_radius IS 'Radius [m] des Probekreises für Bäume der Baumgröße 0 {1 oder 2 m}';
COMMENT ON COLUMN bwi_r.jung_22.b0_hori IS 'Horizontalentfernung [cm] der Probekreise r=1 / 2 m (Baumgrößen 0-6)';

--Werte aus b3v_objekte müssen übernommen werden
ALTER TABLE bwi_r.jung_22 ADD b0_vorh int NULL;
COMMENT ON COLUMN bwi_r.jung_22.b0_vorh IS 'Probebäume im Probekreis r=1 / 2m (Baumgröße 0) vorhanden? (0=nein, 1/leer=ja) aus b3v_objekte';
ALTER TABLE bwi_r.jung_22 ADD b1_6_vorh int NULL;
COMMENT ON COLUMN bwi_r.jung_22.b1_6_vorh IS 'Probebäume im Probekreis r=2 m (Baumgröße 1-6) vorhanden? (0=nein, 1/leer=ja) aus b3v_objekte';

-- update aus b3v_objekte
update bwi_r.jung_22 as target
set 
b0_vorh = source.b0,
b1_6_vorh = source.b1_6 
from bwi_2022.b3v_objekte as source
where source.tnr=target.tnr and source.enr = target.enr 

create table bwi_r.jungpfl_22 (
id_jungpfl_22 int4 GENERATED BY DEFAULT AS IDENTITY NOT NULL,	
tnr int,
enr int,
nr int,
bz int,
ba int,
gr int,
biss int,
schael int,
schu int,
anz int,
CONSTRAINT jungpfl22_pk PRIMARY KEY (id_jungpfl_22)
);


-- b3v_b1_6 bvb insert
insert into bwi_r.jungpfl_22 (tnr,enr,nr,bz,ba,gr,biss,schael,schu,anz)
select
tnr,
enr,
nr,
bz,
ba,
gr,
biss,
schael,
schu,
anz
from bwi_2022.b3v_b1_6

-- b3v_b0
insert into bwi_r.jungpfl_22 (tnr,enr,nr,ba,gr,biss,schu,anz)
select
tnr,
enr,
nr,
ba,
0 as gr,
biss,
schu,
anz
from bwi_2022.b3v_b0


select *
from bwi_2022.b3v_ecke_feld bvef 

select 
tnr, enr, wa 
from 
bwi_2022.b3f_ecke_vorkl bfev 
where bfev.wa between 1 and 5 -- Wald, siehe bwi_meta.x3_wa

-- bwi-netz
select *
from bwi_r.b3_tnr_geom btg 

-- geometrie colums
SELECT f_table_schema, f_table_name, f_geometry_column, srid, type 
FROM geometry_columns 
WHERE f_table_name = 'waldinv_geo.b3_ecke_soll';

ALTER TABLE waldinv_geo.b3_ecke_soll
ALTER COLUMN geom 
SET DATA TYPE geometry(Geometry, 25833) USING ST_SetSRID(geom, 25833);

