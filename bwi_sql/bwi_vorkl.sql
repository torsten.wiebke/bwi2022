-- CONNECTION: name=kubupostgis
-- vor import der neuen Tabellen
delete from bwi_vorkl.b3f_ecke_vorkl 

delete from bwi_vorkl.b3f_ecke_wfkt_quer 

delete from bwi_vorkl.b3f_tnr_vorkl 

delete from bwi_vorkl.b3f_tnr_work 

/* b3f_ecke_vorkl 
 * 
 */



select bfev.tnr ,bfev.enr, bfev.wa,  bfev.gemeinde,  bfev.wg, bfev.neursachenb, bftv.trstatus
from 
bwi_vorkl.b3f_ecke_vorkl bfev 
left join bwi_vorkl.b3f_tnr_vorkl bftv on bfev.tnr=bftv.tnr 
--left join bwiti.b3v_ecke_feld bvef  be on bfev.tnr=be.tnr
where
bftv.trstatus =3 and 
bfev.wa=0

select *--bfev.*
from 
bwi_vorkl.b3f_ecke_vorkl bfev 
join bwi_vorkl.b3f_tnr_vorkl bftv on bfev.tnr=bftv.tnr 
where 
bfev.tnr=46605

select bftv.trstatus
,bfev.ne --Nutzungseinschränkung
,bfev.neursachenb --außerbetriebliche Ursache der Nutzungseinschränkung
,bfev.neursacheb -- innerbetriebliche Ursache der Nutzungseinschränkung
,bfev.nenschutz --Ursache der Nutzungseinschränkung 1-Naturschutz (1=ja, 0=nein)
,bfev.neswald --Ursache der Nutzungseinschränkung 2-Schutzwald (1=ja, 0=nein)
,bfev.neewald --Ursache der Nutzungseinschränkung 3-Erholungswald (1=ja, 0=nein)
,bfev.nesabursach --Ursache der Nutzungseinschränkung 9-sonstige außerbetriebliche Ursachen (1=ja, 0=nein)
,bfev.nesplitter --Ursache der Nutzungseinschränkung 11-Splitterbesitz mit unwirtschaftlicher Größe (1=ja, 0=nein)
,bfev.nestreu --Ursache der Nutzungseinschränkung 12-Streulage (1=ja, 0=nein)
,bfev.neunerschlies --Ursache der Nutzungseinschränkung 13-unzureichender Erschließung (1=ja, 0=nein)
,bfev.negeleig --Ursache der Nutzungseinschränkung 14-Geländeeigenschaften, Nassstandort (1=ja, 0=nein)
,bfev.negerertrag --Ursache der Nutzungseinschränkung 15-geringer Ertragserwartungen (dGZ < 1 m³/(ha*a)) (1=ja, 0=nein)
,bfev.neeigenbin --Ursache der Nutzungseinschränkung 16-Schutzflächen in Eigenbindung (z.B. Naturreservate) (1=ja, 0=nein)
,bfev.nesibursach --Ursache der Nutzungseinschränkung 19-sonstige innerbetriebliche Ursachen (1=ja, 0=nein)
from 
bwi_vorkl.b3f_ecke_vorkl bfev 
join bwi_vorkl.b3f_tnr_vorkl bftv on bfev.tnr=bftv.tnr 
where 
bftv.trstatus between 4 and 5

select bftv.trstatus,bfev.wa
,bfev.ne --Nutzungseinschränkung
,bfev.neursachenb --außerbetriebliche Ursache der Nutzungseinschränkung
,bfev.neursacheb -- innerbetriebliche Ursache der Nutzungseinschränkung
,bfev.nenschutz --Ursache der Nutzungseinschränkung 1-Naturschutz (1=ja, 0=nein)
,bfev.neswald --Ursache der Nutzungseinschränkung 2-Schutzwald (1=ja, 0=nein)
,bfev.neewald --Ursache der Nutzungseinschränkung 3-Erholungswald (1=ja, 0=nein)
,bfev.nesabursach --Ursache der Nutzungseinschränkung 9-sonstige außerbetriebliche Ursachen (1=ja, 0=nein)
,bfev.nesplitter --Ursache der Nutzungseinschränkung 11-Splitterbesitz mit unwirtschaftlicher Größe (1=ja, 0=nein)
,bfev.nestreu --Ursache der Nutzungseinschränkung 12-Streulage (1=ja, 0=nein)
,bfev.neunerschlies --Ursache der Nutzungseinschränkung 13-unzureichender Erschließung (1=ja, 0=nein)
,bfev.negeleig --Ursache der Nutzungseinschränkung 14-Geländeeigenschaften, Nassstandort (1=ja, 0=nein)
,bfev.negerertrag --Ursache der Nutzungseinschränkung 15-geringer Ertragserwartungen (dGZ < 1 m³/(ha*a)) (1=ja, 0=nein)
,bfev.neeigenbin --Ursache der Nutzungseinschränkung 16-Schutzflächen in Eigenbindung (z.B. Naturreservate) (1=ja, 0=nein)
,bfev.nesibursach --Ursache der Nutzungseinschränkung 19-sonstige innerbetriebliche Ursachen (1=ja, 0=nein)
from 
bwi_vorkl.b3f_ecke_vorkl bfev 
join bwi_vorkl.b3f_tnr_vorkl bftv on bfev.tnr=bftv.tnr 
where 
bfev.tnr =37294
--bftv.trstatus between 3 and 5
--and bfev.wa=1

/* update Nutzungseinschränkungen
 * 
 */
-- Nutzungseinschränkungen und Eigentum,auf -1 in Nichtwaldecken
update bwi_vorkl.b3f_ecke_vorkl bfev 
set 
ne =-1
,neursachenb  =-1
,neursacheb  =-1
,nenschutz  =-1
,neswald  =-1
,neewald  =-1
,nesabursach  =-1
,nesplitter  =-1
,nestreu  =-1
,neunerschlies  =-1
,negeleig  =-1
,negerertrag  =-1
,neeigenbin  =-1
,nesibursach  =-1
,eg = -1
,eggrkl =-1
from 
bwi_vorkl.b3f_tnr_vorkl bftv 
where bfev.tnr=bftv.tnr
and bftv.trstatus between 3 and 5
-- bei Export auf Spaltentrennzeichen ";" Value display format "databas native" und encoding "windows-1250" achten. Export über faircloud/bwi/vorklärung - in zip ziehen und in Software importieren, Importvorgang benötigt ca. 4.5 h

select * 
from bwi_vorkl.b3f_ecke_vorkl bfev 
join bwi_vorkl.b3f_tnr_vorkl bftv on bftv.tnr = bfev.tnr 
where bftv.trstatus =4

select *
from 
bwi_vorkl.b3f_ecke_vorkl bfev
where 
wa between 1 and 4 --x3_wa

-- Waldlebensraum vorklärung auf 1=nicht eindeutig und 60= keine Kartierung wo Wald
update bwi_vorkl.b3f_ecke_vorkl bfev 
set
wlt_v =1
,wlt_wiev = 60
--from 
--bwi_vorkl.b3f_tnr_vorkl bftv 
where 
wa between 1 and 4 --x3_wa

-- akte wird als integer erwartet
select distinct bftv.akte 
from 
bwi_vorkl.b3f_tnr_vorkl bftv 
--where bftv.akte is not null

update bwi_vorkl.b3f_tnr_vorkl
set akte = null 

/*update Wildvorkommen 
 * Muffelwild und Gamswild haben wir in ganz Brandenburg nicht
 */
update bwi_vorkl.b3f_tnr_vorkl
set
muf_wild  =-1
,gams_wild  =-1;

-- update Wildvorkommen für Nichtwaldtrakte
update bwi_vorkl.b3f_tnr_vorkl
set
schw_wild  =-1
,rot_wild  =-1
,dam_wild  =-1
,reh_wild = -1
,muf_wild  =-1
,gams_wild  =-1
where 
trstatus between 3 and 5

-- Wildvokommen Schwarz- und Rehwild haben wir im Brandenburger Wald immer
update bwi_vorkl.b3f_tnr_vorkl
set
schw_wild  = 1
,reh_wild = 1
,muf_wild  =-1
,gams_wild  =-1
where 
trstatus between 1 and 2

select *
from 
bwi_vorkl.b3f_tnr_vorkl vtk
join bwi_vorkl.b3f_ecke_vorkl bfev on vtk.tnr = bfev.tnr 
where vtk.tnr = 34513

--update natwg von pnv auf bwi.b3_ecke_202212 bev
with natw as 
(select bev.tnr, bev.enr,bev.natwgv bevnatwgv, 
pp.bwi_natwgv
from bwi.b3_ecke_202212  bev
, lfe.sde_bfn_pnv_20230105 sbp join bwi.pnv_pom pp   on sbp.code_brdbg=pp.karteinh  
where
bev.natwgv is null 
--and bev.wa = 3
and st_dwithin(bev.geom,sbp.geom,0)
)
update bwi.b3_ecke_202212 bev
set natwgv = natw.bwi_natwgv
from natw
where 
--natwgv = -1 and 
bev.tnr=natw.tnr and 
bev.enr =natw.enr

--nathöhe
select distinct  bev.nathoe, count(*) over() 
from bwi_b_vorkl.b3f_ecke_vorkl bev 

update bwi_b_vorkl.b3f_ecke_vorkl
set nathoe = 1
where nathoe =-1


--natürliche Höhenstufe und Standortsklimafeuchtestufe
update bwi.b3_ecke_202212  bev
set 
nathoe  = 1
where nathoe =-1 or nathoe is null

update bwi.b3_ecke_202212 bev 
set st_ks_hs = 1 where st_ks_hs = - 1 or st_ks_hs is null 

update bwi.b3_ecke_202212 
set st_ks_fs = 7 where st_ks_fs = -1 or st_ks_fs is null 

select distinct st_ks_hs, st_ks_fs  
from bwi.b3_ecke_202212 

update bwi.b3_ecke_202212 be 
set
eg=eig.bwi_ea,
eggrkl =eig.eiggrkl
from bwi_vorkl.eigentum2022 eig
where eig.tnr =be.tnr and eig.enr = be.enr 

/* Nutzungseinschränkungen
| ursache                    | layer   | nutzung | nutzungscode | ne-ursache                   |
|----------------------------+---------+---------+--------------+------------------------------|
| exponierte Lage Schutzwald | wfk-bel | 2/3     | ne=4         | neeswald=1 und nesabursach=1 |
| trinkasserschutzgebiet     | lfu-wsg | 2/3     | ne=4         | neswald=1 und nesabursach=1 |
| trinkwasserschutzzone 1    | lfu_wsg | 0/3     | ne=2         | neswald=1 und nesabursach=1 |
| totalreservat              | lfu_trv | 0/3     | ne=2         | nesabursach=1 nenschutz=1    |
| naturwald                  | wfk_nwr | 0/3     | ne=2         | nesabursach=1 nenschutz=1    |
| nicht betretbar            | wfk_nbt | 0/3     | ne=2         | nesabursach=1                |
 */

select
    distinct be.wa
  --  *
from
    bwi.b3_ecke_202212 be;



select distinct ne,neursachenb,neursacheb, neswald, nesabursach, nenschutz  
from bwi.b3_ecke_202212 

select *
from bwi_vorkl.b3f_ecke_vorkl be 

select ne,neursachenb,neursacheb, neswald, nesabursach, nenschutz 
from bwi.b3_ecke_202212  bev 
where 
bev.bl!=11 
and bev.wa=3      -- Blöße
or bev.wa=5       -- bestockter Holzboden

--nutzungseinschrunken auf NULL
update bwi.b3_ecke_202212  bev 
set 
ne = null ,
neursachenb = null ,
neursacheb = null ,
nenschutz = null ,
neswald = null ,
nesabursach= null , 
nesplitter = null ,
nestreu = null ,
neunerschlies= null , 
negeleig = null ,
negerertrag= null , 
neeigenbin= null , 
nesibursach = null 

--naturschutzgebiete, Updated Rows  541
with nsg as (
select
 be.tnr, be.enr, --sln.flaechenty,sln.sg_name,  
 n.nutzungseinschr 
from
lfe.sde_lfu_nsg_20230105 sln 
join bwi.nsgwithtnr n on n.isn::varchar =sln.isn 
join bwi.b3_ecke_202212 be on st_contains(sln.geom,be.geom)
where n.nutzungseinschr > 0)
update bwi.b3_ecke_202212  bev 
set
ne=nsg.nutzungseinschr,
nenschutz=1
from nsg
where 
--bev.bl!=11        -- nicht Berlin
nsg.tnr=bev.tnr and nsg.enr=bev.enr 
and bev.wa=5      -- 3 Blöße, 5 bestockter Holzboden


--exponierte lage - Updated Rows  204
update bwi.b3_ecke_202212  bev 
set 
ne = 4,
neswald = 1,
nesabursach =1
from lfe.sde_wfk_bel_20230114 bel
where wa=5 and st_dwithin(bev.geom,bel.geom,0)

--trinkwasserschutzgebiete - Updated Rows   535
update bwi.b3_ecke_202212  bev 
set 
ne = 4,
neswald = 1,
nesabursach =1
from lfe.sde_lfu_wsg_20230105 ws
where wa=5 and st_dwithin(bev.geom,ws.geom,0)

--trikwasserschutzgebiete zone 1 - Updated Rows 0
select *
from lfe.sde_lfu_wsg_20230105 slw 
where slw.schutzzone = 'Zone I'

update bwi.b3_ecke_202212  bev 
set 
ne = 2,
neswald = 1,
nesabursach =1
from lfe.sde_lfu_wsg_20230105 ws
where wa=5 and st_dwithin(bev.geom,ws.geom,30) and ws.schutzzone = 'Zone I'

--totalreservat - Updated Rows 0
update bwi.b3_ecke_202212  bev 
set 
ne = 2,
neswald = 1,
nenschutz  =1
from lfe.sde_lfu_trv_20230128  ws
where wa=5 and st_dwithin(bev.geom,ws.geom,30)

--naturwald - Updated Rows 0
update bwi.b3_ecke_202212  bev 
set 
ne = 2,
neswald = 1,
nesabursach =1
from lfe.sde_lfu_trv_20230128  ws
where wa=5 and st_dwithin(bev.geom,ws.geom,30)

-- nicht betretbar
select 
*
from lfe.sde_wfk_nbt_20230104


--Updated Rows  223
update bwi.b3_ecke_202212 bev
set
ne = 2,
nesabursach = 1
from lfe.sde_wfk_nbt_20230104 nbt
where wa=5 and st_dwithin(bev.geom,nbt.geom,0)

--übertragung der Vorklärung
select be.bl ,be.tnr,be.enr, be.ne,be.neursachenb,be.neursacheb,be.nenschutz,be.neswald, be.neewald,be.nesplitter,be.nestreu,be.neunerschlies,be.negeleig,be.negerertrag,be.neeigenbin,be.nesibursach  
from bwi_vorkl.b3f_ecke_vorkl be 
where be.wa=5
and be.bl!=11 

--im Nichtwald -1; Updated Rows  3463, 20230404: Updated Rows   19120
update bwi_vorkl.b3f_ecke_vorkl be 
set
ne=-1,
neursachenb=-1,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=-1,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =-1,     --Ursache Naturschutz
neswald =-1,       --Ursache Schutzwald
neewald =-1,       --Ursache Erholungswald
nesplitter=-1,     --Ursache Splitterbesitz
nestreu =-1,       --Ursache Streulage
neunerschlies=-1,  --Ursache unzureichende Erschließung
negeleig=-1,       --Ursache Geländeeigenschaften
negerertrag =-1,   --Ursache geinge Ertragserwartung
neeigenbin =-1,    --Ursache Schutzfläche in Eigenbindung
nesibursach =-1   --sonstige innberbetriebliche Ursache
,nesabursach =-1  -- sonstiga außerbetriebliche Ursache 
where 
be.wa=0        --im Nichtwald -1; Updated Rows  3463

--im Nichtwald -1; Updated Rows  3463, 20230404: Updated Rows   19120
update bwi.b3_ecke_202212  be 
set
ne=-1,
neursachenb=-1,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=-1,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =-1,     --Ursache Naturschutz
neswald =-1,       --Ursache Schutzwald
neewald =-1,       --Ursache Erholungswald
nesplitter=-1,     --Ursache Splitterbesitz
nestreu =-1,       --Ursache Streulage
neunerschlies=-1,  --Ursache unzureichende Erschließung
negeleig=-1,       --Ursache Geländeeigenschaften
negerertrag =-1,   --Ursache geinge Ertragserwartung
neeigenbin =-1,    --Ursache Schutzfläche in Eigenbindung
nesibursach =-1   --sonstige innberbetriebliche Ursache
,nesabursach =-1  -- sonstiga außerbetriebliche Ursache 
where 
be.wa=0        --im Nichtwald -1; Updated Rows  3463

--für Wald Nichtholzboden, Updated Rows 298
update bwi.b3_ecke_202212  be 
set
ne=-1,
neursachenb=-1,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=-1,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =-1,     --Ursache Naturschutz
neswald =-1,       --Ursache Schutzwald
neewald =-1,       --Ursache Erholungswald
nesplitter=-1,     --Ursache Splitterbesitz
nestreu =-1,       --Ursache Streulage
neunerschlies=-1,  --Ursache unzureichende Erschließung
negeleig=-1,       --Ursache Geländeeigenschaften
negerertrag =-1,   --Ursache geinge Ertragserwartung
neeigenbin =-1,    --Ursache Schutzfläche in Eigenbindung
nesibursach =-1   --sonstige innberbetriebliche Ursache
,nesabursach =-1  -- sonstiga außerbetriebliche Ursache 
where 
be.wa=3        --wald 4 nichtholzboden, Updated Rows  298, 3 blöße Updated Rows 52

--für Wald Nichtholzboden, Updated Rows 298
update bwi_vorkl.b3f_ecke_vorkl be 
set
ne=-1,
neursachenb=-1,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=-1,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =-1,     --Ursache Naturschutz
neswald =-1,       --Ursache Schutzwald
neewald =-1,       --Ursache Erholungswald
nesplitter=-1,     --Ursache Splitterbesitz
nestreu =-1,       --Ursache Streulage
neunerschlies=-1,  --Ursache unzureichende Erschließung
negeleig=-1,       --Ursache Geländeeigenschaften
negerertrag =-1,   --Ursache geinge Ertragserwartung
neeigenbin =-1,    --Ursache Schutzfläche in Eigenbindung
nesibursach =-1   --sonstige innberbetriebliche Ursache
,nesabursach =-1  -- sonstiga außerbetriebliche Ursache 
where 
be.wa=3        --wald 4 nichtholzboden, Updated Rows  298, 3 blöße Updated Rows 52

--Für wa=5 und 3 vorinitialisierung, Updated Rows   11182
update bwi.b3_ecke_202212  be 
set
ne=0,
neursachenb=0,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=0,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =0,     --Ursache Naturschutz
neswald =0,       --Ursache Schutzwald
neewald =0,       --Ursache Erholungswald
nesplitter=0,     --Ursache Splitterbesitz
--nestreu =-1,       --Ursache Streulage
--neunerschlies=-1,  --Ursache unzureichende Erschließung
--negeleig=-1,       --Ursache Geländeeigenschaften
negerertrag =0,   --Ursache geinge Ertragserwartung
neeigenbin =0,    --Ursache Schutzfläche in Eigenbindung
nesibursach =0   --sonstige innberbetriebliche Ursache
,nesabursach =0  -- sonstiga außerbetriebliche Ursache 
where 
--be.bl!=11 and         -- nicht Berlin
be.wa=5      -- 3 Blöße Updated Rows 52, 5 bestockter Holzboden  Updated Rows   10955

--Für wa=5 und 3 vorinitialisierung, Updated Rows   11182
update bwi_vorkl.b3f_ecke_vorkl be 
set
ne=0,
neursachenb=0,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=0,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =0,     --Ursache Naturschutz
neswald =0,       --Ursache Schutzwald
neewald =0,       --Ursache Erholungswald
nesplitter=0,     --Ursache Splitterbesitz
--nestreu =-1,       --Ursache Streulage
--neunerschlies=-1,  --Ursache unzureichende Erschließung
--negeleig=-1,       --Ursache Geländeeigenschaften
negerertrag =0,   --Ursache geinge Ertragserwartung
neeigenbin =0,    --Ursache Schutzfläche in Eigenbindung
nesibursach =0   --sonstige innberbetriebliche Ursache
,nesabursach =0  -- sonstiga außerbetriebliche Ursache 
where 
--be.bl!=11 and         -- nicht Berlin
be.wa=5      -- 3 Blöße Updated Rows 52, 5 bestockter Holzboden  Updated Rows   10955

--Für wa=5 und 3 übertragung, Updated Rows   11182
update bwi_vorkl.b3f_ecke_vorkl be 
set
ne=bev.ne,
neursachenb=bev.neursachenb ,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=bev.neursacheb ,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =bev.nenschutz,     --Ursache Naturschutz
neswald =bev.neswald        --Ursache Schutzwald
--neewald =0,       --Ursache Erholungswald
--nesplitter=0,     --Ursache Splitterbesitz
--nestreu =-1,       --Ursache Streulage
--neunerschlies=-1,  --Ursache unzureichende Erschließung
--negeleig=-1,       --Ursache Geländeeigenschaften
--negerertrag =0,   --Ursache geinge Ertragserwartung
--neeigenbin =0,    --Ursache Schutzfläche in Eigenbindung
--nesibursach =0   --sonstige innberbetriebliche Ursache
,nesabursach =bev.nesabursach  -- sonstiga außerbetriebliche Ursache
,eg=bev.eg 
,eggrkl =bev.eggrkl 
,natwgv =bev.natwgv 
,st_ks_hs =bev.st_ks_hs 
,st_ks_fs =bev.st_ks_fs 
from bwi.b3_ecke_202212 bev
where bev.tnr = be.tnr and bev.enr =be.enr  
--and be.bl!=11
and be.wa=5 -- 5 bestockter Holzboden Updated Rows  10955, 3 Blöße Updated Rows 52

--update natw 
update bwi_vorkl.b3f_ecke_vorkl be 
set
natwgv =bev.natwgv 
from bwi.b3_ecke_202212 bev
where be.tnr=bev.tnr and be.enr = bev.enr 
and be.wa=3 -- 5 bestockter Holzboden Updated Rows  11130, 3 Blöße Updated Rows 52

update bwi_vorkl.b3f_ecke_vorkl be 
set
ne=0,
neursachenb=0 ,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=0,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =0,     --Ursache Naturschutz
neswald =0        --Ursache Schutzwald
--neewald =0,       --Ursache Erholungswald
--nesplitter=0,     --Ursache Splitterbesitz
--nestreu =-1,       --Ursache Streulage
--neunerschlies=-1,  --Ursache unzureichende Erschließung
--negeleig=-1,       --Ursache Geländeeigenschaften
--negerertrag =0,   --Ursache geinge Ertragserwartung
--neeigenbin =0,    --Ursache Schutzfläche in Eigenbindung
--nesibursach =0   --sonstige innberbetriebliche Ursache
where 
ne is null 

update bwi_vorkl.b3f_ecke_vorkl be 
set
ne=null,
neursachenb=null ,    --außerbetriebliche Ursache der Nutzungseinschränkung
neursacheb=null,     --innerbetriebliche Ursache der Nutzungseinschränkung
nenschutz =null,     --Ursache Naturschutz
neswald =null        --Ursache Schutzwald
--neewald =0,       --Ursache Erholungswald
--nesplitter=0,     --Ursache Splitterbesitz
--nestreu =-1,       --Ursache Streulage
--neunerschlies=-1,  --Ursache unzureichende Erschließung
--negeleig=-1,       --Ursache Geländeeigenschaften
--negerertrag =0,   --Ursache geinge Ertragserwartung
--neeigenbin =0,    --Ursache Schutzfläche in Eigenbindung
--nesibursach =0   --sonstige innberbetriebliche Ursache
where 
wa=0 


select count(*), wa,ne,neursachenb,neursacheb,nenschutz,neswald  
from bwi_vorkl.b3f_ecke_vorkl be 
--where wa=5
group by wa,ne,neursachenb,neursacheb,nenschutz,neswald 

update bwi_vorkl.b3f_ecke_vorkl be 
set
wlt_wiev = 5
where 
wa=5 and wlt_wiev = -1

select distinct wlt_wiev 
from bwi_vorkl.b3f_ecke_vorkl bfev 

/* Eigentum
 * 
 */
select * 
from bwi.b3_ecke_202212 be 
where wa=5 and ne is not null
 


