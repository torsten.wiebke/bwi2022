/* Skript von Sebastian am 12.02.25 für die Identifikation der Ecken für die CI2027 erstellt
 * 
 */ 

--BB=12, 
--BE=11
--bv_ecke = Falsch: Vorgängerdaten!
--b3f_ecke_vorkl = "aktuelle" Vorklärungsdaten


--Staatswald Land 2x2 km
select count(*) as Staatswald_land_alle
from bwi_2022.b3f_ecke_vorkl be 
where be.eg = 2
-- 3032

--Staatswald Land 2x2 km begehbar
select count(*) as Staatswald_land_begehbar
from bwi_2022.b3f_ecke_vorkl be 
    where be.eg = 2
    and be.begehbar = 1
--2969
--__________________________________________________
    
--Staatswald Land BB 2x2 km - Bundesland über BL
select count(*) as Staatswald_BB_alle
from bwi_2022.b3f_ecke_vorkl be 
join bwi_2022.b3_ecke be2 
    ON be.tnr = be2.tnr 
    and be.enr = be2.enr 
	where be.eg = 2
	and be2.bl = 12
--2857
	
--Staatswald Land BB 2x2 km - Bundesland über VBL
select count(*) as Staatswald_BB_alle
from bwi_2022.b3f_ecke_vorkl be 
	where be.eg = 2
	and be.vbl = 1216
--2857
	
--Staatswald Land BB 2x2 km begehbar b3_ecke
select count(*) as Staatswald_BB_begehbar
from bwi_2022.b3f_ecke_vorkl be 
join bwi_2022.b3_ecke be2 
    ON be.tnr = be2.tnr 
    and be.enr = be2.enr 
	where be.begehbar = 1
	and be.eg = 2
	and be2.bl = 12
--2796
	
--Staatswald Land BB 2x2 km begehbar b3f_ecke_vorkl
select count(*) as Staatswald_BB_begehbar
from bwi_2022.b3f_ecke_vorkl be 
	where be.begehbar = 1
	and be.eg = 2
	and be.vbl = 1216
--2796
--__________________________________________________	

--Staatswald Land BE 2x2 km 
select count(*) as Berlin_alle
from bwi_2022.b3f_ecke_vorkl be 
	where be.eg >= 1
	and be.vbl = 1116
-- 180
	
--BE 2x2 km begehbar
select count(*) as Berlin_begehbar
from bwi_2022.b3f_ecke_vorkl be 
	where be.begehbar = 1
	--and be.eg = 2
	and be.vbl = 1116
--179
	
--___________________________________________________

-- Andere Eigentumsformern BB 
SELECT COUNT(*) AS BB_alle
FROM bwi_2022.b3f_ecke_vorkl be
WHERE be.eg IN (1, 3, 4, 5)  
  AND be.vbl = 1216;
 --8427	

 -- Andere Eigentumsformern BB Netz64 8x8 
 SELECT COUNT(*) AS BB_alle
FROM bwi_2022.b3f_ecke_vorkl be
join bwi_2022.b3_tnr netz
ON be.tnr = netz.tnr
WHERE netz.netz64 = 64
  and be.eg IN (1, 3, 4, 5)  
  AND be.vbl = 1216;
--558
	
-- Andere Eigentumsformern BB begehbar
SELECT COUNT(*) AS BB_alle
FROM bwi_2022.b3f_ecke_vorkl be
WHERE be.begehbar = 1
  AND be.eg IN (1, 3, 4, 5)  
  AND be.vbl = 1216;
 --8254
 
 -- Andere Eigentumsformern BB Netz64 8x8 begehbar
 SELECT COUNT(*) AS BB_alle
FROM bwi_2022.b3f_ecke_vorkl be
join bwi_2022.b3_tnr netz
ON be.tnr = netz.tnr
WHERE netz.netz64 = 64
  and be.begehbar = 1
  and be.eg IN (1, 3, 4, 5)  
  and be.vbl = 1216;
--550
   
