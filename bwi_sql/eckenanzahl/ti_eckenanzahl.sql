select count(*) as tnr_anz, sum(enr) as enr_anz,
       sum(case when wenr > 0 then 1 else 0 end) as wtnr_anz,
       sum(wenr) as wenr_anz,
	   sum(lw_wenr) as lw_wenr
  from ( select tnr, netz, netz64,
                min(vbl) as min_vbl,
	            max(vbl) as max_vbl,
	            count(*) as enr,
	            sum(wa) as wenr,
	            sum(lw_jn) as lw_wenr
           from (select e.Vbl, e.tnr, e.enr, Netz, Netz64,
                        case when wa in (3,4,5) then 1 else 0 end as wa,
	                    case when wa in (3,4,5) and eg = 2 then 1 else 0 end as lw_jn
                   from bwi.dat.b0_ecke as e
                   join bwi.dat.b0_tab as t
                     on t.vbl = e.vbl and t.tnr = e.tnr
                   left outer join bwi.dat.b4_ecke_w as w
                     on e.tnr = w.tnr and e.enr = w.enr
                   where e.Vbl in (1116,1216)
                     and InvE4 = 1) as a
           group by tnr, netz, netz64 ) as b
 -- where min_vbl = 1116 and max_vbl = 1116
 -- where min_vbl = 1216 and max_vbl = 1216 and Netz64 = 64
  where min_vbl = 1216 and max_vbl = 1216 and Netz64 <> 64 and lw_wenr > 0  -- and Netz in (4,8,16)
 -- where min_vbl = 1116 and max_vbl = 1216
