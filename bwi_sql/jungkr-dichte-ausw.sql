-- New script in projekt_lfb.
-- Date: 03.03.2025
-- Time: 13:57:51
-- Database: projekt_lfb            - target database name

-- jungkr
select xb.kurzd, j2.gr, xg.kurzd as grd, j2.biss, j2.anz
--,j2.*
from bwi_r.jung_22 j 
join bwi_r.jungpfl_22 j2 on j.tnr=j2.tnr and j.enr=j2.enr 
join bwi_meta.x3_ba xb on j2.ba = xb.icode
join bwi_meta.x3_gr xg on j2.gr = xg.icode

-- jungkr-dichte
select 
j2.tnr, j2.enr, 
xb.kurzd, j2.gr, xg.kurzd as grd, j2.biss, 
j2.anz,
sum(j2.anz) over (partition by j2.tnr, j2.enr) bäume_pro_ecke,
sum(j2.anz) over (partition by j2.tnr, j2.enr) / (pi() * j.b0_radius ^ 2)::numeric as pflanzendichte_qm_anecke,
sum(j2.anz) over (partition by j2.tnr, j2.enr,xb.kurzd) bäume_pro_baumartundecke,
sum(j2.anz) over (partition by j2.tnr, j2.enr,xb.kurzd) bäume_pro_baumartundecke,
j.b0_zaun,
j.b0_radius  
--,j2.*
from bwi_r.jung_22 j 
join bwi_r.jungpfl_22 j2 on j.tnr=j2.tnr and j.enr=j2.enr 
join bwi_meta.x3_ba xb on j2.ba = xb.icode
join bwi_meta.x3_gr xg on j2.gr = xg.icode

-- jungkr-dichte 1
SELECT 
    j2.tnr, 
    j2.enr, 
    xb.kurzd, 
    j2.gr, 
    xg.kurzd AS grd, 
    j2.biss, 
    j2.anz,
    j.b0_zaun,
    j.b0_radius  
FROM 
    bwi_r.jung_22 j 
JOIN 
    bwi_r.jungpfl_22 j2 ON j.tnr = j2.tnr AND j.enr = j2.enr 
JOIN 
    bwi_meta.x3_ba xb ON j2.ba = xb.icode
JOIN 
    bwi_meta.x3_gr xg ON j2.gr = xg.icode;

-- jungkr-dichte 2
WITH aggregated AS (
    SELECT 
        j2.tnr, 
        xb.kurzd, 
        j2.gr, 
        xg.kurzd AS grd, 
        j2.biss, 
        j.b0_zaun,
        sum(j2.anz) AS sum_anz,
        sum((pi() * j.b0_radius ^ 2)::numeric) AS fläche
    FROM 
        bwi_r.jung_22 j 
    JOIN 
        bwi_r.jungpfl_22 j2 ON j.tnr = j2.tnr AND j.enr = j2.enr 
    JOIN 
        bwi_meta.x3_ba xb ON j2.ba = xb.icode
    JOIN 
        bwi_meta.x3_gr xg ON j2.gr = xg.icode
    GROUP BY 
        j2.tnr, xb.kurzd, j2.gr, xg.kurzd, j2.biss, j.b0_zaun
)
SELECT 
    *,
    sum(sum_anz) over (partition by tnr,kurzd,gr, biss),
    sum(sum_anz) over (partition by tnr,kurzd,gr, biss) / fläche * 10000 AS pflanzendichte_baumart_gr_biss_trakt
FROM aggregated;