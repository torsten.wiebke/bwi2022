--Bäume in WZP4 mit BHD
SELECT COUNT(DISTINCT (wzp.tnr, wzp.enr, wzp.ba, wzp.pk, wzp.m_bhd)) AS line_count
FROM bwi_2022.b3v_wzp wzp
WHERE wzp.m_bhd is not null
  
-- Trakte kontrolliert
SELECT COUNT(kt.unterschriftkt) AS count
FROM bwi_2022.b3f_tnr_work kt
WHERE kt.unterschriftkt IS NOT NULL
  AND kt.unterschriftkt != '';
 
-- Ecken kontrolliert
SELECT COUNT(kt.unterschriftkt) AS count
FROM bwi_2022.b3f_tnr_work kt
JOIN bwi_2022.b3f_ecke_vorkl wa
    ON kt.tnr = wa.tnr 
WHERE kt.unterschriftkt IS NOT NULL
  AND kt.unterschriftkt != ''
  AND wa.wa IN (3, 4, 5);