SELECT wzp.tnr, 
       wzp.enr, 
       wzp.m_hoe, 
       wzp.m_hoe / 10.0 AS m_hoe_m, 
       wzp.ba, 
       x3.langd AS ba_name, 
       wzp.pk, 
       wzp.m_bhd,
       wzp.m_bhd / 10.0 AS bhd_cm,
       wzp.al_ba, 
       geom.geom::geometry(Point, 25833),
       COUNT(*) AS line_count
FROM bwi_2022.b3v_wzp wzp
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = wzp.tnr 
    AND geom.enr = wzp.enr 
JOIN bwi_meta.x3_ba x3 
    ON wzp.ba = x3.icode
WHERE wzp.al_ba IS NOT NULL 
  AND wzp.pk < 6
  AND wzp.pk <> 4
GROUP BY wzp.tnr, wzp.enr, wzp.m_hoe, wzp.ba, x3.langd, wzp.pk, wzp.m_bhd, wzp.al_ba, geom.geom
ORDER BY line_count ASC, wzp.ba;

SELECT wzp.ba, 
       x3.langd AS ba_name, 
       COUNT(*) AS ba_count
FROM bwi_2022.b3v_wzp wzp
JOIN bwi_meta.x3_ba x3 
    ON wzp.ba = x3.icode
WHERE wzp.al_ba IS NOT NULL 
  AND wzp.pk < 6
  AND wzp.pk <> 4
GROUP BY wzp.ba, x3.langd
ORDER BY ba_count ASC
LIMIT 1;

WITH ba_counts AS (
    SELECT wzp.ba, 
           COUNT(*) AS ba_count
    FROM bwi_2022.b3v_wzp wzp
    WHERE wzp.al_ba IS NOT NULL 
      AND wzp.pk < 6
      AND wzp.pk <> 4
    GROUP BY wzp.ba
)
SELECT wzp.tnr, 
       wzp.enr, 
       wzp.m_hoe, 
       wzp.m_hoe / 10.0 AS m_hoe_m, 
       wzp.ba, 
       x3.langd AS ba_name, 
       wzp.pk, 
       wzp.m_bhd,
       wzp.m_bhd / 10.0 AS bhd_cm,
       wzp.al_ba, 
       geom.geom::geometry(Point, 25833),
       ba_counts.ba_count
FROM bwi_2022.b3v_wzp wzp
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = wzp.tnr 
    AND geom.enr = wzp.enr 
JOIN bwi_meta.x3_ba x3 
    ON wzp.ba = x3.icode
JOIN ba_counts 
    ON wzp.ba = ba_counts.ba
WHERE wzp.al_ba IS NOT NULL 
  AND wzp.pk < 6
  AND wzp.pk <> 4
ORDER BY ba_counts.ba_count ASC, wzp.ba;