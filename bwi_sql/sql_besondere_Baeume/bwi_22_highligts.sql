-- Database: projekt_lfb

/* Highligts aus den Messungen zur BWI 2022
 * 
 */

-- Sebastians Ansatz für den größten BHD
SELECT distinct wzp.tnr, 
       wzp.enr, 
       wzp.m_hoe, 
       wzp.m_hoe / 10.0 AS m_hoe_m, 
       wzp.ba, 
       x3.langd AS ba_name, 
       wzp.pk, 
       wzp.m_bhd,
       wzp.m_bhd / 10.0 as bhd_cm,
       wzp.al_ba, 
       geom.geom::geometry(Point, 25833)
FROM bwi_2022.b3v_wzp wzp
JOIN waldinv_geo.b3_ecke_ist geom 
    ON geom.tnr = wzp.tnr 
    AND geom.enr = wzp.enr 
JOIN bwi_meta.x3_ba x3 
    ON wzp.ba = x3.icode
WHERE wzp.m_bhd IS NOT null
  AND wzp.pk < 6
  AND wzp.pk <> 4
ORDER BY wzp.m_bhd DESC;

-- highligtbäume
WITH ranked_data AS (
select distinct
wzp.tnr, 
       wzp.enr, 
       wzp.m_hoe, 
       wzp.m_hoe / 10.0 AS m_hoe_m, 
       wzp.ba, 
       x3.langd AS ba_name, 
       wzp.pk, 
       wzp.m_bhd,
       wzp.m_bhd / 10.0 as bhd_cm,
       wzp.al_ba, 
       istgeom.geom::geometry(Point, 25833),
        RANK() OVER (ORDER BY wzp.m_hoe DESC NULLS LAST) AS rank_hoe,
        RANK() OVER (ORDER BY wzp.m_bhd DESC NULLS LAST) AS rank_bhd,
        RANK() OVER (ORDER BY wzp.al_ba DESC NULLS LAST) AS rank_al_ba
FROM bwi_2022.b3v_wzp wzp
JOIN waldinv_geo.b3_ecke_ist istgeom 
    ON istgeom.tnr = wzp.tnr 
    AND istgeom.enr = wzp.enr 
JOIN bwi_meta.x3_ba x3 -- für Baumartanzeige
    ON wzp.ba = x3.icode
WHERE wzp.m_bhd IS NOT null
  AND wzp.pk between 0 and 1 -- probebaumkennziffer »neuer Probebaum« oder »wiederholt aufgenommen« - siehe select  * from bwi_meta.x3_pk xp 
  AND wzp.pk <> 4 -- Probebaumkennziffer  »nicht stehend, am Ort verblieben (ggf. Totholz)«
  )
SELECT *
FROM ranked_data
WHERE rank_hoe <= 10
   and rank_bhd <= 10
   and rank_al_ba <= 10
;

-- Versuch 3
WITH ranked_data AS (
    SELECT distinct
        wzp.tnr, 
        wzp.enr, 
        wzp.m_hoe, 
        wzp.m_hoe / 10.0 AS m_hoe_m, 
        wzp.ba, 
        x3.langd AS ba_name, 
        wzp.pk, 
        wzp.m_bhd,
        wzp.m_bhd / 10.0 as bhd_cm,
        wzp.al_ba, 
        istgeom.geom::geometry(Point, 25833),
        RANK() OVER (ORDER BY wzp.m_hoe desc NULLS LAST) AS rank_hoe,
        RANK() OVER (ORDER BY wzp.m_bhd desc NULLS LAST) AS rank_bhd,
        RANK() OVER (ORDER BY wzp.al_ba desc NULLS LAST) AS rank_al_ba
    FROM bwi_2022.b3v_wzp wzp
    JOIN waldinv_geo.b3_ecke_ist istgeom 
        ON istgeom.tnr = wzp.tnr 
        AND istgeom.enr = wzp.enr 
    JOIN bwi_meta.x3_ba x3 -- für Baumartanzeige
        ON wzp.ba = x3.icode
    WHERE wzp.m_bhd IS NOT NULL
      AND wzp.pk BETWEEN 0 AND 1
      AND wzp.pk <> 4
),
top_hoe AS (
    SELECT 'top_hoe' as top, * 
    FROM ranked_data
    WHERE rank_hoe <= 10
    order by m_hoe
),
top_bhd AS (
    SELECT 'top_bhd' as top,* 
    FROM ranked_data
    WHERE rank_bhd <= 10
    order by m_bhd
),
top_al_ba AS (
    SELECT 'top_al' as top, * 
    FROM ranked_data
    WHERE rank_al_ba <= 10
    order by al_ba
)
-- Die finalen Top-10-Ergebnisse zusammenführen
SELECT * 
FROM top_hoe
UNION
SELECT * 
FROM top_bhd
UNION
SELECT * 
FROM top_al_ba
order by rank_al_ba,rank_bhd,rank_hoe ;

-- Versuch 4 - als View gespeichert

drop view bwi_2022.v_top_wzp;

create or replace view bwi_2022.v_top_wzp as (
WITH ranked_data AS (
    SELECT distinct
        wzp.tnr, 
        wzp.enr, 
        wzp.m_hoe, 
        wzp.m_hoe / 10.0 AS m_hoe_m, 
        wzp.ba, 
        x3.langd AS ba_name, 
        wzp.pk, 
        wzp.m_bhd,
        wzp.m_bhd / 10.0 as bhd_cm,
        wzp.al_ba, 
        istgeom.geom::geometry(Point, 25833),
        RANK() OVER (ORDER BY wzp.m_hoe DESC NULLS LAST) AS rank_hoe,
        RANK() OVER (ORDER BY wzp.m_bhd DESC NULLS LAST) AS rank_bhd,
        RANK() OVER (ORDER BY wzp.al_ba DESC NULLS LAST) AS rank_al_ba
    FROM bwi_2022.b3v_wzp wzp
    JOIN waldinv_geo.b3_ecke_ist istgeom 
        ON istgeom.tnr = wzp.tnr 
        AND istgeom.enr = wzp.enr 
    JOIN bwi_meta.x3_ba x3 -- für Baumartanzeige
        ON wzp.ba = x3.icode
    WHERE wzp.m_bhd IS NOT NULL
      AND wzp.pk BETWEEN 0 AND 1
      AND wzp.pk <> 4
),
top_hoe AS (
    SELECT 'top_hoe' AS top, * 
    FROM ranked_data
    WHERE rank_hoe <= 10
    ORDER BY m_hoe DESC
),
top_bhd AS (
    SELECT 'top_bhd' AS top, * 
    FROM ranked_data
    WHERE rank_bhd <= 10
    ORDER BY m_bhd DESC
),
top_al_ba AS (
    SELECT 'top_al' AS top, * 
    FROM ranked_data
    WHERE rank_al_ba <= 10
    ORDER BY al_ba DESC
),
vereinigt as (
-- Die finalen Top-10-Ergebnisse zusammenführen
SELECT *
FROM top_hoe
UNION ALL
SELECT * 
FROM top_bhd
UNION ALL
SELECT * 
FROM top_al_ba
ORDER BY rank_al_ba, rank_bhd, rank_hoe)
SELECT top,  ba_name, m_hoe_m, rank_hoe, bhd_cm,  rank_bhd, al_ba, rank_al_ba
from vereinigt);
  
