-- CONNECTION: name=u_waldinv@wiegis_lfe
-- GIN-Index erzeugen auf df um die document_tokens zu nutzen
CREATE INDEX tsvind ON bwi_waldinv.df USING gin (document_tokens)

-- namensauswahltabelle
select be.tnr, be.enr,be.eg,be.eggrkl,bfev.wa,bfev.eg as ti_eg, bfev.eggrkl as ti_egrkl, d.bwi_ea  as d_eg, d.eiggrkl as d_egrkl,d.sicher,d.eig_schr_gb, d.eig_schreib_vereinh, d.document_tokens,  d.flstckt  
from bwi_waldinv.b3_ecke_202212 be 
join bwi_waldinv.b3f_ecke_vorkl bfev on be.tnr=bfev.tnr and be.enr=bfev.enr 
join bwi_waldinv.df d on d.tnr=be.tnr and d.enr=be.enr 
where bfev.wa between 1 and 5
and
bfev.eg=1
and d.sicher is null 
and d.ort is  not null 
--and bfev.eg is null
--order by d.enr asc, d.tnr