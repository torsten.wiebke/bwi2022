-- CONNECTION: name=u_waldinv@wiegis_lfe
-- New script in u_waldinv@wiegis_lfe 167.235.247.86  ${server} wiegis_lfe .
-- from torsten 
-- in /home/torsten/.local/share/DBeaverData/workspace6/bwi   
-- Date: 23.03.2023
-- Time: 12:21:20

-- Wieviel Wald gibt es zu diesen Eigentümernummern im gesamten Wald
SELECT 
    CASE 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 5 THEN '11: bis 5 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 10 THEN '12: > 5 bis 10 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 20 THEN '13: > 10 bis 20 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 50 THEN '2: > 20 bis 50 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 100 THEN '3: > 50 bis 100 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 200 THEN '4: > 100 bis 200 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 500 THEN '5: > 200 bis 500 ha' 
        WHEN SUM(szan.summe_wald_qm)/10000::real <= 1000 THEN '6: > 500 bis 1000 ha' 
        ELSE '7: > 1000 ha' 
    END AS klassen,
    SUM(szan.summe_wald_qm)/10000::real AS flaeche
FROM bwi_waldinv.summewaldfl_zu7598adr_nr szan
WHERE szan.adr_nr IN (select  d.adresse_nr--,d.*  
    from 
    bwi_waldinv.df d 
    where --d.eig_schr_gb similar to '%Lausitzer und%'
    d.document_tokens @@to_tsquery('Solms-Laubach & friedberg')--('Solms-Laubach & frankfurt')--('Zuydtwyck&potsdam')--('Zweckverband & ludwigsfelde')--('Zweckverband&Eberswalde')--('Zwiebel ')--('Zwillenberg-Tietz-Stiftung ')--('Stockhausen & börries ')
    --('Stockhausen & mary ')--('Deutsch-Polnischen  ')--('WWF ')--('Umweltstiftung &WWF ')--('Ulrich & Sabine')--('Ulrich & Michael')--('Ulrich & Katrin & marienberg ')--('Ulrich & Katrin & vetschau ')--('Ulrich & Dietmar ')--('Ukro ')--'Uhlitz ')--('Tygör & arno')--('Turbanisch ')--('Tunkel ')--('Tschöke ')--('Trott ')--('Tschirner ')--('Trummer ')--('Dillen ')--('Triltsch ')
    --('Trietchen ')--('Trachsel ')--('Toschka ')--('Tornow & Ingrid  ')--('Tönnies & Klemens ')--('Thiermann & scharringhausen')--('Thiermann & butzow')--('Thierbach & Sebastian ')--('Thierbach & Matthias ')--('Thierbach & Manuel ')--('Thiemke & mario')--('Thiemke & ernst')--('Thieme')--('Thielemann &Silvio')
    --('Thielemann &Jörg')--'Thielemann &Gerd')--('Thiele & Rosemarie ')--('Thiele & Ronald ')--('Thiele & Renate ')--('Thiele & Nora ')--('Thiele & Hubert ')--('Thiele & Gerd ')--('Thiele & Eveline ')--('Thiele & Elisa ')--('Thiele & Carsten ')--('Thiele & Burkhard ')--('Thiele & André ')--('Strempel&hans-dirk')
    --('Strempel& christel')--('Strelow')--('Stramke')--('Stöwer')--('Stottmann & cloppenburg')--('Stottmann & neuruppin')--('Storandt')--('Stolle & Regina ')--('Stolle & Gerd ')--('Stoffel & Brigitte')--('Naturlandschaften & Brandenburg ')--('Stift & Neuzelle ')--('Stiewe')--('Stieger & peter')--('Stewin')--('Steuck')--('Stephan & Bettina ')--('Stephan & Kerstin ')--('Steinriede & hans-wolfgang ')
    --('Steinriede & ingo ')--('Steinhilber ')--('Steffen & renate ')--('Stefan & Helga ')--('Stechow & Karl-Alexander')--('Stechow & Jesko')--('Stechow & benita')--('Stechbarth')--('Kummer & carmen')--('Kühling & Emstek')--('Kühling & berlin ')--('Staufenbiel ')--('Stauber ')--('Staudinger ')--('Stammermann ')--('Stahn ')--('Stahl &Peter ')--('Stahl &Benjamin ')--('Staffehl  ')
    --('Staeck  ')--('Stadtgemeinde & Lieberose  ')--('Amt &Unterspreewald  ')--('Stadt &Ziesar ')--('Stadt &Zehdenick ')--('Stadt &Wittenberge ')--('Stadt &Werneuchen ')--('Stadt & Vetschau/Spreewald  ')--('Stadt & Treuenbrietzen  ')--('Stadt & Templin  ')--('Stadt & Storkow  ')--('Stadt & Spremberg  ')--('Stadt & Schönewalde  ')--('Stadt & Rathenow  ')
    --('Stadt & Pritzwalk  ')--s('Stadt & Luckenwalde  ')--('Stadt & Lebus  ')--('Stadt & Königs  ')--('Stadt & Ketzin/Havel  ')--('Stadt & Herzberg ')--('Stadt & Großräschen ')--('Stadt & Golßen ')--('Stadt & Gartz ')--('Stadt & Friesack ')--('Stadt & Friedland ')--('Stadt & Forst ')--('Stadt & Falkensee ')
    --('Stadt & Falkenberg ')--('Stadt & Eberswalde ')--('Stadt & Doberlug-Kirchhain ')--('Stadt & Dahme/Mark ')--('Stadt & Calau ')--('Stadt & Brandenburg ')--('Stadt & Beelitz ')--('Stadt & Baruth/Mark   ')--('Stadt & Bad & Wilsnack  ')--('Ozik-Scharf &Anja  ')--('Scharf&grobower ')--('Scharein ')
    --('Schennen & Beryl')--('Schalk & günter')--('Schalitz')--('Scheen')--('Schalewa')--('Schäfer&Wolfgang')--('Schäfer&torsten')--('Schäfer&Maik')--('Schäfer&Falko')--('Schäfer&Dietmar')--('Schäfer&Brigitte')--('Schäfer&angelika')--('Schäfer&gert')--('Schacky & andreas')--('Schacky & claudia')--('sasse & birgit')--('saß&christiane')
    --('Agrargenossenschaft &Freyenstein')--('Wald- & Grundbesitz  ')--(' Gut & Stieten  ')--(' Forst & Landbetrieb & Göllnitz ')--(' bvvg')--(' reichelt &ute')--(' reichelt & alexandra')--(' Rawolle&harald')--(' Pilz')--(' Petersdorf & ronny')--(' Paulo&hans-christian')--(' Paulo&gisela')
    --(' Paulo & rosemarie')--(' Paulo & ralf')--(' Orlikowski')--(' Ohm')--(' Naumann')--(' NABU-Stiftung')--(' Musick & torsten')--(' Muschalla')--(' martens&jan')--(' mahlow&alma')--(' mahlow&jörg')--(' Lynar-Lassen')--(' Lukas &petra')--(' Lukas &jürgen')--(' Lippmann &andernach')--(' Lippmann &oberland')
    --(' Lippmann &torgau')--(' Lemberger')--(' Laudien &andreas&bad')--(' Laudien &andreas& wohltorf')--(' Laudien & kay')--(' Künnemeyer&dorfstraße')--(' Künnemeyer&klaus-otto')--(' Kulick&werner')--(' Kulick&erika')--(' kühnel&inge')--(' kühnel&ilse')--(' klopp&margot')--(' klopp&karin')--(' Klitzing&karl')
    --(' Klitzing&jürgen')--(' Kiep')--(' Kiefer &stefan&landwehr')--(' Kiefer &stefan&steinreich')--(' Kiefer &martin')--(' Kiefer & heinz')--(' Keller&detlev')--(' Keller&jens')--(' Käthe & werner')--(' Käthe & ruth')--(' Käthe & bertram')--(' Kangarlou')--(' Kampsen')--(' Kaltschmidt & toni')
    --(' Kaltschmidt & sebastian')--(' Kaltschmidt & danny')--(' Kaltschmidt & daniel')--(' Kaltschmidt & bodo')--(' kallus')--(' kaiser & udo')--(' kaiser & antje')--(' Kahle')--('Kägeler ')--('jordan & karsten ')--('jordan & jörg')--('jordan & ellen')--('jordan & antje')--('jordan & roland')--('Klima& waldgesellschaft ')
    --('Klima & marlies ')--('Klee-Trieschmann ')--('Kindermann')--('Khadjavi-Gontard ')--('Kätzmer & felix')--('Kamphorst & Königs')--('Kamphorst & heidesee')--('Kampfhenkel')--('Jirjahlk')--mielost ')--('mielost ')--('jeß & kerstin')--('Jefferies')--('jasper & josef')--('jasper & paul')--('Janus')--('Jankowski& dirk')
    --('Höhne &sylvia')--('Höhne & otto')--('Hoffmann & hans-jochen')--('Hoffmann & hans & 53819')--('Hoffmann & gisela')--('Hoffmann & frank')--('Hinze & joachim')--('Hinze & ilse')--('Hinze& Robert')--('Hinckeldey &olga')--('Hinckeldey & bettina')--('Hildebrand')
    --('Hilbers & konrad')--('Hilbers & albert')--('Hesse & Peter')--('Hesse & Dorfstr')--('Herzig')--('Herz')--('Herke')--('Lynar & rochus& berlin &  alexander')--('Lynar & rochus& berlin & ! alexander')--('Lynar & rochus & lübbenau')--('Lynar&Luise')--('Lynar & bettina & kaiserdamm')--('Lynar & bettina & fasanenstraße')--('Lynar & bettina& bülowstraße')
    --('Halmai')--Hannemann')--('Hannemann & otto-irmin')--('Hannemann & karin')--('Hannemann & hannelore')--('Hannemann & frank')--('Hannemann & eckhard')--('hammer')--('hamker')--('schmidt & steffen ')--('Hartmann & raik')--('Hartmann&mark')--('Hartmann &katja')--('Hartmann & jana')--('Hartmann & bärbel')--('Harting & calau')--('Harting & münchen')
    --('Hannuschke & jörg')--('Hannuschke & jens')--('Hannuschke & gerd')--('Hannuschke& klaus')--('Hamacher')--('Helm &harald')--('Helm & gerald')--('Gratz')--('Gartz/Oder ')--('Düsterhöft')--('Dietrich & peter')--('Endres & Schonungen')--('Endres & Berlin')--('Ebel & Marlies')--('Ebel & Albert')
    --('Duwe & maik')--('Duwe &jörg')--('Fiebig & christel')--('Fiebig &sandra')--'Fiege & elisabeth')--('Ender & manfred')--Elbert('Elbert &wilhelm')--('Elbert & gerd')--('Eckardstein & heike')--('Eckardstein & Christian')--('Ey')--('Lippe &!schloßplatz &! Schlossplatz ')--('Wilke & Lorenz')--('Kirche & Dümde ')
    )