# Ministerbesuch zur Pressetermin nach BWI-Veröffentlichung des BMEL am 08.10.2024

## Pressekonferenz BMEL

- 8.10.2024 vorraussichtlich Ministerteilnahme Cem Özdemir
- Schwerpunktthemen
  - Baumartenausfälle (ggf. mit Vergleich Schwerpunktländer)
  - Unterstand / Verjüngung
  - Naturnähe
  - Perspektiven des neuen Waldes

## Pressekonferenz MLUK  

### vorraussichtliche Hauptinteressen der Presse

- [ ] Wald der Zukunft
- [ ] Verbiss und dessen Bewertung
- [ ] Wieviel Bäume im Brandenburger Wald

### Termin

- Montag, 14. Oktober 2024 09:30 -- 11:30 Uhr
  - Einladung nach BMEL PK BWI am 08.10.2024 durch MLUK

### Ort  

- ggf. Waldschule im Wildpark Potsdam
- [ ] **Anmerkung**: Ort noch nicht bestätigt
-Sebastian Arnold und Referat 46 klären Ort  
- [ ] **Klärung**: Waldbild vor Ort
- [ ] **Klärung**: Technische Ausstattung
  - [ ] Referat 46 will zusammen mit LFB großen Monitor besorgen
- [ ] **Klärung**: Catering - MLUK mit Pressestelle MB2
- [x] **Klärung**: Parkmöglichkeiten - sind vorhanden vor Ort
- [ ] **Klärung**: Anreise - Plan mit Presseeinladung, Aufsteller bei Einfahrt (Dirk Eichhorst Waldschule Wildpark)

 
### Teilnehmer

- Minister
- MLUK, Referat 46 - Dr Carsten Leßner, Martina Heinitz
- Abt. 4 Urlrike Hagemann - Präsentation
- FB42 - Leitung Ralf Kätzel - ist nicht notwendig, aber kann natürlich!
- SG Waldinventuren

### Organisatorisches

- [ ] Infos ad hoc zu Martina
- [ ] **nächste Beratung Pressemappe**: in der Woche vom 12.-13.9. 
- [ ] **Drucklayout**: in der Woche vom 12.-13.9. 
- [ ] **Klärung**: Aufbau
- [ ] **Klärung**: Ablauf - kommt mit Presseeinladung MLUK
- [ ] **Klärung**: Ansprechpartner - Pressestelle MB2
- [ ] **Klärung**: Pressemappe - siehe auch [Pressemappe](#pressemappe)
- [ ] **Klärung**: Presseausweis -  Pressestelle  MB2
- [ ] **Klärung**: Pressemitteilung - Entwurf Martina Heinitz in Abstimmung mit LFE und Abt. 4
- [ ] **Klärung**: Presseverteiler - Pressestelle MB2
- [ ] **Klärung**: Presseakkreditierung - Pressestelle MB2
- [ ] **Klärung**: Pressekontakt - Pressestelle MB2
- [ ] **Klärung**: Pressefotos - Pressestelle MB2
- [ ] **Klärung**: Presseausstattung - Pressestelle MB2
- [ ] **Klärung**: Presseanfragen - Pressestelle MB2
- [ ] **Klärung**: RBB "Der Tag" - Pressestelle MB2

### Kommunikationskette

### Pressemappe

- Unterlagen eine Woche vor PK fertig

#### Erstellung durch

- Martina Heinitz,  
- Torsten Wiebke,  
- Ulrike Hagemann,  
- Jan Engel,  
- Sandra Gensch
- Pressetelle MB2

#### Pressemappeninhalt

- [ ] Presseeinladung inkl. Ablaufplan
- [ ] Flyer
- [ ] Powerpoint Frau Hagemann
- [?] weiterführende Informationen & Ergänzungen zu den Flyerthemen - soweit von Belang besser direkt in Vortarg von Frau Hagemann integrieren?
  - [ ] Zuwachs: Bis CI 17 gestiegen, dann "eingebrochen"; Prognose CI 27 - bricht weiter ein
  - [ ] Verjüngung  
    - [ ] 4 m und
    - [ ] Verjüngungskreis
    - [ ] Vergleich NV in Privatwald und LW
    - [ ] Diskussion mit Ergebnissen aus VWM?
  - [ ] Diff. nach Eigentumsform & Baumart ? Signifikanz
  - [ ] neue Regierung, neue Ziele?
  - [ ] Ausblick nächste Inventuren - CI 2027
  - [ ] Waldschutz?
  
### Flyer

#### Technisch

- erweitertete Visitenkarten mit den wichtigsten Informationen
- ca. Din A4 doppelseite
- Erstellung als SVG
- zwei / drei Textabschnitte [BWI Methodik und Trends](#bwi-methodik-und-trends) und [Trends der BWI](#trends-der-bwi-2022) sowie [Weitere Informationen und Ansprechpartner](#weitere-informationen-und-ansprechpartner)

#### Flyerinhalt

##### Abstrakt

kurzer Abriss zur BWI und den Trends

- was ist die BWI
- Durchgeführt seit 1987, seit 2002 Gesamtdeutsch
- letzte Inventur 2012
- Trends 2012 - 2022

##### BWI Methodik und Trends

> Die Bundeswaldinventur (BWI) ist eine durch das Bundeswaldgesetz vorgeschriebene forstliche Großrauminventur, die deutschlandweit durchzuführen ist. Mit der Bundeswaldinventur werden die großräumigen Waldverhältnisse und forstlichen Produktionsmöglichkeiten in Deutschland erfasst. Ihre Ergebnisse sind eine Grundlage für forst-, handels- und umweltpolitische Planungen und Entscheidungen. 

Die erste BWI wurde in den Jahren 1986 bis 1989 in Westdeutschland durchgeführt. Die erste Gesamtdeutsche Bundeswaldinventur (BWI 2002) wurde von 2001 bis 2003 durchgeführt. Die dritte, die BWI 2012, wurde von 2011 bis 2012 durchgeführt. Die vierte Bundeswaldinventur (BWI 2022) wurrde von 2021 bis 2022 durchgeführt.

##### Trends der BWI 2022

Brandenburgs Wälder sind weiterhin durch Kiefern dominiert, der Anteil ist durch den Waldumbau um zwei Prozent gesunken. Die Vorräte sind weiter gestiegen. Das vorherrschende Alter ist 60 Jahre. In dem Alter sind noch verstärkte Zuwächse zu verzeichnen was die weitere Vorratssteigerung begründet. Der Anteil von Beständen mit mehreren Schichten und mehr Laubholz ist ebenfalls gestiegen. Brandenburg ist auf dem Weg zu einen strukturreicheren Mischwald.

##### Infografiken

Ein oder Zwei umschließende Infografiken mit folgenden Inhalten in/an/mit 10 grafischen Elementen:

1. **"Umrisskarte" Brandenburg** mit grafisch gestalteter "Zoom" zu Stichprobenpunkt
   - Botschaft: Waldfläche ist stabil (+100 ha) - zwei Zahlen
   - BB Top 5 (4) waldreichste Bundesländer
   - grafisch gestalteter "Zoom" zu Stichprobenpunkt
     - Grafiken Methodik / Probekreise u.ä.
     - Anzahl Waldecken / Gemessene Bäume / Arbeitsstunden Trupps o.ä.
2. **Eigentumsverhältnisse**
   - Vergleich zum Waldflächennachweis ?
   - Leichter Übergang Staatswald zu Privatwald
   - Botschaft: Eigentumsvereteilung bleibt weitgehend stabil - Betreuung Privatwald weiterhin relevant
3. **Baumartenzusammensetzung**
   - Kiefer, Eiche, Buche, (ggf.Birke, Fichte?)
   - reine Verteilung in Prozent, dahinter kurzmessage mit Botschaft
   - Botschaft: Kiefer gesunken, Laubholzansteil gestiegen
4. **Struktur** (in Kombi mit Baumartenzusammensetzung darstellen?) 
   - Botschaft: Der zukünftige Wald ist / hat wesentlich höheren Laubholzanteil & Baumartenmischung
   - Bäume bis 4 m - Unterstand
5. **Vorrat**
   - 3 Hauptbaumarten - ideele Werte
   - Botschaft: weiter gestiegen
6. **Zuwachs / Nutzung**
   - Botschaft: Wächst mehr zu als wir Ernten
   - Grafisch Gegenüberstellen
7. **Totholz**
   - Botschaft: Steigerung
   - Aber: überwiegend Nadel und klein klein
8. **kleine Bäume / Verjüngungskreis**
   - Botschaft: Zu wenig Pflanzen / ha
   - Verbiss halbiert, aber noch immer zu viel bzw. zu wenig Pflanzen für Waldumbau
9. **Highlights**
   - Dickster Baum
   - Höchster Baum
   - Seltenster Baum
   - Anzahl Bäume insgesamt
   - Bäume / Ecken kontrolliert ;-)

###### Weitere Informationen und Ansprechpartner

## Broschüre

### ohne WEHAM

- Start frühestens nach PK 14.10.2024
- Veröffentlichung Ende 1. Quartal 2025

## EFS

### mit WEHAM

- Fachbericht BWI 2022 inkl. WEHAM
- Veröffentlichung 4 Quartal 2025