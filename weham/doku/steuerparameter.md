# Steuerparameter in WEHAM 2022

## Sortierung

Bis einschließlich Lauf 44 wurde die Sorteneinteilung aus WEHAM 2012 übernommen. Erst am 18.02.2025 konnte telefonisch eine Abstimmung mit dem Landeswald, in Persona Uwe Engelmann, erfolgen. Die Sorteneinteilung wurde in der [WEHAM 2022](/weham/weham_data/WEHAM_2012_2022_Sortenaufteilung_BB.xlsx) übernommen.

<!--TODO: Dokumentation fortsetzen-->

## Durchforstungsmodelle

Parallel zur BIL in Bad Breisig wird ein Lauf außerhalb des normalen Zyklus durchgeführt. Dafür sind die Steuerparameter insbesondere für die Eigentumsgruppen einzutragen:

## Landeswald

- +- ökologische Bewirtschaftung
- Zielbestockungsgrad 0,8
- Realistische Zielstärkennutzung
  - Fichte: 130 ± 10 Jahre
  - Tanne: 200 ± 10 Jahre, BHD  70 cm
  - Douglasie: 180 ± 10 Jahre, BHD  70 cm
  - Kiefer: 140 ± 10 Jahre, BHD  60 cm
  - Kiefer: 140 ± 10 Jahre, ab dem 20. Projektionsjahr
  - Lärche: 140 ± 10 Jahre, BHD  70 cm
  - Buche: 150 ± 10 Jahre, BHD  90 cm
  - Eiche: 250 ± 10 Jahre, BHD  90 cm
  - Ahorn: 180 ± 10 Jahre, BHD  70 cm
  - Erle: 110 ± 10 Jahre, BHD  80 cm
  - Pappel: 70 ± 10 Jahre, BHD  60 cm

## Bund

-  Bund  hat mehr Dienstleistungscharakter und Fokussiert stark auf Naturschutz: 
  -  lange Umtriebszeiten, kurze Pflegeintervalle in Jungwuchspflege und lange Zeiträume zwischen den Durchforstungen, nur starke Durchmesser werden genutzt, 
  - mehr Hochdurchforstung
- macht wohl BIMA - Schmitz fragt an
- wenn BIMA nichts neues liefert bleibt es beim alten Wert

## Privatwald groß > 1000 ha

- Privatwald groß: hat wirtschaftliche Interessen
- arbeitet aber eher nach ANW-Kriterien, geringe Durchmesser, viele Pflegeintervalle, mittlere Umtriebszeiten,
- Bestockungsgrad auf 0,7 und 0,8,
- mehr Hochdurchforstung
- Endnutzung mittel/früh mit Mindestdurchmesser im hohen Bereich und 50 % (gilt dann als Mindestdurchmesser)
  - Fichte: 160 ± 10 Jahre, BHD  60 cm
  - Fichte: 160 ± 10 Jahre, ab dem 15. Projektionsjahr
  - Tanne: 200 ± 10 Jahre
  - Douglasie: 180 ± 10 Jahre, BHD  70 cm
  - Kiefer: 140 ± 10 Jahre, BHD  60 cm
  - Kiefer: 140 ± 10 Jahre, ab dem 20. Projektionsjahr
  - Lärche: 140 ± 10 Jahre, BHD  70 cm
  - Buche: 150 ± 10 Jahre, BHD  90 cm
  - Eiche: 250 ± 10 Jahre, BHD  90 cm
  - Ahorn: 180 ± 10 Jahre, BHD  70 cm
  - Erle: 100 ± 10 Jahre, BHD  70 cm
  - Pappel: 70 ± 10 Jahre, BHD  50 cm

## Privatwald mittel 20  ha - 1000 ha

- Privatwald mittel: Arbeitet wirtschaftlich und nicht so sehr nach ANW,
- mittlere Durchmesser, wenige Pflegeintervalle, mittlere Umtriebszeiten, Bestockungsgrad: 0,8,
- Gleichdurchforstung/Hochdurchforstung
- Endnutzung früh mit Mindestdurchmesser im mittleren/höheren Bereich und 50 % (gilt dann als Mindestdurchmesser)
  - Fichte: 160 ± 10 Jahre, BHD  60 cm
  - Fichte: 160 ± 10 Jahre, ab dem 15. Projektionsjahr
  - Tanne: 200 ± 10 Jahre
  - Douglasie: 180 ± 10 Jahre, BHD  90 cm
  - Kiefer: 140 ± 10 Jahre, BHD  70 cm
  - Kiefer: 150 ± 10 Jahre, ab dem 20. Projektionsjahr
  - Lärche: 140 ± 10 Jahre, BHD  70 cm
  - Buche: 150 ± 10 Jahre, BHD  90 cm
  - Eiche: 250 ± 10 Jahre, BHD  90 cm
  - Ahorn: 180 ± 10 Jahre, BHD  70 cm
  - Erle: 100 ± 10 Jahre, BHD  70 cm
  - Pappel: 70 ± 10 Jahre, BHD  50 cm

## Privatwald klein < 20 ha

- Viele Betriebe arbeiten gar nicht, lange Umtriebszeiten,
- Bestockungsgrad 1 - 1,3 ⇒ 1,2, lange Pflegeintervalle ⇒ 15, hohe Durchmesser, Gleichdurchforstung - 24.02.2025 aktuell alle BA Niederdurchforstung bei Alter 80
- Endnutzung spät mit Mindestdurchmesser im mittleren Bereich und 50 % (gilt dann als Mindestdurchmesser)
  - Fichte: 160 ± 10 Jahre, BHD  60 cm
  - Tanne: 200 ± 10 Jahre, BHD  60 cm
  - Douglasie: 180 ± 10 Jahre, BHD  70 cm
  - Kiefer: 150 ± 10 Jahre, BHD  60 cm
  - Kiefer: 150 ± 10 Jahre, ab dem 20. Projektionsjahr
  - Lärche: 140 ± 10 Jahre, BHD  70 cm
  - Buche: 150 ± 10 Jahre, BHD  80 cm
  - Eiche: 250 ± 10 Jahre, BHD  90 cm
  - Ahorn: 180 ± 10 Jahre, BHD  80 cm
  - Erle: 100 ± 10 Jahre, BHD  80 cm
  - Pappel: 70 ± 10 Jahre, BHD  60 cm

## Kommunalwald

- arbeiten wirtschaftlich, ähnlich wie Privatwald mittel mit geringeren Umtriebszeiten und 
- höheren Bestockungsgraden ⇒ 1,
- Nieder- Gleichdurchforstung
  - Fichte: 150 ± 20 Jahre, BHD  60 cm
  - Fichte: 150 ± 20 Jahre, ab dem 15. Projektionsjahr
  - Tanne: 200 ± 20 Jahre
  - Douglasie: 180 ± 10 Jahre, BHD  70 cm
  - Kiefer: 150 ± 10 Jahre, BHD  50 cm
  - Kiefer: 150 ± 10 Jahre, ab dem 20. Projektionsjahr
  - Lärche: 140 ± 10 Jahre, BHD  70 cm
  - Buche: 160 ± 10 Jahre
  - Eiche: 250 ± 10 Jahre, BHD  90 cm
  - Ahorn: 180 ± 10 Jahre, BHD  80 cm
  - Erle: 100 ± 10 Jahre, BHD  70 cm
  - Pappel: 70 ± 10 Jahre

# Wiederbewaldung - Baumartenwechsel
**todo:** TI-Nachfragen

- Fichte und Kiefer werden von allen Bundesländern stark bewirtschaftet
- Nutzung steigt kurz und mittelfristig
- Vorrat sinkt mittelfristig stark
- Nutzung steigt anfangs, sinkt mittelfristig und stagniert zum Ende der Modellierungszeit
- Anteil von Kiefer und Fichte sinkt stark -> Flächen kommen in Wiederbewaldung
- TI prüft Methoden zur Eintragung von Baumartenmischungstabellen

@sebastian.schmidt ergänze gerne die Annahmen und trage für den aktuellen Lauf sobald als möglich ein. Gib aber Rückmeldung per signal ob Du gerade daran arbeitest. Irgendwann machen wir das hier gemeinsam und dann solltest Du nicht drinnen sein.