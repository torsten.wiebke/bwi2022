# Weham

Weham besteht aus einem [Durchforstungsmodell](#durchfostungsmodell) und einem [Sortierungsmodell](#sortierung).

## Durchfostungsmodell

### Durchforstungsparameter



## Sortierung

### Zuweisung Sortierung

Hier werden die Parameter für die Sortierung ausgewählt. Für die Tabelle Zuweistung Sortierung gibt es folgende Hilfestellungen:

#### Sortiervarianten

Es können folgende Sortiervarianten ausgewählt werden:

- **Sortiervariante 1**: Langholz - Mittenstärkensortierung
- **Sortiervariante 2**: Kurzholz - Fixlängen zwischen 2 und 6 m
- **Sortiervariante 3**: Langholz am unteren Stammabschnitt und Fixlängen oben
- **Sortiervariante 4**: Fixlänge (Block) am unteren Stammabschnitt und Langholz oben

#### Parameter
- **Aufarbeitungszopf**: Grenzzopf für die Aufarbeitung in cm m.R.
- **X-Holz**: Länge in cm den nicht verwertbaren Holzes am Stammfuß
- **Min.Zopf unten**: Mindestzopf für Stammholz o.R. in cm
- **Min.Zopf oben**: Mindestzopf für Stammholz o.R. in cm
- **Länge** Sortimentslänge in m zwischen 2 und 6 m bei Fixlängensortierung
- **Mindestzopf**: Mindestzopf für Fixlängensortierung o.R. in cm
- **Längenzugabe**: in cm und % der Fixlänge - die jeweils größere Angabe wird verwendet
- **Fixlängen max**: maximal Anzahl der auszuhaltenden Fixlängen - mindestens 1 ist einzutragen

#### Parametertabelle

| Spaltenkopf             | Erklärung                                                          |
| ---                     | ---                                                                |
| Baumart                 | Baumart                                                            |
| Bhd Stufe               | ab dem BHD greift die Maßnahme                                     |
| Variante                | [Sortiervariante](#sortiervarianten)                               |
| Anteil                  | Anteil des Holzes das nach der Variante sortiert wird              |
| Aufarb. zopf            | Bis zu diesem Durchmesser wird in Stammholz sortiert               |
| X-Holz                  | Holz vom Schnitt bis zur Verwertung (Wurzelanlauf)                 |
| Längenzugabe abs.       | Längenzugabe in cm                                                 |
| Längenzugabe rel.       | Längenzugabe in %                                                  |
| Min.-Zopf (unten)       | Zopfdurchmesser Stammholz                                          |
| Min.-Zopf (oben)        | Zopfdurchmesser nächstes Stammholzstück                            |
| Fixlsort. (Länge)       | länge der Fixlängen bis Zopfdurchmesser erreicht                   |
| Fixlsort. (Mindestzopf) | Zopf bis bis zu dem Fixlängen erzeugt werden, danach Industrieholz |
| Fixl. max.              | maximale Anzahl der Fixlängenstücken pro Baum                      |



## Wiederbewaldung

Momentan werden aus dem alten Bestand zufällig Bäume gezogen und zur Wiederbewaldung genutzt. Es wäre möglich die PNV zu nutzen. Wenn man das will, müsste man das für alle Bundesländer machen.

### Problem Blößen

Bisher ist man davon ausgegangen, dass man immer Blößen hat und das ein durchlaufender Posten ist. In dieser Inventur tauchen Blößen aber in größeren Dimensionen auf. Dafür ist eine Wiederbewaldungslösung notwendig.
