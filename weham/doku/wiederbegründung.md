# Wiederbegründung

WEHAM ist ein Altersklassenmodell. Es erfolgt ein Kahlschlag und eine Wiederbegründung. In der letzten WEHAM erfolgte die Wiederbegründung aufgrund der vorherigen Bestockung.

In der Annahme eine Baumartenliste übergeben zu müssen wurde in Absprache mit dem Waldbau des LFE eine Baumartenliste für die Wiederbegründung festgelegt:

<details> <summary>E-Mail vom 22.07.24 über die Baumartenzusammensetzung der Wiederbegründung</summary>

</details>
Diese Baumartenliste würde aber zu einer pauschalen Wiederbegründung aller Ecken mit dieser Mischung führen. Das TI bot die Wiederbegründung nach PNV an. Um wenigstens Ansatzweise auch standörtliche Unterschiede zu berücksichtigen, wurde die Wiederbegründung anhand der PNV durchgeführt.

In der aktuellen WEHAM wurde sich darauf geeinigt die Wiederbegründung anhand der PNV oder einer Baumartenmischungstabelle durchzuführen. Brandenburg entschied sich für eine modifizierte PNV-Übernahme.

In den nördlichen Wuchsgebieten

- Ostholsteinisch-Westmecklenburger Jungmoränenland
- (Mittel-) Mecklenburger Jungmoränenland
- Ostmecklenburg-Nordbrandenburger Jungmoränenland (Nordbrandenburger Jungmoränenland)
- Ostniedersächsisch-Altmärkisches Altmoränenland (Westprignitz-Altmärkisches Altmoränenland)

wurde die PNV belassen.

In den südlichen Wuchsgebieten

- Mittelbrandenburger Talsand- und Moränenland
- Mittleres nordostdeutsches Altmoränenland
- Düben-Niederlausitzer Altmoränenland

und in den südlichen Wuchsbezirken des Wuchsgebietes Nordostbrandenburger Jungmoränenland (Mittelbrandenburger Jungmoränenland)

- Seelower Platte
- Buckower Stobber-Durchbruch
- Strausberger Platte

wurde die Buche (Fagus Sylvatica) entfernt.

In den südlichen Wuchsbezirken des Wuchsgebietes Düben-Niederlausitzer Altmoränenland

wurde zusätzlich noch der Bergahorn (Acer pseudoplatanus) deutlich (ca. 50%) reduziert.

Die geänderte [*.csv](/weham/weham_data/wiederbegründung_hnp_20250214.csv) wurde am 14.2. per E-Mail an das TI übersandt.
